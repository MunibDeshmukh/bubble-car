<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentalSaleReadingMiscLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_sale_reading_misc_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sale_id');
            $table->longText('field_desc');
            $table->integer('field_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_sale_reading_misc_logs');
    }
}
