@extends('Inc.app')

@section('content')
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
      




    <div class="md-card productsTbl">
      
        <div class="md-card-content">

            <div class="dt_colVis_buttons"></div>
            <table id="" class="myTable" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Customer </th>
                    <th>Voucher No</th>
                    <th>Car Type</th>
                    <th>Car Name</th>
                    <th>Total Amount</th>
                    <th>Payable</th>
                    <th>View</th>
                </tr>
                </thead>



               <tbody>
                   @foreach ($table as $i)

                       <tr>
                           <td>{{$i->id}}</td>
                           <td>{{$i->companyName}}</td>
                           <td>{{$i->voucherNo}}</td>
                           <td>{{$i->carType}}</td>
                           <td>{{$i->carName}}</td>
                           <td>{{$i->total_amount}}</td>
                           <td>{{$i->payable}}</td>
                           <td><a href="javascript:;" class="btn btn-success jobCardDetail" id="{{$i->id}}"  data-toggle="modal" data-target="#myModal">Detail</a></td>
                          

                       </tr>

                   @endforeach
               </tbody>
            </table>
        </div>
    </div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Job Card</h4>
      </div>
      <div class="modal-body" id="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
      <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
   
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

    <script>
        function destroy(id,name) {
            var r = confirm("Delete " + name);
            if (r == true) {
                window.location="brand.delete."+id;
            }
        }
    </script>
<script>

 $(document).ready(function(){
               $('.myTable').DataTable( {
     dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
} );
       
 
     
     $('.jobCardDetail').click(function(){
         
        var id = $(this).attr('id');
         
         $.post('jobcard.detail',{id:id,_token:'{{csrf_token()}}'},function(data){
             
             
             $('#modal-body').html(data);
         });
         
         
     });
     
     
     
 });









</script>

@endsection

