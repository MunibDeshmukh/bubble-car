@extends('Inc.app')

@section('content')

    <!-- statistics (small charts) -->
    <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium " data-uk-grid-margin>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_visitors peity_data">5,3,9,6,5,9,7</span></div>
                    <span class="uk-text-muted uk-text-small">Vendors </span>
                    <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>12456</noscript></span></h2>
                </div>
            </div>
        </div>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_sale peity_data">5,3,9,6,5,9,7,3,5,2</span></div>
                    <span class="uk-text-muted uk-text-small">Items</span>
                    <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>142384</noscript></span></h2>
                </div>
            </div>
        </div>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">64/100</span></div>
                    <span class="uk-text-muted uk-text-small">Services</span>
                    <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>64</noscript></span>%</h2>
                </div>
            </div>
        </div>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_live peity_data">5,3,9,6,5,9,7,3,5,2,5,3,9,6,5,9,7,3,5,2</span></div>
                    <span class="uk-text-muted uk-text-small">Visitors (live)</span>
                    <h2 class="uk-margin-remove" id="peity_live_text">46</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-4 uk-text-center " id="dashboard_sortable_cards" data-uk-grid-margin>


        <a href="purchase.list">
            <img src="/assets/img/home/purchase.jpg">
            <div style="font-size: 19px;text-align: center;background-color:#673ab7;color: white;font-weight: bold;">Common Purchase</div>
        </a>
        <a href="local.purchase.list">
            <img src="/assets/img/home/purchase.jpg">
            <div style="font-size: 19px;text-align: center;background-color:#673ab7;color: white;font-weight: bold;">Local Purchase</div>
        </a>
        <a href="vendor.payable.list">
            <img src="/assets/img/home/transctions.jpg">
            <div style="font-size: 19px;text-align: center;background-color:#673ab7;color: white;font-weight: bold;">Vendor Payables</div>
        </a>
    </div>


@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- d3 -->
    <script src="bower_components/d3/d3.min.js"></script>
    <!-- metrics graphics (charts) -->
    <script src="bower_components/metrics-graphics/dist/metricsgraphics.min.js"></script>
    <!-- chartist (charts) -->
    <script src="bower_components/chartist/dist/chartist.min.js"></script>
    <!-- maplace (google maps) -->
    <script src="http://maps.google.com/maps/api/js"></script>
    <script src="bower_components/maplace-js/dist/maplace.min.js"></script>
    <!-- peity (small charts) -->
    <script src="bower_components/peity/jquery.peity.min.js"></script>
    <!-- easy-pie-chart (circular statistics) -->
    <script src="bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
    <!-- countUp -->
    <script src="bower_components/countUp.js/dist/countUp.min.js"></script>
    <!-- handlebars.js -->
    <script src="bower_components/handlebars/handlebars.min.js"></script>
    <script src="assets/js/custom/handlebars_helpers.min.js"></script>
    <!-- CLNDR -->
    <script src="bower_components/clndr/clndr.min.js"></script>

    <!--  dashbord functions -->
    <script src="assets/js/pages/dashboard.min.js"></script>
@endsection

