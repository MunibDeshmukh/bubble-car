@extends('Inc.app')

@section('content')
    <div class="md-card" style="background: rgba(255,255,255,0.9)">
        <div class="md-card-toolbar">
            <div class="md-card-toolbar-actions">
                <a href="customer.create" style="color: inherit">Add New</a>
            </div>
            <h3 class="md-card-toolbar-heading-text">
                Edit Customer
            </h3>
        </div>
        <div class="md-card-content " style="display: block;">
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-6-10 uk-push-2-10 uk-row-first">
                    <form action="customer.update" method="post">

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif

                        {{csrf_field()}}
                        <div style="width: 70%;margin-left: 15%">
                        </div>
                        <table style="width: 100%">
                            <tbody>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Customer ID</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small " name="id" id="id" value="{{$id}}" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td style=" padding:15px 0;max-width: 50px; ">Company Type</td>
                                <td>
                                <select title="" id="customer_type" data-md-selectize name="customer_type"  onchange="if(this.value=='rental'){$('#rental_opt').prop('hidden',false)}else{$('#rental_opt').prop('hidden',true)}" >
                                      <option selected>Select...</option>
                                      <option value="our_station">Our Station</option>
                                      <option value="rental">Rental</option>
                                </select>
                                    @if($customer_type)
                                    <script>
                                        $(document).ready(function () {
                                            $("#customer_type").val("{{$customer_type}}");
                                        });
                                    </script>
                                    @endif
                                </td>
                            </tr>

                             <tr hidden id="rental_opt" >
                                <td style="padding:15px 0;max-width: 50px; " >Rental Type</td>
                                <td>
                                <select title="" id="rental_type" data-md-selectize name="rental_type"  >
                                      <option selected>Select...</option>
                                      <option value="quantity">Quantity</option>
                                      <option value="reading">Reading</option>
                                </select>
                                    @if($rental_type)
                                        <script>
                                            $(document).ready(function () {
                                                $("#rental_type").val("{{$rental_type }}");
                                                $("#rental_opt").prop("hidden",false);
                                            });
                                        </script>
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Company Name</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="company_name" name="company_name" value="{{$company_name}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Contact Person Name</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small " name="contact_person_name" id="contact_person_name" value="{{$person_name}}" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Contact No.</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small " name="contact" id="contact" value="{{$contact}}" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Email</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small " name="email" id="email" value="{{$email}}" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Address</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small " name="address" id="address" value="{{$address}}" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                             <tr>
                                <td style="padding:15px 0;max-width: 50px; ">NTN</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small " name="ntn" id="ntn"  value="{{$ntn}}" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                              <tr>
                                <td style="padding:15px 0;max-width: 50px; ">SRB</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small " name="srb" id="srb"  value="{{$srb}}" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                             <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Opening Balance</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small " name="opening_balance" id="opening_balance"  value="{{$opening_balance}}" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Business Nature</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small " name="business_nature" id="business_nature"  value="{{$business_nature}}" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                        <div class="uk-form-row">
                            <div class="uk-width-1-1 uk-margin-top">
                                <button type="submit" name="save" class="md-btn md-btn-primary uk-float-right">Create</button>
                                <a href="" class="md-btn md-btn uk-float-right">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>

    </div>





        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>


@endsection

