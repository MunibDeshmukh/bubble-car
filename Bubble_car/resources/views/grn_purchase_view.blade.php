           <table id="dt_tableExport" class="uk-table table-bordered table-responsive table-hover">
                       <thead>
                       <tr align="center">
                           <td><b>Product Name</b></td>
                           <td><b>Quantity</b></td>
                           <td><b>Price</b></td>
                           <td><b>Amount</b></td>
                           <td><b>Status</b></td>
                           <td><b>Received</b></td>

                       </tr>
                       </thead>
                       <tbody id="itemsTbl">

                       @foreach($table as $i)

                           <tr align="center">
                               
                               <td>{{$i->name}}</td>
                               <td>{{$i->item_quantity}}</td>
                               <td>{{$i->price}}</td>
                               <td>{{$i->amount}}</td>
                               <td>{{$i->status}}</td>
                              <td>
                                <?php if($i->status=="Received"){?>
                       <i class="fas fa-check-circle"></i>
                                <?php }else{?>
                                <a href="javascript:;" id="" onclick="generateGrn('{{$i->vendor_id}}','{{$i->product_id}}','{{$i->voucher_no}}','{{$i->purchase_id}}'
                                   ,'{{$i->name}}','{{$i->amount}}','{{$i->item_quantity}}')" 
                                   class="btn btn-info ">Received</a>
                                <?php } ?>
                              </td>

                           </tr>
                       @endforeach

                       </tbody>



                   </table>


                   <script type="text/javascript">
                     

function generateGrn(id,productId,voucherNo,purchase_id,productName,amount,Quantity){

      $.ajax({
                            url: "grn.insert",
                            type: "GET",
                            data: {
                                
                                id: id,
                                productId:productId,
                                voucherNo:voucherNo,
                                purchase_id:purchase_id,
                                productName:productName,
                                amount:amount,
                                Quantity:Quantity

                            },
                           
                            success: function (e) {

                                $('#itemsTbl').html(e);

                              

                            }
                        });  

}


                   </script>