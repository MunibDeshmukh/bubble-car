@extends('Inc.app')

@section('content')
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">

    <div class="md-card light-bg">
        <div class="md-card-toolbar" >
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons ">add</i>
            </div>
            <h3 class="md-card-toolbar-heading-text">
                Service
            </h3>
        </div>
        <div class="md-card-content" >
            <div id="msg"></div>
            <div class="uk-grid" data-uk-grid-margin="">

                <div class="uk-width-medium-1-1 uk-row-first">
                    <table id="dt_tableExport" class="uk-table table-bordered table-responsive table-hover">
                        <thead>
                        <tr>
                            <th>Service Name </th>
                            <th>Small Price </th>
                            <th>Medium Price </th>
                            <th>Large Price </th>
                            <th>Extra Large </th>
                            <th>View </th>


                        </tr>
                        </thead>
                        <tbody id="itemsTable">

                        @foreach($table as $i)

                            <tr>
                                <td>{{$i->ServiceName}}</td>
                                <td>{{$i->small}}</td>
                                <td>{{$i->medium}}</td>
                                <td>{{$i->large}}</td>
                                <td>{{$i->extraLarge}}</td>
                                <td><a href="javascript:;" id="{{$i->serviceId}}" data-toggle="modal" data-target="#myModal" class="btn btn-info viewRecipe">View Recipe</a> </td>


                            </tr>
                        @endforeach

                        </tbody>



                    </table>

                </div>
            </div>



        </div>

    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>




@endsection

