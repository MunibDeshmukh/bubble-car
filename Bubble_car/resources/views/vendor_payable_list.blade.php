@extends('Inc.app')

@section('content')

    <h2 class="text-light">Vendor Payables</h2>
    <hr>


    <div class="md-card">
        <div class="md-card-toolbar" style="display: none">
        <div class="md-card-toolbar-actions">
            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
            <i class="md-icon material-icons md-card-toggle"  >&#xE316;</i>
        </div>
        <h3 class="md-card-toolbar-heading-text">
            All Items
        </h3>
    </div>
        <div class="md-card-content">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    <div class="uk-alert uk-alert-success" data-uk-alert="">
                        <a href="#" class="uk-alert-close uk-close"></a>
                        {{ session()->get('message') }}
                    </div>
                </div>
            @endif
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Company Name</th>
                    <th>Credit</th>
                    <th>Debit</th>
                    <th>Balance</th>
                    <th>Date</th>
                    {{--<th>Manage</th>--}}
                </tr>
                </thead>



               <tbody>
               <span hidden> {{$purchase =0}}</span>
               <span hidden> {{$balance =0}}</span>
               <span hidden> {{$payment=0}}</span>
                   @foreach ($table as $i)

                       <tr>
                           <td>{{$i->id}}</td>
                           <td>{{$i->company_name}}</td>
                           <td>{{$i->purchase}}</td>
                           <td>{{$i->payment}}</td>
                           <td>{{$i->balance}}</td>
                           <td>{{$i->purchase_date}}</td>


                           {{--<td>--}}
                               {{--<a href="product.edit.{{$i->id}}" style="" class=" md-btn-icon"><i class="uk-icon-edit no_margin"></i></a>--}}
                               {{--<a href="#" onclick="destroy('{{$i->id}}','')" style="" class=" md-btn-icon"><i class="uk-icon-remove no_margin"></i></a>--}}
                           {{--</td>--}}

                       </tr>

                       <span hidden> {{$purchase += $i->purchase }}</span>
                       <span hidden> {{$balance += $i->balance  }}</span>
                       <span hidden> {{$payment += $i->payment  }}</span>

                   @endforeach
               </tbody>
                <tfoot>
                <tr>
                    <th></th>
                    <th>Total</th>
                    
                    <th>{{$purchase}}</th>
                    <th>{{$payment}}</th>
                    <th>{{$balance}}</th>
                    <th></th>

                </tr>
                </tfoot>
            </table>
        </div>
    </div>


        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>

    <script>
        function destroy(id,name) {
            var r = confirm("Delete " + name);
            if (r == true) {
                window.location="product.delete."+id;
            }
        }
    </script>

@endsection

