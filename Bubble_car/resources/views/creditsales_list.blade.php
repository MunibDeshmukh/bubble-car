@extends('Inc.app')

@section('content')
           <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
<div class="row">
        <h2 style="font-family:Times New Roman;">Credit Sales</h2>
             <div class="col-sm-6">
                    
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" class="btn btn-default products">Products</button>
  <button type="button" class="btn btn-secondary services">Service</button>
  <button type="button" class="btn btn-secondary outsourceservices">Out Source Service </button>
</div>
            
            </div>
            </div>
        <br>   




    <div class="md-card productsTbl">
      
        <div class="md-card-content">

            <div class="dt_colVis_buttons"></div>
            <table id="" class="myTable" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Amount</th>
                    <th>Tax</th>
                    <th>Receivable</th>
                    <th>Status</th>
                </tr>
                </thead>



               <tbody>
                   @foreach ($table as $i)

                       <tr>
                           <td>{{$i->productName}}</td>
                           <td>{{$i->item_quantity}}</td>
                           <td>{{$i->price}}</td>
                           <td>{{$i->amount}}</td>
                           <td>{{$i->tax}}</td>
                           <td>{{$i->receivable}}</td>
                           <td>{{$i->status}}</td>
                          

                       </tr>

                   @endforeach
               </tbody>
            </table>
        </div>
    </div>



    <div class="md-card servicesTbl">
      
        <div class="md-card-content">

            <div class="dt_colVis_buttons"></div>
            <table id="" class="myTable" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Service Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Amount</th>
                    <th>Tax</th>
                    <th>Receivable</th>
                    <th>Status</th>
                </tr>
                </thead>



               <tbody>
                   @foreach ($table1 as $i)

                       <tr>
                           <td>{{$i->serviceName}}</td>
                           <td>{{$i->item_quantity}}</td>
                           <td>{{$i->price}}</td>
                           <td>{{$i->amount}}</td>
                           <td>{{$i->tax}}</td>
                           <td>{{$i->receivable}}</td>
                           <td>{{$i->status}}</td>
                          

                       </tr>

                   @endforeach
               </tbody>
            </table>
        </div>
    </div>


    <div class="md-card outsourceTbl">
      
        <div class="md-card-content">

            <div class="dt_colVis_buttons"></div>
            <table id="" class="myTable" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Out Source Service Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Amount</th>
                    <th>Tax</th>
                    <th>Receivable</th>
                    <th>Status</th>
                </tr>
                </thead>



               <tbody>
                   @foreach ($table2 as $i)

                       <tr>
                           <td>{{$i->serviceName}}</td>
                           <td>{{$i->item_quantity}}</td>
                           <td>{{$i->price}}</td>
                           <td>{{$i->amount}}</td>
                           <td>{{$i->tax}}</td>
                           <td>{{$i->receivable}}</td>
                           <td>{{$i->status}}</td>
                          

                       </tr>

                   @endforeach
               </tbody>
            </table>
        </div>
    </div>



        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
   
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

    <script>
        function destroy(id,name) {
            var r = confirm("Delete " + name);
            if (r == true) {
                window.location="brand.delete."+id;
            }
        }
    </script>
<script>

 $(document).ready(function(){
          $('.myTable').DataTable( {
     dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
} );
       
    $('.servicesTbl').hide();
    $('.outsourceTbl').hide();
    
    
$('.services').click(function(){
   
    $(this).removeClass('btn btn-secondary');
    $('.products').removeClass('btn btn-default');
    $('.outsourceservices').removeClass('btn btn-default');
    
    $(this).addClass('btn btn-default');
    $('.products').addClass('btn btn-secondary');
    $('.outsourceservices').addClass('btn btn-secondary');
    $('.productsTbl').fadeOut(1000);
    $('.outsourceTbl').fadeOut(1000);
    $('.servicesTbl').fadeIn(1000);
    
}); 
    
    $('.products').click(function(){
   
    $(this).removeClass('btn btn-secondary');
    $('.services').removeClass('btn btn-default');
    $('.outsourceservices').removeClass('btn btn-default');
    
    $(this).addClass('btn btn-default');
    $('.services').addClass('btn btn-secondary');
    $('.outsourceservices').addClass('btn btn-secondary');
    $('.productsTbl').fadeIn(1000);
    $('.servicesTbl').fadeOut(1000);
         $('.outsourceTbl').fadeOut(1000);
    
}); 
    
   $('.outsourceservices').click(function(){
   
    $(this).removeClass('btn btn-secondary');
    $('.services').removeClass('btn btn-default');
    $('.products').removeClass('btn btn-default');
    
    $(this).addClass('btn btn-default');
    $('.services').addClass('btn btn-secondary');
    $('.products').addClass('btn btn-secondary');
    $('.productsTbl').fadeOut(1000);
    $('.servicesTbl').fadeOut(1000);
    $('.outsourceTbl').fadeIn(1000);
}); 
     
     
 });









</script>

@endsection

