@extends('Inc.app')

@section('content')

    <div class="md-card" style="background: rgba(255,255,255,0.9)">
        <div class="md-card-toolbar">
            <div class="md-card-toolbar-actions">
             
            </div>
            <h3 class="md-card-toolbar-heading-text">
               Services
            </h3>
        </div>
        <div class="md-card-content " style="display: block;">
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-6-10 uk-push-2-10 uk-row-first">
                    <form action="" method="post">

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif

                        {{csrf_field()}}
                        <div style="width: 70%;margin-left: 15%">
                        </div>
                        <table style="width: 100%">
                            <tbody>


                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Service Name</td>
                                <td>
                                    <div class="md-input-wrapper">
                                        <input type="text" autocomplete="off" placeholder="Service Name" class="md-input md-input-small" id="serviceName" name="serviceName" value="{{old('company_name')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>


                            </tbody>
                        </table>


                        <div class="uk-form-row">
                            <div class="uk-width-1-1 uk-margin-top">
                                <button type="button" name="save" class="md-btn md-btn-primary uk-float-right setPrice">Set Price</button>

                            </div>
                        </div>
                            <table style="width: 100%" id="setPriceTbl">
                                <tbody>


                                <tr>
                                    <td style="padding:15px 0;max-width: 50px; ">Small</td>
                                    <td>
                                        <div class="md-input-wrapper">
                                            <input type="number" autocomplete="off" placeholder="Small Price" class="md-input md-input-small" id="smallPrice" name="smallPrice" value="{{old('company_name')}}"><span class="md-input-bar "></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:15px 0;max-width: 50px; ">Medium</td>
                                    <td>
                                        <div class="md-input-wrapper">
                                            <input type="number" autocomplete="off" placeholder="Medium Price" class="md-input md-input-small" id="mediumPrice" name="mediumPrice" value="{{old('company_name')}}"><span class="md-input-bar "></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:15px 0;max-width: 50px; ">Large</td>
                                    <td>
                                        <div class="md-input-wrapper">
                                            <input type="number" autocomplete="off" placeholder="Large Price" class="md-input md-input-small" id="largePrice" name="largePrice" value="{{old('company_name')}}"><span class="md-input-bar "></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:15px 0;max-width: 50px; ">Extra Large</td>
                                    <td>
                                        <div class="md-input-wrapper">
                                            <input type="number" autocomplete="off" placeholder="Extra Large Price" class="md-input md-input-small" id="extralargePrice" name="extralarge" value="{{old('company_name')}}"><span class="md-input-bar "></span></div>
                                    </td>
                                </tr>


                                </tbody>
                            </table>

                            <div class="uk-form-row">

                            <div class="dropdown pull-right">
                                <button class="btn btn-primary dropdown-toggle createReipe " type="button" data-toggle="dropdown">Create Recipe
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu ul">
                                    <li> <a href="javascript:;" class="small">Small</a></li>
                                    <li> <a href="javascript:;" class="medium">Medium</a></li>
                                    <li> <a href="javascript:;" class="large">Large</a></li>
                                    <li><a  href="javascript:;" class="extraLarge">Extra Large</a></li>
                                </ul>
                            </div>
                            </div>
                         <p><b id="cp"></b></p>
                        <table CLASS="table table-bordered allpro">

                            <tr align="center">
                                <td><b>Check</b></td>
                                <td><b>Product Code</b></td>
                                <td><b>Product Name</b></td>
                                <td><b>Product Category</b></td>
                                <td><b>Product Brand</b></td>
                            </tr>
                          @foreach($table as $tbl)
                            <tr align="center" id="tr{{$tbl->id}}">
                                <td><input type="checkbox" onchange="chk(this,'{{$tbl->id}}')" class="chk" value ="{{$tbl->id}}"></td>
                                <td>{{$tbl->pro_code}}</td>
                                <td>{{$tbl->name}}</td>
                                <td>{{$tbl->category_name}}</td>
                                <td>{{$tbl->brand_name}}</td>

                            </tr>
                   @endforeach
                        </table>
                            <div class="uk-form-row">
                                <div class="uk-width-1-1 uk-margin-top">
                                    <button type="button" name="save" class="md-btn md-btn-primary uk-float-right continue">Continue</button>

                                </div>
                            </div>


                            <table CLASS="table table-bordered selpro">
                              <thead>
                                <tr align="center">

                                    <td><b>Product Code</b></td>
                                    <td><b>Product Name</b></td>
                                    <td><b>Product Category</b></td>
                                    <td><b>Product Brand</b></td>
                                    <td><b>Product Quantity</b></td>
                                </tr>
                              </thead>
                                <tbody id="selectedPro">

                                </tbody>
                            </table>
                            <div class="uk-form-row">
                                <div class="uk-width-1-1 uk-margin-top">
                                    <button type="button" name="save" class="md-btn md-btn-primary uk-float-right mesurment">Continue</button>

                                </div>
                            </div>

                            <table style="width: 100%" id="mesurmentTbl">
                                <tbody>

                                <tr>
                                    <td style="padding:15px 0;max-width: 50px; ">Measurment Quantity</td>
                                    <td>
                                        <div class="md-input-wrapper">
                                            <input type="number" autocomplete="off" placeholder="Quantity" class="md-input md-input-small" id="measurmentQuantity" name="measurmentQuantity" ><span class="md-input-bar "></span></div>
                                    </td>
                                </tr>


                                <tr>
                                    <td style="padding:15px 0;max-width: 50px; ">Price</td>
                                    <td>
                                        <div class="md-input-wrapper">
                                            <input type="number" autocomplete="off" placeholder="Price" class="md-input md-input-small" id="price" name="servicePrice" ><span class="md-input-bar "></span></div>
                                    </td>
                                </tr>



                                </tbody>
                            </table>
                            <div class="uk-form-row">
                                <div class="uk-width-1-1 uk-margin-top">
                                    <button type="button" name="save" class="md-btn md-btn-primary uk-float-right done">Done</button>

                                </div>
                            </div>


                    </form>
                    <div class="alert alert-success msg">Recipe Created Successfully..</div>
                </div>
            </div>



        </div>

    </div>





@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>



<script>
    $(document).ready(function(){
        //Hide Sections
        $('#setPriceTbl').hide();
        $('.createReipe').hide();
        $('.allpro').hide();
        $('.continue').hide();
        $('.selpro').hide();
        $('.mesurment').hide();
        $('#mesurmentTbl').hide();
        $('.done').hide();
        $('.msg').hide();
        //Event Sections
        $('.setPrice').click(function(){

            $('#setPriceTbl').fadeToggle(1000);
            $('.createReipe').fadeToggle(1000);
        });


        $('.ul li').on('click',function(){

            var txt = $(this).text();
            $('.allpro').fadeIn(1000);
            $('#cp').text(txt);
            $('.continue').fadeIn(1000);

        });


        $('.continue').on('click',function(){


            $('.selpro').fadeIn(1000);
            $('.mesurment').fadeIn(1000);

        });

        $('.mesurment').on('click',function(){


            $('#mesurmentTbl').fadeIn(1000);
            $('.done').fadeIn(1000);


        });


        $('.done').click(function(){
            var proId = [];
            var proQuantity = [];
            var serviceName = $('#serviceName').val();
           var smallPrice = $('#smallPrice').val();
           var mediumPrice = $('#mediumPrice').val();
           var largePrice = $('#largePrice').val();
           var extralargePrice = $('#extralargePrice').val();
           var measurmentQuantity = $('#measurmentQuantity').val();
            var price = $('#price').val();
            var recipeType = $('#cp').text();

            $('.quantity').each(function (e) {
                proId[e] = $(this).attr('id');
                proQuantity[e] = $(this).val();
            });


          $.post('service.create',{proId:proId,proQuantity:proQuantity,serviceName:serviceName,smallPrice:smallPrice,
              mediumPrice:mediumPrice,largePrice:largePrice,extralargePrice:extralargePrice,measurmentQuantity:measurmentQuantity,
              price:price,recipeType:recipeType ,_token :'{{csrf_token()}}'},function(data){
               $('.msg').fadeIn(1000);
              setTimeout(function(){
                  location.reload();

              }, 2000);

          });







        });




    });

</script>
    <script>

        function chk(inp,id){

            if( $(inp).is(':checked')){

                $productCode    =  $("#tr"+id).find('td').eq(1).html();
                $productName  =  $("#tr"+id).find('td').eq(2).html();
                $productCategory =  $("#tr"+id).find('td').eq(3).html();
                $productBrand =  $("#tr"+id).find('td').eq(4).html();


//                       alert($a);
                $("#selectedPro").append(
                    '<tr id="pr'+id+'" align="center">' +
                    '<td>'+$productCode+'</td>' +
                    '<td>'+$productName+'</td>' +
                    '<td>'+$productCategory+'</td>' +
                    '<td>'+$productBrand+'</td>' +
                    '<td><input type="text" id="'+id+'"  class="quantity" value="0" style="max-width:80px"></td>' +

                    '</td>'
                );



            }else{
                $('#pr'+id).remove();
            }
        }

    </script>
@endsection