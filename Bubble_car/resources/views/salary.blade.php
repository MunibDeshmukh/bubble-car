@extends('Inc.app')

@section('content')
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">



    <div class="md-card light-bg md-card-primary">
        <div class="md-card-toolbar" light-bg >
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-fullscreen-activate toolbar_fixed"></i>
            </div>
            <h3 class="md-card-toolbar-heading-text">
                <b>Salary</b>
            </h3>
        </div>
        <div class="md-card-content" >
            <div id="msg"></div>
            <div class="uk-grid" data-uk-grid-margin="">

                <div class="uk-width-medium-1-1  uk-row-first">
                   <table id="dt_tableExport" class="uk-table table-bordered table-responsive table-hover">
                       <thead>
                       <tr>
                           <th>#</th>
                           <th>Name</th>
                           <th>Salary</th>
                           <th>Working Days</th>
                           <th>Per Day</th>
                           <th>Attended Days</th>
                           <th>Net Salary</th>
                           <th>Total Loan Amount</th>
                           <th>Due Loan Amount</th>
                           <th>Loan Installment</th>
                           <th>Advance Amount</th>
                           <th>Detect Loan</th>
                           <th>Payable</th>
                           <th>Status</th>


                       </tr>
                       </thead>
                       <tbody id="itemsTable">
                       @foreach($table as $i)


                           <tr id="tr{{$i->id}}">
                               <td>{{intval($i->id)}}</td>
                               <td>{{$i->name}}</td>
                               <td>{{intval($i->salary)}}</td>
                               <td contenteditable="true" bgcolor="#f5f5f5"></td>
                               <td onclick="parseInt($(this).html(parseInt(parseFloat($(this).closest('tr').find('td').eq(2).html()) / parseFloat($(this).closest('tr').find('td').eq(3).html()))));"></td>
                               <td contenteditable="true" bgcolor="#f5f5f5"></td>
                               <td onclick="
                               $(this).html(parseInt($(this).closest('tr').find('td').eq(4).html()) * parseInt($(this).closest('tr').find('td').eq(5).html()));"></td>
                               <td>{{intval($i->loan_amount)}}</td>
                               <td>{{intval($i->loan_due)}}</td>
                               <td contenteditable="true"> {{intval($i->loan_installment)}}</td>
                               <td>{{intval($i->advance_amount)}}</td>
                               <td>
                                   <input title=""  type="checkbox" onchange="if($(this).is(':checked')){
                                       $(this).closest('tr').find('td').eq(12).html($(this).closest('tr').find('td').eq(6).html() - $(this).closest('tr').find('td').eq(9).html() - $(this).closest('tr').find('td').eq(10).html() );
                                   }else{$(this).closest('tr').find('td').eq(12).html(parseInt($(this).closest('tr').find('td').eq(6).html()  - $(this).closest('tr').find('td').eq(10).html()));}" data-switchery data-switchery-color="#673ab7"  id="switch_demo_primary" />
                               </td>
                               <td></td>
                               <td><i class="material-icons"></i></td>

                           </tr>
                       @endforeach



                       </tbody>



                   </table>

                    <div class="uk-form-row">
                        <div class="uk-width-1-1 uk-margin-top">
                            <button  type="button" onclick="create()" class="md-btn md-btn-primary uk-float-right">POST</button>

                        </div>
                    </div>
                </div>
            </div>



        </div>

    </div>

        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>



        <script>

            $(document).ready(function () {
                $('.md-card-collapsed').css('display','Inherit');
            });

            function create() {

                var detect ='no';

                for ($i = 0; $i < "{{count($table->all())}}"; $i++) {
                    var status = null;

                    var id = $("#itemsTable").find('tr').eq($i).find('td').eq(0).text();
                    var name = $("#itemsTable").find('tr').eq($i).find('td').eq(1).text();
                    var salary = $("#itemsTable").find('tr').eq($i).find('td').eq(2).text();
                    var working_days = $("#itemsTable").find('tr').eq($i).find('td').eq(3).text();
                    var per_day = $("#itemsTable").find('tr').eq($i).find('td').eq(4).text();
                    var attended_day = $("#itemsTable").find('tr').eq($i).find('td').eq(5).text();
                    var net_salary = $("#itemsTable").find('tr').eq($i).find('td').eq(6).text();
                    var loan_amount = $("#itemsTable").find('tr').eq($i).find('td').eq(7).text();
                    var due_loan_amount = $("#itemsTable").find('tr').eq($i).find('td').eq(8).text();
                    var loan_installment = $("#itemsTable").find('tr').eq($i).find('td').eq(9).text();
                    var advance_amount = $("#itemsTable").find('tr').eq($i).find('td').eq(10).text();
                    var detect_btn = $("#itemsTable").find('tr').eq($i).find('td').eq(11).find('input');
                    var payable = $("#itemsTable").find('tr').eq($i).find('td').eq(12).text();
                     status = $("#itemsTable").find('tr').eq($i).find('td').eq(13).find('i');

                    if($(detect_btn).is(':checked')) detect = 'yes';

                    $.ajax({
                        url: "salary.create",
                        type: "POST",
                        data: {
                            _token: '{{csrf_token()}}',
                             id :id,
                            name : name,
                            salary : salary,
                            working_days :working_days,
                            per_day : per_day,
                            attended_day : attended_day,
                            net_salary : net_salary,
                            loan_amount : loan_amount,
                            due_loan_amount : due_loan_amount,
                            loan_installment : loan_installment,
                            advance_amount : advance_amount,
                            detect:detect,
                            payable :payable
                        },
                        beforeSend: function () {
                            console.log("-----------------------------------");
                        },
                        complete: function (e) {
                            setTimeout(function () {
                                if(e.responseText==1)
                                    status.html("check");
                                else{
                                    status.html("info");
                                }
                            },300);

                            console.log(e);
                        }

                    });
                }
            }



    </script>

@endsection

