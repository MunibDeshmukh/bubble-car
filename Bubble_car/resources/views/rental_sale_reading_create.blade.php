@extends('Inc.app')

@section('content')


    <h2 class="text-light">Rental Sale Reading</h2>
    <hr>


    <div class="md-card">

        <div class="md-card-content" style="display: block;">
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-1-1 uk-row-first">
                    <form action="customer_payable.create" method="post">

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif

                        {{csrf_field()}}
                        <div style="width: 70%;margin-left: 15%">
                        </div>
                        <table style="width: 100%">
                            <tbody>
                            <tr>
                                <td style="padding:15px 0;width: 130px; ">Sale No.</td>
                                <td>
                                    <div class="md-input-wrapper"><input value="{{$sale_id}}" readonly type="text" autocomplete="off"  class="md-input md-input-small" id="sale_id" name="sale_id" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 100px; ">Company Name</td>
                                <td>
                                    <div class="md-input-wrapper"><input  readonly="" type="text" autocomplete="off" class="md-input md-input-small" id="company_name" name="company_name" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 100px; ">Contact Person</td>
                                <td>
                                    <div class="md-input-wrapper"><input  readonly="" type="text" autocomplete="off" class="md-input md-input-small" id="person_name" name="person_name" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 100px; ">Contact#</td>
                                <td>
                                    <div class="md-input-wrapper"><input  readonly="" type="text" autocomplete="off" class="md-input md-input-small" id="contact" name="contact" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>


                                    <div class="uk-overflow-container" style="background-color: whitesmoke">
                                        <table class="uk-table  table-bordered table-hover  " >

                                        <thead>
                                        <tr>
                                            <th  style="max-width: 30px;">#</th>
                                            <th  style="max-width: 100px;">Product </th>
                                            <th>Previous</th>
                                            <th>Current</th>
                                            <th>Quantity</th>
                                            <th>Rate</th>
                                            <th>Amount</th>
                                            <th>Other</th>
                                            <th>NetAmount</th>
                                            <th>MinCopy</th>
                                            <th>RemainingAmount</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody id="products" >

                                    </tbody>

                                </table>
                                    </div>

                            <table style="width: 100%">
                            <tr>
                                <td style="padding: 15px 0;width: 130px;">Invoice No.</td>
                                <td  >
                                    <div class="md-input-wrapper">
                                        <input type="text" readonly value="{{$voucher}}" required id="invoice_no" name="invoice_no" class="md-input"><span class="md-input-bar "></span>
                                    </div>
                                </td>

                            </tr>

                            <tr>
                                <td style="padding: 15px 0;">Total Amount</td>
                                <td  >
                                    <div class="md-input-wrapper">
                                        <input type="text"  readonly onclick="calcAmount()" id="final_amount" name="final_amount" class="md-input"><span class="md-input-bar "></span>
                                    </div>
                                     </td>
                        
                            </tr>
                            <td style="padding: 15px 0;">Date</td>
                            <td  style=";position: relative"  >
                                <input readonly
                                       style="border: none;position: absolute;width: 100%;margin-top: -10px;;background: rgba(000,000,000,0)" placeholder="click to select" 
                                       title=""  type="text" id="date" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                            </td>
                        </tr>


                             <tr id="stTr">
                                <td style="padding:15px 0;max-width: 100px; ">Sales Tax</td>
                                <td>
                                    <div class="uk-grid" data-uk-grid-margin="">
                                <div class="uk-width-large-1-2 uk-width-medium-1-2">
                                <div class="md-input-wrapper">
                                    <input type="text" onkeyup="$('#sale_tax_no').val(parseInt(parseInt($('#final_amount').val())/100*parseFloat($(this).val())))"  placeholder="In percentage" id="sale_tax_per" name="sale_tax_per" class="md-input"><span class="md-input-bar "></span>
                                    </div>
                                </div>
                                <div class="uk-width-large-1-2 uk-width-medium-1-2">
                                <div class="md-input-wrapper">
                                    <input type="text" readonly="" id="sale_tax_no" name="sale_tax_no" class="md-input"><span class="md-input-bar "></span>
                                    </div>
                                </div>
                            </div>
                        
                                </td>
                            </tr>
                            </tbody>
                        </table>

                          <table style="width: 100%">
                            <tbody  id="misc_fields">

                                
                            </tbody>
                            
                        </table>
                         <a class="md-btn md-btn-small" style="" onclick="miscFields()" >ADD</a>
                        <div class="uk-form-row">
                            <div class="uk-width-1-1 uk-margin-top">
                                <button type="button" onclick="create()" name="save" class="md-btn md-btn-primary uk-float-right">Create</button>
                                <a href="" class="md-btn md-btn uk-float-right">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>

    </div>





        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>


    <script>
        $(document).ready(function () {
            getSale("{{$sale_id}}");
        });
        
       function getSale(id)
       {
            $.ajax({
                url:"rental.sale.get",
                type:"POST",
                data:
                    {
                    _token: '{{csrf_token()}}',
                    id:id
                    },
                complete:function (e)
                {
                    console.log("----------: Data Retrieve Successfully  :----------");
                    var json = JSON.parse(e.responseText);
                    console.log(json);
                    var reading = 0;
                    var min_copy = 0;

                    $("#contact").val(json[0]['customer_contact']);
                    $("#company_name").val(json[0]['company_name']);
                    $("#person_name").val(json[0]['person_name']);

                    $.ajax
                    ({
                        url:"rental.sale.products.get",
                        type:"POST",
                        data:
                            {
                            _token: '{{csrf_token()}}',
                            id:id
                            },
                        complete:function (e)
                        {
                            console.log("----------: Data Retrieve Successfully  :----------");
                            var json = JSON.parse(e.responseText);
                            console.log(json);
                            $("#products").html(null);
                            $.each(json,function (key,item) {


                                $.ajax
                                ({
                                    url:"rental.sale.reading.get",
                                    type:"POST",
                                    data:
                                        {
                                        _token: '{{csrf_token()}}',
                                        sale_id:id,
                                        product_id:json[key]['product_id']
                                        },
                                        complete:function (e)
                                        {
                                            console.log("----------: Data Retrieve Successfully  :----------");
                                            var json2 = JSON.parse(e.responseText);
                                            console.log(json2);
                                               if(json2.length)
                                               {

                                                $("#products").append
                                                (
                                                    '<tr id=tr'+json[key]['product_id']+'>' +
                                                    '<td>' + json[key]['product_id'] + '</td>' +
                                                    '<td>' + json[key]['product_name'] + '</td>' +
                                                    '<td>' + json2[0]['current_reading'] + '</td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')">'+json2[0]['min_copy']+'</td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                     '</tr>'
                                                );
                                            }else{
                                                $("#products").append
                                                (
                                                    '<tr id=tr'+json[key]['product_id']+'>' +
                                                    '<td>'+json[key]['product_id']+'</td>' +
                                                    '<td>'+json[key]['product_name']+'</td>' +
                                                    '<td>0</td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '<td contenteditable="true" onblur="calc($(this),'+json[key]['product_id']+')"></td>' +
                                                    '</tr>'
                                                );
                                            }

                                        }
                                    });

                            });

                        }
                    });
                }
            });
        }







    function miscFields()
    {

        $("#misc_fields").append('<tr><td><div class="md-input-wrapper"><input type="text" placeholder="Description" autocomplete="off" class="md-input " id="net_amount" name="net_amount" value=""><span class="md-input-bar "></span></div></td><td style="max-width:100px;"><div class="uk-input-group"><div class="md-input-wrapper"><input placeholder="Value" type="text" class="md-input"><span class="md-input-bar "></span></div><span class="uk-input-group-addon"><a onclick="removeMiscFields($(this))"><i class="material-icons" >close</i></a></span></div></td></tr>');
    }

    function removeMiscFields(elm)
    {
        elm.closest("tr").remove();
    }


    function calc(elm,e) {
        if (!isNaN(elm.text())) {
            var table = $("#products");
            var previous_reading = table.find('#tr' + e).find('td').eq(2);
            var current_reading = table.find('#tr' + e).find('td').eq(3);
            var quantity = table.find('#tr' + e).find('td').eq(4);
            var rate = table.find('#tr' + e).find('td').eq(5);
            var amount = table.find('#tr' + e).find('td').eq(6);
            var other = table.find('#tr' + e).find('td').eq(7);
            var net_amount = table.find('#tr' + e).find('td').eq(8);
            var min_copy = table.find('#tr' + e).find('td').eq(9);
            var amount_remaining_copy = table.find('#tr' + e).find('td').eq(10);
            var total_amount = table.find('#tr' + e).find('td').eq(11);


            quantity.text((parseFloat(current_reading.text()) - parseFloat(previous_reading.text())).toFixed(2));


            amount.text((parseFloat(quantity.text()) * parseFloat(rate.text())).toFixed(2));
            net_amount.text((parseFloat(amount.text()) + parseFloat(other.text())).toFixed(2));
            amount_remaining_copy.text(0);

            if (parseInt(quantity.text()) < parseInt(min_copy.text())) {
                var a =    parseInt(min_copy.text())-parseInt(quantity.text()) ;

                var b = (parseFloat(rate.text()) * a).toFixed(2);
                amount_remaining_copy.text(b);
            } else {
                amount_remaining_copy.text(0);
            }

            if (amount_remaining_copy.text() != 0) {
                var total = (parseFloat(amount_remaining_copy.text()) + parseFloat(net_amount.text()));
            } else {
                var total = parseInt(net_amount.text());
            }

            total_amount.text(total);
        }else{

            elm.text(null);

        }
    }

    function  calcAmount() {
        var final_amount     = 0;
        var rows = $("#products > tr").length;
        for ( var i =0 ; i<rows;  i++)
        {
            final_amount  += parseInt($("#products").find('tr').eq(i).find('td').eq(11).text());
            console.log(final_amount);
            $("#final_amount").val(final_amount);
        }
    }


    
    function create() {

var valid = 0 ;

        $('.md-card').find('input[type="text"]').each(function () {
            if (!$(this).val()) {
                valid = 0;
            } else {
                valid = 1;
            }
        });



        if(valid ==1 ) {


            var misc_fields_lenght = $("#misc_fields> tr").length;
            var products_lenght = $("#products> tr").length;

            var sale_id = $("#sale_id").val();
            var final_amount = $("#final_amount").val();
            var service_tax = $("#sale_tax_per").val();
            var date = $("#date").val();


            $.ajax({
                url: "rental.sale.reading.create",
                type: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    sale_id: sale_id,
                    final_amount: final_amount,
                    service_tax: service_tax,
                    date: date,
                    invoice_no: $('#invoice_no').val(),
                },
                beforeSend: function () {

                },
                success: function (e) {
                    console.log("----------: Data Saved Successfully  :----------");
                    console.log(e);
                    var table = $("#products");
                    for ($i = 0; $i < products_lenght; $i++) {

                        var product_id = table.find('tr').eq($i).find('td').eq(0).text();
                        var previous_reading = table.find('tr').eq($i).find('td').eq(2).text();
                        var current_reading = table.find('tr').eq($i).find('td').eq(3).text();
                        var quantity = table.find('tr').eq($i).find('td').eq(4).text();
                        var rate = table.find('tr').eq($i).find('td').eq(5).text();
                        var amount = table.find('tr').eq($i).find('td').eq(6).text();
                        var other = table.find('tr').eq($i).find('td').eq(7).text();
                        var net_amount = table.find('tr').eq($i).find('td').eq(8).text();
                        var min_copy = table.find('tr').eq($i).find('td').eq(9).text();
                        var amount_remaining_copy = table.find('tr').eq($i).find('td').eq(10).text();
                        var total_amount = table.find('tr').eq($i).find('td').eq(11).text();


                        $.ajax({
                            url: "rental.sale.reading.product.log.create",
                            type: "POST",
                            data: {
                                _token: '{{csrf_token()}}',
                                sale_id: sale_id,
                                product_id: product_id,
                                current_reading: current_reading,
                                previous_reading: previous_reading,
                                quantity: quantity,
                                rate: rate,
                                amount: amount,
                                other: other,
                                net_amount: net_amount,
                                min_copy: min_copy,
                                amount_remaining_copy: amount_remaining_copy,
                                total_amount: total_amount,
                                invoice_no: $('#invoice_no').val(),
                            },
                            complete: function (e) {
                                console.log("----------: Data Saved Successfully  :----------");
                                console.log(e);
                            }

                        });
                    }

                    for ($i = 0; $i < misc_fields_lenght; $i++) {

                        var field_name = $("#misc_fields").find('tr').eq($i).find('td').eq(0).find('div').find('input').val();
                        var field_amount = $("#misc_fields").find('tr').eq($i).find('td').eq(1).find('div').find('input').val();
                        $.ajax({
                            url: "rental.sale.reading.misc.log.create",
                            type: "POST",
                            data: {
                                _token: '{{csrf_token()}}',
                                sale_id: sale_id,
                                field_name: field_name,
                                field_amount: field_amount,
                            },
                            complete: function (e) {
                                console.log("----------: Data Saved Successfully  :----------");
                                console.log(e);
                            }

                        });
                    }

                    // console.log(e);

                },
                complete: function () {
                    window.location="rental.sale.reading.create.success.{{$sale_id}}";
                }
            });

        }
        else{
            alert("all fields are required !");
        }


    }
    
    

    </script>

@endsection

