@extends('Inc.app')

@section('content')


    <h2 class="text-light">New Receivable</h2>
    <hr>


    <div class="md-card">

        <div class="md-card-content" style="display: block;">
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-6-10 uk-push-2-10 uk-row-first">
                    <form action="customer.payable.create" method="post">

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif

                        {{csrf_field()}}
                        <div style="width: 70%;margin-left: 15%">
                        </div>
                        <table style="width: 100%">
                            <tbody>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Customer</td>
                                <td>
                                    <select title="" id="customer_id" data-md-selectize name="customer_id" onchange="getCustomerInfo($(this).val())" >
                                        <option selected>Select...</option>
                                        @foreach($customer as $i)
                                            <option data-contact="{{$i->contact}}" value="{{$i->id}}">{{$i->company_name}}</option>
                                        @endforeach

                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Contact#</td>
                                <td>
                                    <div class="md-input-wrapper"><input  disabled type="text" autocomplete="off" class="md-input md-input-small" id="contact" name="contact" ><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Payment Mod</td>
                                <td>
                                    <select title="" id="payment_mode" class="md-input" name="payment_mode" onchange="paymentMod(this.value)" >
                                        <option selected>Select...</option>
                                        <option value="cash">Cash</option>
                                        <option value="cheque">Cheque</option>
                                        <option value="online transfer">Online Transfer</option>
                                        <option value="fund transfer">Fund Transfer</option>
                                        <option value="pay order">Pay Order</option>
                                        <option value="direct deposit">Direct Deposit</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Total Payable</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="total_payable" name="total_payable" value="{{old('total_payable')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>



                            <tr hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Bank Name</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="bank_name" name="bank_name" value="{{old('bank_name')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Branch Name</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="branch_name" name="branch_name" value="{{old('contact_person')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>


                            <tr hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Account Title</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="account_title" name="account_title" value="{{old('account_title')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>


                            <tr hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Cheque No.</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="cheque_no" name="cheque_no" value="{{old('cheque_no')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>


                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Amount Paid </td>
                                <td>
                                    <div class="md-input-wrapper"><input onkeyup="$('#remain_balance').val($('#total_payable').val() - $('#amount_paid').val())" type="text" autocomplete="off" class="md-input md-input-small" id="amount_paid" name="amount_paid" value="{{old('amount_paid')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr >
                                <td style="padding:15px 0;max-width: 50px; ">Remaining Balance</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="remain_balance" name="remain_balance" value="{{old('remain_balance')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Received As</td>
                                <td>
                                    <select title="" id="account_id" class="md-input" name="account_id" onchange="paymentMod(this.value)" >
                                        <option selected>Select...</option>
                                        @foreach($bank as $i)
                                            <option value="{{$i->id}}">{{$i->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>



                            </tbody>
                        </table>
                        <div class="uk-form-row">
                            <div class="uk-width-1-1 uk-margin-top">
                                <button type="submit" name="save" class="md-btn md-btn-primary uk-float-right">Create</button>
                                <a href="" class="md-btn md-btn uk-float-right">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>

    </div>





        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>


    <script>
        function getCustomerInfo(id){
            $.ajax({
                url:"get.customer.info",
                type:"POST",
                data:{
                    _token: '{{csrf_token()}}',
                    customer_id:id
                },
                beforeSend:function () {
                    $("#contact").val("");
                    $("#total_payable").val("");
                },
                complete:function (e) {
                    console.log(e);
                    if(e.responseText.length > 10) {
                        var json = JSON.parse(e.responseText);
                        $("#contact").val(json[0]['customer_contact']);
                        $("#total_payable").val(json[0]['balance']);
                    }
                }
            });
        }

        function  paymentMod(e) {

            if(e=="cash"){

                $('table').find('tr').eq(4).prop('hidden',true);
                $('table').find('tr').eq(5).prop('hidden',true);
                $('table').find('tr').eq(6).prop('hidden',true);
                $('table').find('tr').eq(7).prop('hidden',true);
            }else if(e=="cheque"){
                $('table').find('tr').eq(4).prop('hidden',false);
                $('table').find('tr').eq(5).prop('hidden',false);
                $('table').find('tr').eq(6).prop('hidden',false);
                $('table').find('tr').eq(7).prop('hidden',false);
            }else if(e=="online transfer" || e=="fund transfer"){
                $('table').find('tr').eq(4).prop('hidden',false);
                $('table').find('tr').eq(5).prop('hidden',false);
                $('table').find('tr').eq(6).prop('hidden',false);
                $('table').find('tr').eq(7).prop('hidden',true);
            }else if(e=="pay order" || e=="direct deposit"){
                $('table').find('tr').eq(4).prop('hidden',false);
                $('table').find('tr').eq(5).prop('hidden',true);
                $('table').find('tr').eq(6).prop('hidden',true);
                $('table').find('tr').eq(7).prop('hidden',true);
            }

        }

    </script>

@endsection

