@extends('Inc.app')

@section('content')

    <h2 class="text-light">Product List</h2>
    <hr>


    <div class="md-card">
        <div class="md-card-toolbar" style="display: none">
        <div class="md-card-toolbar-actions">
            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
            <i class="md-icon material-icons md-card-toggle"  >&#xE316;</i>
        </div>
        <h3 class="md-card-toolbar-heading-text">
            All Items
        </h3>
    </div>
        <div class="md-card-content">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    <div class="uk-alert uk-alert-success" data-uk-alert="">
                        <a href="#" class="uk-alert-close uk-close"></a>
                        {{ session()->get('message') }}
                    </div>
                </div>
            @endif
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Check</th>
                   <th>Product Code</th>
                    <th>Product Name</th>
                    
                    <th>Brand</th>
                    <th>Category</th>
                    <th>Quantity</th>
                    
                </tr>
                </thead>

                

               <tbody  id="productList">
                   @foreach ($table as $i)

                       <tr id="tr{{$i->id}}">
                           <td><input type="checkbox" onchange="checkbox(this,{{$i->id}})" id="{{$i->id}}"></td>
                           <td>{{$i->pro_code}}</td>
                           <td>{{$i->name}}</td>
                           
                           <td>{{$i->brandName}}</td>
                           <td>{{$i->categoryName}}</td>
                           <td>{{$i->product_quantity}}</td>
                          

                       </tr>

                   @endforeach
               </tbody>
            </table>
            
        </div>
    </div>



      <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    
                   <th>Product Code</th>
                    <th>Product Name</th>
                    <th>Brand</th>
                    <th>Quantity</th>
                    <th>Add</th>
                    <th>Subtract</th>
                    <th>Remove</th>
                    
                </tr>
                </thead>

                

               <tbody id="selectedProduct">
              
               </tbody>
          
            </table>
        <button class="btn btn-success pull-right adjust">Adjust</button>


<div id="test"></div>
        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>

    <script>
        function destroy(id,name) {
            var r = confirm("Delete " + name);
            if (r == true) {
                window.location="product.delete."+id;
            }
        }
    </script>
   
   <script>


     

     
     
     
            function checkbox(x,y) {
                   if( $(x).is(':checked')){
                       
                       
                       var proCode   =  $("#tr"+y).find('td').eq(1).text();
                   
                    var   proName =  $("#tr"+y).find('td').eq(2).html();
                    var proPrice =  $("#tr"+y).find('td').eq(3).html();
                        var proBrand =  $("#tr"+y).find('td').eq(4).html();


                       $("#selectedProduct").append(
                           '<tr id="selectedProduct'+y+'">' +
                           '<td>'+proCode+'</td>' +
                           '<td>'+proName+'</td>' +
                        
                            '<td>'+proBrand+'</td>' +
                           '<td><input type="number" value="0" id="'+y+'" class="quanPro" style="max-width:80px"></td>' +
                           '<td><input type="checkbox" name="chk[]" value="add" > </td>'+
                           '<td><input type="checkbox" name="chk[]" value="subtract" > </td>'+
                           '<td><a  class="fa fa-remove" onclick="removeEditRow(this)"></a></td>'+
                           '</td>'
                       );



                   }else{
                       $('#selectedProduct'+y).remove();
                   }
                }
     
     
  function removeEditRow(e) {
                    e.closest("tr").remove()
                }
 


</script>
<script>
$(document).ready(function(){
    
    $('.adjust').click(function(){
        
         var id = [];
       var quantity = [];
        var chk = [] ;
       
        
      $('.quanPro').each(function(e){
           
           id[e] = $(this).attr('id');
           quantity[e] = $(this).val();
       });
        
               
$("input[name='chk[]']:checked").each(function ()
{
    chk.push($(this).val());
});
        
        
      
  
  $.post('AdjustStock.manage',{id:id,quantity:quantity,chk:chk, _token:'{{csrf_token()}}'},function(data){
      
      
      
     location.reload();
     
  });
     
      
        
        
    });
    
    
    
});



</script>
     

















@endsection

 