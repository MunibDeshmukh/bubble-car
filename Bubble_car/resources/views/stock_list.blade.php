@extends('Inc.app')

@section('content')
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">

    <div class="md-card light-bg">
        <div class="md-card-toolbar" >
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons ">add</i>
            </div>
            <h3 class="md-card-toolbar-heading-text">
                Stock
            </h3>
        </div>
        <div class="md-card-content" >
            <div id="msg"></div>
            <div class="uk-grid" data-uk-grid-margin="">

                <div class="uk-width-medium-1-1 uk-row-first">
                    <table id="dt_tableExport" class="uk-table table-bordered table-responsive table-hover">
                       <thead>
                       <tr>
                           <th>Gr  No </th>
                           <th>Voucher No </th>
                          <th>Product Name</th>
                          <th>Product Quantity</th>
                          <th>Total price</th>

                       </tr>
                       </thead>
                       <tbody id="itemsTable">

                       @foreach($table as $i)

                           <tr>
                            <td>{{$i->gr_no}}</td>
                               <td>{{$i->voucher_no}}</td>
                               <td>{{$i->product_name}}</td>
                               <td>{{$i->product_quantity}}</td>
                               <td>{{$i->product_price}}</td>
                              
                           </tr>
                       @endforeach

                       </tbody>



                   </table>

                </div>
            </div>



        </div>

    </div>

@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>




@endsection

