@extends('Inc.window')

@section('content')
    <?PHP
    $product_total   = 0;
    $misc_total      = 0;
    $total           = 0;
    $copyDiff        = 0;
    $total_copyDiff  = 0;
    $total_tax       = 0;
    $total_misc      = 0;
    ?>
    <style>
        th{
            font-weight: bold !important;
        }
    </style>
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    <table class="table ">

        <tbody><tr>

            <td colspan="2">
                <div style="border-bottom: 1px solid black;border-top: 1px solid black;">
                    <b>KHAN BUSINESS PRODUCTS</b><br>Ph. # 36364027, 36364037, 36802400-1<br>Fax # 36802400 Cell: 0333-210261-3<br>khanbusinessproducts@yahoo.com
                </div>
            </td>
            <td colspan="3" style="text-align: center"><b style="font-size: 36px">KHAN's</b></td>
            <td colspan="2">
                <div style="border-bottom: 1px solid black;border-top: 1px solid black;">
                    Anwar-ul-Uloom, SP-10, Block-10,<br>Gulberg, F. B. Area Adj, Gulberg <br>Police Station Karachi<br>
                    E-mail:khanbusinessproducts@gmail.com
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <h3 style="text-align: center"><u>RENTAL SALE READING VOUCHER</u></h3>
    <table style="margin-top: 20px;">
        <tr><td style="width: 150px;">Invoice No.</td> <td>{{$table->voucher_no}}</td></tr>
        <tr><td>Company Name</td> <td>{{$table->company_name}}</td></tr>
        <tr><td>Contact Person Name</td> <td>{{$table->person_name}}</td></tr>
    </table>


                    <table id="dt_tableExort" class="uk-table table-bordered table-responsive table-hover" >
                       <thead>
                       <tr>
                           <th>Product</th>
                           <th>Previous</th>
                           <th>Current</th>
                           <th>Quantity</th>
                           <th>Rate</th>
                           <th>Amount</th>
                           <th>Others</th>
                           <th>NetAmount</th>

                       </tr>
                       </thead>

                       <tbody id="itemsTable">

                            @foreach($products as $i)
                                @if($i->net_amount !=0)
                                    <?php $product_total += $i->net_amount?>
                           <tr>
                               <td>{{$i->product_name}}</td>
                               <td>{{$i->previous_reading}}</td>
                               <td>{{$i->current_reading}}</td>
                               <td>{{$i->quantity}}</td>
                               <td>{{$i->rate}}</td>
                               <td>{{$i->amount}}</td>
                               <td>{{$i->others}}</td>
                               <td>{{$i->net_amount}}</td>
                           </tr>
                           @endif
                            @endforeach

                       </tbody>
                   </table>

    <table style="width: 100%" class="uk-table table-bordered table-responsive table-hover" >
        <thead>
        <tr>
            <th style="text-align: center"><h3 style="text-align: center">MISC BILLING</h3></th>
            <th style="width: 100px;"></th>
        </tr>
        <tr>
            <th style="text-align: center">Description</th>
            <th style="width: 100px;">Amount</th>
        </tr>
        </thead>
        <tbody style="text-align: center">


        @foreach($products as $i)
            @if($i->total_amount !=0)

            <tr>
                <td >MINIMUM {{$i->min_copy}} COPIES (DIFF COPIES @if(intval($i->quantity) < $i->min_copy ) {{$copyDiff=$i->min_copy -$i->quantity   }} @else 0  @endif ) </td>
                <td>@if(intval($i->quantity) < $i->min_copy ) {{$total_copyDiff=round($copyDiff*$i->rate)}}  @else 0  @endif </td>
            </tr>
            @endif
        @endforeach
        <tr>
            <td >SRB NO: {{$table->srb}} SALES TAX ON SERVICES {{$table->service_tax}}%</td>
            <td>{{$tax = round((($product_total + $total_copyDiff)/100)*$table->service_tax)}}</td>
        </tr>

        @foreach($misc as $i)
            <tr>
                <td>{{$i->field_name}}</td>
                <td>{{$i->field_amount}}</td>
            </tr>

            <?php $total_misc+= $i->field_amount; ?>

        @endforeach



        <tr>
            <th style="text-align: right">TOTAL</th>
            <th style="text-align: center">{{$qw = round($tax + $product_total + $total_misc  + $total_copyDiff)}}</th>
        </tr>
        <tr class="hidden-print">
            <td colspan="4" class="etxt">
                <button onclick="window.print();" class="uk-button-primary   uk-button uk-align-center uk-float-right"><i class="material-icons md-light no_margin">print</i></button>
                <button onclick="window.close();" class="uk-button-danger uk-button uk-align-center uk-float-right"><i class="material-icons md-light no_margin">close</i></button>
            </td>

        </tr>
        </tbody>
    </table>





@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>


<script>

    $(document).ready(function () {



    });
    
</script>
    

@endsection

