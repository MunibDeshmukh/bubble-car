@extends('Inc.window')

@section('content')
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    <table style="margin-top: 20px;">
        {{-- <tr><td style="width: 150px;">Invoice No.</td> <td>{{$voucher_no}}</td></tr> --}}
        <tr><td>Company Name</td> <td>{{$company_name}}</td></tr>
        <tr><td>Contact Person Name</td> <td>{{$person_name}}</td></tr>
    </table>

                    <table style="width: 100%" class="uk-table table-bordered table-responsive table-hover" >
                       <thead>
                       <tr>
                           <th>Product</th>
                           <th>Rate</th>
                           <th>Quantity</th>
                           <th>Amount</th>
                       </tr>
                       </thead>
                       <tbody id="itemsTable">



                       @foreach($table as $i)

                           <tr id="unSel{{$i->id}}">
                               <td>{{$i->product_name}}</td>
                               <td>{{$i->price}}</td>
                               <td>{{$i->item_quantity}}</td>
                               <td>{{$i->amount}}</td>
                           </tr>

                       @endforeach
                       </tbody>
                   </table>

    <table style="width: 100%" class="uk-table table-bordered table-responsive table-hover" >
        <thead>
        <tr>
            <th style="text-align: center"><h3 style="text-align: center">MISC BILLING</h3></th>
            <th style="width: 100px;"></th>
        </tr>
        <tr>
            <th style="text-align: center">Description</th>
            <th style="width: 100px;">Amount</th>
        </tr>
        </thead>
        <tbody style="text-align: center">

        <tr>
            <td >Tax </td>
            <td>{{$tax}}</td>
        </tr>
       






        <tr>
            <th style="text-align: right">TOTAL</th>
            <th>{{$total_amount}}</th>

        </tr>
        <tr class="hidden-print">
            <td colspan="4" class="etxt">
                <button onclick="window.print();" class="uk-button-primary   uk-button uk-align-center uk-float-right"><i class="material-icons md-light no_margin">print</i></button>
                <button onclick="window.close();" class="uk-button-danger uk-button uk-align-center uk-float-right"><i class="material-icons md-light no_margin">close</i></button>
            </td>

        </tr>
        </tbody>
    </table>




@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>




@endsection

