@extends('Inc.app')

@section('content')


    <div class="md-card">
        <div class="md-card-toolbar">
        <div class="md-card-toolbar-actions">
            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
            <i class="md-icon material-icons "  >add</i>
        </div>
        <h3 class="md-card-toolbar-heading-text">
            Expense Receivable List
        </h3>
    </div>
        <div class="md-card-content">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    <div class="uk-alert uk-alert-success" data-uk-alert="">
                        <a href="#" class="uk-alert-close uk-close"></a>
                        {{ session()->get('message') }}
                    </div>
                </div>
            @endif
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Head Name</th>
                    <th>Amount</th>
                    <th>Date</th>
                </tr>
                </thead>



               <tbody>
                   @foreach ($table as $i)

                       <tr>
                           <td>{{$i->id}}</td>
                           <td>{{$i->head_name}}</td>
                           <td>{{$i->amount}}</td>
                           <td>{{$i->created_at}}</td>




                       </tr>


                   @endforeach
               </tbody>

            </table>
        </div>
    </div>


        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>

    <script>
        function destroy(id,name) {
            var r = confirm("Delete " + name);
            if (r == true) {
                window.location="product.delete."+id;
            }
        }
    </script>

@endsection

