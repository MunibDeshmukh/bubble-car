@extends('Inc.app')

@section('content')
    <style>
        #productRows input {padding: 4px;width: 100%;}
        .md-card .md-card-content {
            padding: 5px !important;
        }
        #product_search{
            max-height: 200px;
            height: auto;
            overflow: scroll;
            overflow-x: hidden;
            position: absolute;
            background: white;
            width: 100%;
            margin-left: 1px;
            -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
            box-shadow: 0 1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
        }
        #product_search ul {width: 100%;}
        #product_search ul li{padding:0 2px;cursor: pointer;border-left: 5px solid white }
        #product_search ul li:hover{color: royalblue;border-left: 5px solid royalblue;}



    </style>
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">

        <h2 class="text-light">Rental Sale <b style="text-transform: capitalize"></b> </h2>
    <hr>



    <div class="md-card light-bg">
        <div class="md-card-toolbar" >
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-toggle" ></i>
            </div>
        </div>

        <div class="md-card-content form" >
            <div class="md-card-content" >
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-1 uk-row-first">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                        <div class="uk-grid">
                            <div class="uk-width-medium-2-5">
                                <div class="parsley-row">
                                    <select title="" id="customer_id" class="md-input">
                                        <option value="empty"  selected>Select Vendors...</option>
                                        @foreach($customers as $i)
                                            <option value="{{$i->id}}">{{$i->company_name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>



                            <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <div class="md-input-wrapper"><label>Sale Date</label><input type="text" class="md-input" id="date" data-uk-datepicker="{format:'YYYY-MM-DD'}"><span class="md-input-bar"></span></div>

                                </div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                <select title="" id="type" class="md-input">
                                    <option value hidden  selected>Rental Type...</option>
                                    <option value="reading"  >Reading</option>
                                    <option value="quantity"  >Quantity</option>

                                </select>
                            </div>

                        </div>

                    </div>
                </div>

            </div>





        </div>

        <div class="md-card-content" >
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-1-1 uk-row-first">
                    <table style="width: 100%" class="uk-table table-bordered" >
                        <thead>
                        <tr style="text-align: center" >
                            <td>Product</td>
                            <td>Opening Reading</td>
                            <td>Minimum Copies</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody id="productRows">

                        </tbody>


                    </table>
                </div>
                <div class="uk-width-medium-1-1 uk-margin-top">
                    <table style="width: 100%">
                        <tr>
                            <td style="position: relative;padding-left: 7px;max-width: 50px;width: auto">
                                <input type="text" style="padding: 4px;width: 100%" onkeyup="productSearch(this.value)" placeholder="Search Here...">
                                <div id="product_search">
                                    <ul style="list-style: none;" id="product_list">

                                    </ul>
                                </div>
                            </td>
                            <td width="30" style="padding-left: 20px"><i class="material-icons"  data-uk-tooltip="{pos:'right'}" title="Live search for products !" style="cursor:pointer;">info</i></td>
                            <td><button onclick="create()" id="create" class="uk-button uk-button-primary " style="float: right">Save</button></td>
                        </tr>
                    </table>
                </div>


            </div>

        </div>





        <br>

    </div>



@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>



        <script>

            function create() {

                var isValid = true;
                $("#productRows").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });

                $(".form").find('select').each(function() {
                    if ($(this).val() == "empty") {
                        isValid = false;
                    }
                });

                $(".form").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });


                if(isValid==true) {

                    var qty =  $("#productRows > tr").length;
                    var tr = $("#productRows");
                    $.ajax({
                        url:"rental.sale.create",
                        type:"POST",
                        data:{
                            _token   :'{{csrf_token()}}',
                            customer :$('#customer_id').val(),
                            date     :$('#date').val(),
                            type     :$('#type').val()
                        },
                        beforeSend:function () {
                            $("#create").prop('disabled')
                        },
                        success:function (e) {

                            for($i=0;$i<qty;$i++) {
                                var i_id       = tr.find('tr').eq($i).find('td').eq(0).find('input').data('id');
                                var i_reading    = tr.find('tr').eq($i).find('td').eq(1).find('input').val();
                                var i_copy      = tr.find('tr').eq($i).find('td').eq(2).find('input').val();
                                $.ajax({
                                    url : "rental.sale.create.log",
                                    type: "POST",
                                    data: {
                                        _token: '{{csrf_token()}}',
                                        id      : i_id,
                                        reading : i_reading,
                                        copy :  i_copy,
                                        type:$('#type').val()
                                    },
                                    beforeSend: function () {
                                        console.log("-----------------------------------");
                                    },
                                    complete: function (e) {
                                        console.log(e);
                                    }
                                });
                            }

                            console.log(e);


                        }
                        ,complete:function (e) {
                            window.location="rental.sale.create.success";
                        }
                    });

                }else{
                    alert("Please fill all files first!")
                }



            }



            function productSearch(e) {
                $.ajax({
                    url:"product.by_name",
                    type:"POST",
                    data: {
                        _token:'{{csrf_token()}}',
                        q:e
                    },
                    complete:function (e){
//                        console.log(e);
                        var json = JSON.parse(e.responseText);
                        $("#product_list").html(null);
                        $.each(json, function(i, item) {
                            var name =item['name'];
                            $("#product_list").append("<li onclick='productRow("+item['id']+")'>"+name+"</li>");
                        });
                    }
                })
            }


            function productRow(e) {
                if ($("#productRows").find("#selectedRow" + e).html() == null) {
                    var tr = "tr";
                    $.ajax({
                        url: "product.by_id",
                        type: "POST",
                        data: {
                            _token: '{{csrf_token()}}',
                            q: e
                        },
                        complete: function (e) {
//                        console.log(e);
                            var json = JSON.parse(e.responseText);
                            $.each(json, function (i, item) {
//                            console.log(item['name']);

                                $("#productRows").append(
                                    '<tr id="selectedRow'+item['id']+'">' +
                                    '<td><input onkeyup="productRowCalc($(this))" type="text"   value="' + item['name'] + '" data-id="'+item['id']+'" ></td>' +
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" value="' + item['sailing_price']+'"></td>' +
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="Quantity"></td>'+
                                    '<td style="width: 30px !important;"><input type="button" value="X" onclick="productRowRemove($(this))"></td>'+
                                    '</tr>'
                                );

                            });
                        }
                    })
                }
            }

            function productRowRemove(elm)
            {
                elm.closest('tr').remove();
            }



        </script>

@endsection

