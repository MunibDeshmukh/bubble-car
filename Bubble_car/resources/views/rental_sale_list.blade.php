@extends('Inc.app')

@section('content')
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">

        <h2 class="text-light">Rental Sale List <b style="text-transform: capitalize"></b> </h2>
    <hr>



    <div class="md-card light-bg">
        <div class="md-card-toolbar" style="display: none;">
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-toggle" id="m1" style="display: none; opacity: 1; transform: scale(1);"></i>
            </div>
        </div>
        <div class="md-card-content" >
            <div id="msg"></div>
            <div class="uk-grid" data-uk-grid-margin="">

                <div class="uk-width-medium-1-1 uk-row-first">
                    <table id="dt_tableExport" class="uk-table table-bordered table-responsive table-hover">
                       <thead>
                       <tr>
                           <th>#</th>
                           <th>Company Name</th>
                           <th>Contact Person</th>
                           <th>Rental Type</th>
                           <th>Add New</th>
                           <th>View Details</th>

                       </tr>
                       </thead>
                       <tbody id="itemsTable">

                       @foreach($table as $i)

                           <tr id="unSel{{$i->id}}">
                               <td>{{$i->id}}</td>
                               <td>{{$i->company_name}}</td>
                               <td>{{$i->person_name}}</td>
                               <td >
                                   @if($i->rental_type=="reading")
                                       <li><span style="text-transform: capitalize" class="uk-badge uk-badge-primary">{{$i->rental_type}}</span></li>
                                   @endif

                                   @if($i->rental_type=="quantity")
                                       <li><span style="text-transform: capitalize" class="uk-badge uk-badge-info">{{$i->rental_type}}</span></li>
                                   @endif
                               <td>
                                   @if($i->rental_type=="reading")
                                       <li><a href="rental.sale.reading.create.{{$i->id}}" class="md-btn-icon uk-margin"><i class="material-icons no_margin">add_circle</i></a> </li>
                                   @endif

                                   @if($i->rental_type=="quantity")
                                       <li><a href="rental.sale.quantity.create.{{$i->id}}" class="md-btn-icon uk-margin"><i class="material-icons no_margin">add_circle</i></a> </li>
                                   @endif

                               </td>

                               <td>
                                   @if($i->rental_type=="reading")
                                       <li><a href="rental.sale.reading.list.{{$i->id}}" class="md-btn-icon uk-margin"><i class="material-icons no_margin">list_alt</i></a> </li>
                                   @endif

                                   @if($i->rental_type=="quantity")
                                       <li><a href="rental.sale.quantity.list.{{$i->id}}" class="md-btn-icon uk-margin"><i class="material-icons no_margin">list_alt</i></a> </li>
                                   @endif

                               </td>


                           </tr>
                       @endforeach

                       </tbody>



                   </table>

                </div>
            </div>



        </div>

    </div>

@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>



        <script>

            $(document).ready(function () {
                $('.md-card-collapsed').css('display','Inherit');
            });

            function viewlogs(i) {
                window.open("rental.sale.logs.view."+i,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=550,height=650')
            }

            function destroy(id,name) {
                var r = confirm("Delete " + name);
                if (r == true) {
                    window.location="gst_sale.delete."+id;
                }
            }



        </script>

@endsection

