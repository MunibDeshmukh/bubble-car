@extends('Inc.app')

@section('content')
    <div class="md-card"  style="background: rgba(255,255,255,0.9)">
        <div class="md-card-toolbar">
            <div class="md-card-toolbar-actions">
                <a href="vendor.create" style="color: inherit">Add New</a>
            </div>
            <h3 class="md-card-toolbar-heading-text">
                All Vendors
            </h3>
        </div>
        <div class="md-card-content">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    <div class="uk-alert uk-alert-success" data-uk-alert="">
                        <a href="#" class="uk-alert-close uk-close"></a>
                        {{ session()->get('message') }}
                    </div>
                </div>
            @endif
            <div class="dt_colVis_buttons"></div>
            <table id="dt_tableExport" class="uk-table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Vendor Name</th>
                    <th>Person Name</th>
                    <th>Contact#</th>
                    <th>Email</th>
                    <th>CNIC</th>
                    <th>Address</th>
                    <th>Opening Balance</th>
                    <th>Action</th>
                    
                   
                </tr>
                </thead>



               <tbody>
                   @foreach ($table as $i)

                       <tr>
                           <td>{{$i->id}}</td>
                           <td>{{$i->company_name}}</td>
                           <td>{{$i->person_name}}</td>
                           <td>{{$i->contact}}</td>
                           <td>{{$i->email}}</td>
                           <td>{{$i->cnic}}</td>
                           <td>{{$i->address}}</td>
                           
                           <td>{{$i->opening_balance}}</td>
                          
                           <td>
                               <a href="outSourcevendor.edit.{{$i->id}}" style="" class=" md-btn-icon"><i class="uk-icon-edit no_margin"></i></a>
                               <a href="#" onclick="destroy('{{$i->id}}')" style="" class=" md-btn-icon"><i class="uk-icon-remove no_margin"></i></a>
                           </td>

                       </tr>

                   @endforeach
               </tbody>
            </table>
        </div>
    </div>


        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>

    <script>
        function destroy(id) {
           
                window.location="outSourcevendor.delete."+id;
            
        }

        $(document).ready(function () {
            $("")
        })
    </script>

@endsection

