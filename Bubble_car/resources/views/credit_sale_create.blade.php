@extends('Inc.app')

@section('content')
    <style>
      #productRows input {padding: 4px;width: 100%;}
      .md-card .md-card-content {
          padding: 5px !important;
      }
        #product_search{
            max-height: 200px;
            height: auto;
            overflow: scroll;
            overflow-x: hidden;
            position: absolute;
            background: white;
            width: 100%;
            margin-left: 1px;
            -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
            box-shadow: 0 1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
        }
      #product_search ul {width: 100%;}
      #product_search ul li{padding:0 2px;cursor: pointer;border-left: 5px solid white }
      #product_search ul li:hover{color: royalblue;border-left: 5px solid royalblue;}



    </style>
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    <div class="md-card light-bg">
        <div class="md-card-toolbar" >
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-toggle" id="m1" style="display: none; opacity: 1; transform: scale(1);"></i>
            </div>
        </div>

        <div class="md-card-content form" >
            <div class="md-card-content" >
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-1 uk-row-first">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <select title="" id="vendor_id" class="md-input">
                                        <option value="empty"  selected>Select Customer...</option>
                                        @foreach($customer as $i)
                                            <option value="{{$i->id}}">{{$i->person_name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                             <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <div class="md-input-wrapper md-input-filled"><label>Voucher No.</label><input readonly type="text" class="md-input" id="voucher_no" required="" value="{{$voucher}}"><span class="md-input-bar"></span></div>
                                </div>
                            </div>

                             <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <select title="" id="tax" class="md-input" >
                                        <option value="empty"  selected>Select Tax...</option>
                                        <option value="0" data-id="0,0">None</option>
                                        @foreach($tax as $i)
                                            <option data-id="{{$i->amount}},{{$i->name}}" value="{{$i->amount}}">{{$i->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                             <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <select title="" id="tax_action" class="md-input">
                                        <option value="empty"  selected>Tax Action</option>
                                        <option value="0">Addition</option>
                                        <option value="1">Subtraction</option>
                                    </select>

                                </div>
                            </div>
                           


                            <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <div class="md-input-wrapper"><label>Sale Date</label><input type="text" class="md-input" id="date" data-uk-datepicker="{format:'YYYY-MM-DD'}"><span class="md-input-bar"></span></div>

                                </div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                <select title="" id="tax_as" class="md-input">
                                    <option value="empty" selected>Tax Detect As...</option>
                                    <option value="1"  >Percentage</option>
                                    <option value="0"  >Number</option>

                                </select>
                            </div>
                            
                             <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <select title="" id="vendor_id" class="md-input carType">
                                        <option value="empty"  selected>Select car Type</option>
                                        @foreach($car as $i)
                                            <option value="{{$i->type}}">{{$i->type}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            
                            
                                <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <select title="" id="vendor_id" class="md-input carName">
                                        <option value="empty"  selected>Select car</option>
                                       
                                           
                                        
                                    </select>

                                </div>
                            </div>
                          
                            
                        </div>

                    </div>
                </div>

            </div>





        </div>
        
        
        <br>
        
        
        
             <div class="row">
        
             <div class="col-sm-6">
                    
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" class="btn btn-default products">Products</button>
  <button type="button" class="btn btn-secondary services">Service</button>
  <button type="button" class="btn btn-secondary outsourceservices">Out Source Service </button>
</div>
            
            </div>
            </div>
        <br>

            <div class="md-card-content productsTab">
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-1 uk-row-first">
                        <table style="width: 100%" class="uk-table table-bordered" >
                            <thead>
                            <tr style="text-align: center" >
                                <td>Product</td>
                                
                                <td>Unit</td>
                                <td>Price</td>
                                <td>Quantity</td>
                                <td>Amount</td>
                                
                                <td>Tax</td>
                                <td>Total</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="productRows">

                            </tbody>

                            <tfoot style="text-align: center">
                            <tr >
                                <td></td>
                                <td></td>
                                <td></td>
                                
                                <td>Total</td>
                                <td id="amount"></td>
                                
                                <td id="tax_amount"></td>
                                <td id="total"></td>
                                <td></td>
                            </tr>
                            </tfoot>


                        </table>
                    </div>
                    <div  class="uk-width-medium-1-1 uk-margin-top">
                        <table style="width: 100%">
                            <tr>
                                <td style="position: relative;padding-left: 7px;max-width: 50px;width: auto">
                                    <input type="text" id="productSearch" style="padding: 4px;width: 100%" onkeyup="productSearch(this.value)" placeholder="Search Here...">
                                    <div id="product_search">
                                        <ul style="list-style: none;" id="product_list">

                                        </ul>
                                    </div>
                                </td>
                                <td width="30" style="padding-left: 20px"><i class="material-icons"  data-uk-tooltip="{pos:'right'}" title="Live search for products !" style="cursor:pointer;">info</i></td>
                                <td><button onclick="create()" id="create" class="uk-button uk-button-primary " style="float: right">Save</button>
                                <button style="float: right; margin-right:10px;" class="uk-button uk-button-primary ">Generate Invoice</button>
                                </td>
                                
                            </tr>
                        </table>
                </div>


                </div>

            </div>


       <div class="md-card-content servicesTab">
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-1 uk-row-first">
                        <table style="width: 100%" class="uk-table table-bordered serviceTbl">
                            <thead>
                            <tr style="text-align: center" >
                                <td>Service</td>
                               <td>Price</td>
                                <td>Quantity</td>
                                <td>Amount</td>
                                
                                <td>Tax</td>
                                <td>Total</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="servicesRows">

                            </tbody>

                            <tfoot style="text-align: center">
                            <tr >
                                <td></td>
                                <td></td>
                                <td></td>
                                

                                <td id="service_amount"></td>
                                
                                <td id="service_tax_amount"></td>
                                <td id="service_total"></td>
                                <td></td>
                            </tr>
                            </tfoot>


                        </table>
                    </div>
                    <div class="uk-width-medium-1-1 uk-margin-top">
                        <table style="width: 100%">
                            <tr>
                                <td style="position: relative;padding-left: 7px;max-width: 50px;width: auto">
                                    <input type="text" style="padding: 4px;width: 100%" onkeyup="servicesSearch(this.value)" placeholder="Search Here...">
                                    <div id="product_search">
                                        <ul style="list-style: none;" id="services_list">

                                        </ul>
                                    </div>
                                </td>
                                <td width="30" style="padding-left: 20px"><i class="material-icons"  data-uk-tooltip="{pos:'right'}" title="Live search for Services !" style="cursor:pointer;">info</i></td>
                                <td><button onclick="servicescreate()" id="servicescreate" class="uk-button uk-button-primary " style="float: right">Save</button>
                                <button style="float: right; margin-right:10px;" class="uk-button uk-button-primary ">Generate Invoice</button>
                                </td>
                            </tr>
                        </table>
                </div>


                </div>

            </div>
        
        
        
       <div class="md-card-content outsourceTab">
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-1 uk-row-first">
                        <table style="width: 100%" class="uk-table table-bordered outsourceTbl">
                            <thead>
                            <tr style="text-align: center" >
                                <td>Out Source Service</td>
                               <td>Price</td>
                                <td>Quantity</td>
                                <td>Amount</td>
                                
                                <td>Tax</td>
                                <td>Total</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="outsourceRows">

                            </tbody>

                            <tfoot style="text-align: center">
                            <tr >
                                <td></td>
                                <td></td>
                                <td></td>
                                

                                <td id="outsource_amount"></td>
                                
                                <td id="outsource_tax_amount"></td>
                                <td id="outsource_total"></td>
                                <td></td>
                            </tr>
                            </tfoot>


                        </table>
                    </div>
                    <div class="uk-width-medium-1-1 uk-margin-top">
                        <table style="width: 100%">
                            <tr>
                                <td style="position: relative;padding-left: 7px;max-width: 50px;width: auto">
                                    <input type="text" style="padding: 4px;width: 100%" onkeyup="outsourceSearch(this.value)" placeholder="Search Here...">
                                    <div id="product_search">
                                        <ul style="list-style: none;" id="outsource_list">

                                        </ul>
                                    </div>
                                </td>
                                <td width="30" style="padding-left: 20px"><i class="material-icons"  data-uk-tooltip="{pos:'right'}" title="Live search for Services !" style="cursor:pointer;">info</i></td>
                                <td><button onclick="outsourcecreate()" id="outsourcecreate" class="uk-button uk-button-primary " style="float: right">Save</button>
                                <button style="float: right; margin-right:10px;" class="uk-button uk-button-primary ">Generate Invoice</button>
                                </td>
                            </tr>
                        </table>
                </div>


                </div>

            </div>


<br>

    </div>






        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>


        <script>



            function create() {
                // alert($('#voucher_no').val());

                var isValid = true;
                $("#productRows").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });

                $(".form").find('select').each(function() {
                    if ($(this).val() == "empty") {
                        isValid = false;
                    }
                });

                $(".form").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });
            

                if(isValid==true) {

                var qty =  $("#productRows > tr").length;
                var tr = $("#productRows");
                var tax_type=$('#tax option:selected').text();
                var tax_value=$('#tax').val();
                var tax_action=$('#tax_action').val();
                var tax_as=$('#tax_as').val();
                
                $.ajax({
                    url:"creditsaleProduct.create",
                    type:"POST",
                    data:{
                        _token   :'{{csrf_token()}}',
                        qty      :qty,
                       
                        amount   :$("#amount").text(),
                        total    :$("#total").text(),
                        date     :$("#date").val(),
                        carType  :$(".carType").val(),
                        carName  :$(".carName").val(),
                        tax      :tax_value,
                        tax_type      :tax_type,
                        tax_action      :tax_action,
                        tax_as      :tax_as,
                        // gd_no    :$("#gd_no").val(),
                        // gd_date  :$("#gd_date").val(),
                        // bl_date  :$("#bl_date").val(),
                        // bl_no    :$("#bl_no").val(),
                        voucher  :$("#voucher_no").val(),
                        vendor   :$('#vendor_id').val(),
                        // container:$('#container').val()
                    },
                    beforeSend:function () {
                        $("#create").prop('disabled')


                    },
                    success:function (e) {

                        for($i=0;$i<qty;$i++) {

                            var i_id       = tr.find('tr').eq($i).find('td').eq(0).find('input').data('id');
                            var i_price    = tr.find('tr').eq($i).find('td').eq(2).find('input').val();
                            var i_qty      = tr.find('tr').eq($i).find('td').eq(3).find('input').val();
                            var i_amount   = tr.find('tr').eq($i).find('td').eq(4).find('input').val();
                            
                            var i_gst      = tr.find('tr').eq($i).find('td').eq(5).find('input').val();
                            var i_total    = tr.find('tr').eq($i).find('td').eq(6).find('input').val();

                            $.ajax({
                                url : "creditsaleProduct.create.log",
                                type: "POST",
                                data: {
                                    _token: '{{csrf_token()}}',
                                    id  : i_id,
                                    qty : i_qty,
                                    price :  i_price,
                                    amount : i_amount,
                                    
                                    tax:i_gst,
                                    total:i_total
                                },
                                beforeSend: function () {
                                    console.log("");
                                },
                                complete: function (e) {
                                    console.log(e);
                                }
                            });
                        }

                        console.log(e);


                    }
                    ,complete:function (e) {
                        
                  window.location="creditsales.create";
                    }
                });

                }else{
                    alert("Please fill all files first!")
                }



            }

            
              function servicescreate() {
                // alert($('#voucher_no').val());

               var isValid = true;
                $("#productRows").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });

                $(".form").find('select').each(function() {
                    if ($(this).val() == "empty") {
                        isValid = false;
                    }
                });

                $(".form").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });
            
            
                if(isValid==true) {

                var qty =  $("#servicesRows > tr").length;
                var tr = $("#servicesRows");
                var tax_type=$('#tax option:selected').text();
                var tax_value=$('#tax').val();
                var tax_action=$('#tax_action').val();
                var tax_as=$('#tax_as').val();
                
                $.ajax({
                    url:"creditsaleProduct.create",
                    type:"POST",
                    data:{
                        _token   :'{{csrf_token()}}',
                        qty      :qty,

                        amount   :$("#service_amount").text(),
                        total    :$("#service_total").text(),
                        date    :$("#date").val(),
                        carType  :$(".carType").val(),
                        carName  :$(".carName").val(),
                        tax      :tax_value,
                        tax_type      :tax_type,
                        tax_action      :tax_action,
                        tax_as      :tax_as,
                        // gd_no    :$("#gd_no").val(),
                        // gd_date  :$("#gd_date").val(),
                        // bl_date  :$("#bl_date").val(),
                        // bl_no    :$("#bl_no").val(),
                        voucher  :$("#voucher_no").val(),
                        vendor   :$('#vendor_id').val(),
                        // container:$('#container').val()
                    },
                    beforeSend:function () {
                        $("#servicescreate").prop('disabled')


                    },
                    success:function (e) {

                        for($i=0;$i<qty;$i++) {

                            var i_id       = tr.find('tr').eq($i).find('td').eq(0).find('input').data('id');
                            var i_price    = tr.find('tr').eq($i).find('td').eq(1).find('input').val();
                            var i_qty      = tr.find('tr').eq($i).find('td').eq(2).find('input').val();
                            var i_amount   = tr.find('tr').eq($i).find('td').eq(3).find('input').val();
                            
                            var i_tax      = tr.find('tr').eq($i).find('td').eq(4).find('input').val();
                            var i_total    = tr.find('tr').eq($i).find('td').eq(5).find('input').val();

                            $.ajax({
                                url : "creditsaleService.create.log",
                                type: "POST",
                                data: {
                                    _token: '{{csrf_token()}}',
                                    id  : i_id,
                                    qty : i_qty,
                                    price :  i_price,
                                    amount : i_amount,
                                    
                                    tax:i_tax,
                                    total:i_total
                                },
                                beforeSend: function () {
                                    console.log("-----------------------------------");
                                },
                                complete: function (e) {
                                    console.log(e);
                                }
                            });
                        }

                        console.log(e);


                    }
                    ,complete:function (e) {
                        
                     window.location="creditsales.create";
                    }
                });

                }else{
                    alert("Please fill all files first!")
                }



            }
            
               function outsourcecreate() {
                // alert($('#voucher_no').val());

               var isValid = true;
                $("#productRows").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });

                $(".form").find('select').each(function() {
                    if ($(this).val() == "empty") {
                        isValid = false;
                    }
                });

                $(".form").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });
            
            
                if(isValid==true) {

                var qty =  $("#outsourceRows > tr").length;
                var tr = $("#outsourceRows");
                var tax_type=$('#tax option:selected').text();
                var tax_value=$('#tax').val();
                var tax_action=$('#tax_action').val();
                var tax_as=$('#tax_as').val();
                
                $.ajax({
                    url:"creditsaleProduct.create",
                    type:"POST",
                    data:{
                        _token   :'{{csrf_token()}}',
                        qty      :qty,

                        amount   :$("#outsource_amount").text(),
                        total    :$("#outsource_total").text(),
                        date    :$("#date").val(),
                        carType  :$(".carType").val(),
                        carName  :$(".carName").val(),
                        tax      :tax_value,
                        tax_type      :tax_type,
                        tax_action      :tax_action,
                        tax_as      :tax_as,
                        // gd_no    :$("#gd_no").val(),
                        // gd_date  :$("#gd_date").val(),
                        // bl_date  :$("#bl_date").val(),
                        // bl_no    :$("#bl_no").val(),
                        voucher  :$("#voucher_no").val(),
                        vendor   :$('#vendor_id').val(),
                        // container:$('#container').val()
                    },
                    beforeSend:function () {
                        $("#servicescreate").prop('disabled')


                    },
                    success:function (e) {

                        for($i=0;$i<qty;$i++) {

                            var i_id       = tr.find('tr').eq($i).find('td').eq(0).find('input').data('id');
                            var i_price    = tr.find('tr').eq($i).find('td').eq(1).find('input').val();
                            var i_qty      = tr.find('tr').eq($i).find('td').eq(2).find('input').val();
                            var i_amount   = tr.find('tr').eq($i).find('td').eq(3).find('input').val();
                            
                            var i_tax      = tr.find('tr').eq($i).find('td').eq(4).find('input').val();
                            var i_total    = tr.find('tr').eq($i).find('td').eq(5).find('input').val();

                            $.ajax({
                                url : "creditsaleoutsource.create.log",
                                type: "POST",
                                data: {
                                    _token: '{{csrf_token()}}',
                                    id  : i_id,
                                    qty : i_qty,
                                    price :  i_price,
                                    amount : i_amount,
                                    
                                    tax:i_tax,
                                    total:i_total
                                },
                                beforeSend: function () {
                                    console.log("-----------------------------------");
                                },
                                complete: function (e) {
                                    console.log(e);
                                }
                            });
                        }

                        console.log(e);


                    }
                    ,complete:function (e) {
                        
                    window.location="creditsales.create";
                    }
                });

                }else{
                    alert("Please fill all files first!")
                }



            }
          
            

            function productSearch(e) {
                $.ajax({
                    url:"product.by_name",
                    type:"POST",
                    data: {
                        _token:'{{csrf_token()}}',
                         q:e
                    },
                    complete:function (e){
//                        console.log(e);
                        var json = JSON.parse(e.responseText);
                        $("#product_list").html(null);
                        $.each(json, function(i, item) {
                        var name =item['name'];
                            $("#product_list").append("<li onclick='productRow("+item['id']+")'>"+name+"</li>");
                        });
                    }
                })
            }
            
            
       function servicesSearch(e) {    
                $.ajax({
                    url:"services.by_name",
                    type:"POST",
                    data: {
                        _token:'{{csrf_token()}}',
                         q:e
                    },
                    complete:function (e){
//                        console.log(e);
                        var json = JSON.parse(e.responseText);
                        $("#services_list").html(null);
                        $.each(json, function(i, item) {
                        var name =item['ServiceName'];
                            $("#services_list").append("<li onclick='servicesRows("+item['id']+")'>"+name+"</li>");
                        });
                    }
                })
            }
            
            
              function outsourceSearch(e) {    
                $.ajax({
                    url:"outsource.by_name",
                    type:"POST",
                    data: {
                        _token:'{{csrf_token()}}',
                         q:e
                    },
                    complete:function (e){
//                        console.log(e);
                        var json = JSON.parse(e.responseText);
                        $("#outsource_list").html(null);
                        $.each(json, function(i, item) {
                        var name =item['serviceName'];
                            $("#outsource_list").append("<li onclick='outsourceRows("+item['id']+")'>"+name+"</li>");
                        });
                    }
                })
            }
            
                       function outsourceRows(e) {
                if ($("#outsourceRows").find("#selectedRow" + e).html() == null) {
                    var tr = "tr";
                    $.ajax({
                        url: "outsource.by_id",
                        type: "POST",
                        data: {
                            _token: '{{csrf_token()}}',
                            q: e
                        },
                        complete: function (e) {
//                        console.log(e);
                            var json = JSON.parse(e.responseText);
                            $.each(json, function (i, item) {
//                            console.log(item['name']);

                                $("#outsourceRows").append(
                                    '<tr id="selectedRow'+item['id']+'">' +
                                    '<td><input  type="text"   value="' + item['serviceName'] + '" data-id="'+item['id']+'" ></td>' +
                                   
                                    '<td><input  type="text" onkeyup="outsourcequan($(this))"    placeholder="Price"></td>'+
                                    '<td><input onkeyup="outsourcequan($(this))" type="number" value="0" placeholder="Quantity"></td>'+
                                    '<td><input  type="number" placeholder="Amount"></td>'+
                                    
                                    '<td><input  type="number" onkeyup="outsourcequan($(this))" value="0" placeholder="Tax" id="xx" ></td>'+
                                    '<td><input  type="number" placeholder="Total Amount"></td>' +
                                    '<td style="width: 30px !important;"><input type="button" value="X" onclick="productRowRemove($(this))"></td>'+
                                    '<td hidden><input type="number" ></td>'+//hidden field for text amount
                                    
                                    '</tr>'
                                );

                            });
                            
                        }
                    })
                }
            }
            
            
            
            
                        function servicesRows(e) {
                if ($("#servicesRows").find("#selectedRow" + e).html() == null) {
                    var tr = "tr";
                    $.ajax({
                        url: "services.by_id",
                        type: "POST",
                        data: {
                            _token: '{{csrf_token()}}',
                            q: e
                        },
                        complete: function (e) {
//                        console.log(e);
                            var json = JSON.parse(e.responseText);
                            $.each(json, function (i, item) {
//                            console.log(item['name']);

                                $("#servicesRows").append(
                                    '<tr id="selectedRow'+item['id']+'">' +
                                    '<td><input  type="text"   value="' + item['ServiceName'] + '" data-id="'+item['id']+'" ></td>' +
                                   
                                    '<td><input  type="text"  value="' + item['small'] + '"  placeholder="Price"></td>'+
                                    '<td><input onkeyup="servicequan($(this))" type="number" value="0" placeholder="Quantity"></td>'+
                                    '<td><input  type="number" placeholder="Amount"></td>'+
                                    
                                    '<td><input  type="number" onkeyup="servicequan($(this))" value="0" placeholder="Tax" id="xx" ></td>'+
                                    '<td><input  type="number" placeholder="Total Amount"></td>' +
                                    '<td style="width: 30px !important;"><input type="button" value="X" onclick="productRowRemove($(this))"></td>'+
                                    '<td hidden><input type="number" ></td>'+//hidden field for text amount
                                    
                                    '</tr>'
                                );

                            });
                            
                        }
                    })
                }
            }

            function productRow(e){
                if ($("#productRows").find("#selectedRow" + e).html() == null) {
                    var tr = "tr";
                    $.ajax({
                        url: "product.by_id",
                        type: "POST",
                        data: {
                            _token: '{{csrf_token()}}',
                            q: e
                        },
                        complete: function (e) {
//                        console.log(e);
                            var json = JSON.parse(e.responseText);
                            $.each(json, function (i, item) {
//                            console.log(item['name']);

                                $("#productRows").append(
                                    '<tr id="selectedRow'+item['id']+'">' +
                                    '<td><input onkeyup="productRowCalc($(this))" type="text"   value="' + item['name'] + '" data-id="'+item['id']+'" ></td>' +
                                    '<td><input onkeyup="productRowCalc($(this))" type="text" value="' + item['unit']+'" readonly></td>' +
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="Price"></td>'+
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="Quantity"></td>'+
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="Amount"></td>'+
                                    
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="Tax" id="xx" ></td>'+
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="Total Amount"></td>' +
                                    '<td style="width: 30px !important;"><input type="button" value="X" onclick="productRowRemove($(this))"></td>'+
                                    '<td hidden><input type="number" ></td>'+//hidden field for text amount
                                    
                                    '</tr>'
                                );

                            });
                            
                        }
                    })
                }
            }

            function productRowRemove(elm)
            {
                elm.closest('tr').remove();
            }

            // function gettax(amount){
            //     $('#xx').val(amount);
            // }

            function productRowCalc(elm)
            {

                var tax=$('#tax').val();

                var sign = $("#tax_as").val();
                var action = $("#tax_action").val();

                var tr   = elm.closest('tr');

                var unit = parseFloat(tr.find('td').eq(4).find('input').val()) / 100;

                tr.find('td').eq(4).find('input').val(Math.ceil(tr.find('td').eq(2).find('input').val()) * parseFloat(tr.find('td').eq(3).find('input').val()));

                if(sign==1){
                    if(tax=='empty'){
                       tr.find('td').eq(5).find('input').val(0);
                    }else{
                      tr.find('td').eq(5).find('input').val(tax);
                    }
                    var amount = unit * parseFloat(tr.find('td').eq(5).find('input').val());
                    tr.find('td').eq(8).find('input').val(amount)
                }else {
                     if(tax=='empty'){
                       tr.find('td').eq(5).find('input').val(0);
                    }else{
                      tr.find('td').eq(5).find('input').val(tax);
                    }
                    
                        tr.find('td').eq(8).find('input').val(tr.find('td').eq(5).find('input').val());
                   
                        // tr.find('td').eq(8).find('input').val(tr.find('td').eq(5).find('input').val());
                    
                    
                }
                // if(sign==1) {
                //     var amount = unit * parseFloat(tr.find('td').eq(5).find('input').val());
                //     tr.find('td').eq(10).find('input').val(amount)
                // }else{tr.find('td').eq(10).find('input').val(tr.find('td').eq(5).find('input').val());}
                // if(sign==1) {
                //     var amount = unit * parseFloat(tr.find('td').eq(6).find('input').val());
                //     tr.find('td').eq(11).find('input').val(amount)
                // }else{tr.find('td').eq(11).find('input').val(tr.find('td').eq(6).find('input').val());}
                if(action==0){
                     tr.find('td').eq(6).find('input').val(parseFloat(tr.find('td').eq(4).find('input').val()) +
                    parseFloat(tr.find('td').eq(8).find('input').val()))
                }else if(action==1){
                    tr.find('td').eq(6).find('input').val(parseFloat(tr.find('td').eq(4).find('input').val()) -
                    parseFloat(tr.find('td').eq(8).find('input').val()))
                }
                // + parseFloat(tr.find('td').eq(10).find('input').val()) +
                //     parseFloat(tr.find('td').eq(11).find('input').val()));
               totalCalc();
            }
           
            
            
                  function servicequan(elm)
            {
                

                var sign = $("#tax_as").val();
                var action = $("#tax_action").val();

                var tr   = elm.closest('tr');

          if(action==0){

             if(sign==1){
                 var tax =  tr.find('td').eq(4).find('input').val();
                 var amount = tr.find('td').eq(3).find('input').val();

                 var persentage = parseInt(amount) * parseInt(tax) / 100;
                 var total  = parseInt(amount) + parseInt(persentage);


                 tr.find('td').eq(5).find('input').val(total);

             }else if(sign==0){
                 var tax =  tr.find('td').eq(4).find('input').val();
                 var amount = tr.find('td').eq(3).find('input').val();

                 var total = parseInt(amount) + parseInt(tax) ;



                 tr.find('td').eq(5).find('input').val(total);

                    }





                }else if(action==1){
                    if(sign==1){
                        var tax =  tr.find('td').eq(4).find('input').val();
                        var amount = tr.find('td').eq(3).find('input').val();

                        var persentage = parseInt(amount) * parseInt(tax) / 100;
                        var total  = parseInt(amount) - parseInt(persentage);


                        tr.find('td').eq(5).find('input').val(total);

                    }else if(sign==0){
                        var tax =  tr.find('td').eq(4).find('input').val();
                        var amount = tr.find('td').eq(3).find('input').val();

                        var total = parseInt(amount) - parseInt(tax) ;



                        tr.find('td').eq(5).find('input').val(total);

                    }



                }





                var price = tr.find('td').eq(1).find('input').val();
                var quantity = tr.find('td').eq(2).find('input').val();
                var amount = quantity * price;
                tr.find('td').eq(3).find('input').val(amount);


           var amountshow = 0;
           var taxshow = 0;
           var totalshow = 0;
            
               $('.serviceTbl').find("tr").children("td:nth-child(4)").find('input')
                    .each(function() {
                        $this = $(this);
                        amountshow += parseFloat($this.val());});
                   $('.serviceTbl').find("tr").children("td:nth-child(5)").find('input')
                    .each(function() {
                        $this = $(this);
                        taxshow += parseFloat($this.val());});
                   $('.serviceTbl').find("tr").children("td:nth-child(6)").find('input')
                    .each(function() {
                        $this = $(this);
                        totalshow += parseFloat($this.val());});

                $("#service_amount").text(amountshow);

                $("#service_tax_amount").text(taxshow);
                $("#service_total").text(totalshow);



                
            }
            
            
               function outsourcequan(elm)
            {
                

                var sign = $("#tax_as").val();
                var action = $("#tax_action").val();

                var tr   = elm.closest('tr');

          if(action==0){

             if(sign==1){
                 var tax =  tr.find('td').eq(4).find('input').val();
                 var amount = tr.find('td').eq(3).find('input').val();

                 var persentage = parseInt(amount) * parseInt(tax) / 100;
                 var total  = parseInt(amount) + parseInt(persentage);


                 tr.find('td').eq(5).find('input').val(total);

             }else if(sign==0){
                 var tax =  tr.find('td').eq(4).find('input').val();
                 var amount = tr.find('td').eq(3).find('input').val();

                 var total = parseInt(amount) + parseInt(tax) ;



                 tr.find('td').eq(5).find('input').val(total);

                    }





                }else if(action==1){
                    if(sign==1){
                        var tax =  tr.find('td').eq(4).find('input').val();
                        var amount = tr.find('td').eq(3).find('input').val();

                        var persentage = parseInt(amount) * parseInt(tax) / 100;
                        var total  = parseInt(amount) - parseInt(persentage);


                        tr.find('td').eq(5).find('input').val(total);

                    }else if(sign==0){
                        var tax =  tr.find('td').eq(4).find('input').val();
                        var amount = tr.find('td').eq(3).find('input').val();

                        var total = parseInt(amount) - parseInt(tax) ;



                        tr.find('td').eq(5).find('input').val(total);

                    }



                }





                var price = tr.find('td').eq(1).find('input').val();
                var quantity = tr.find('td').eq(2).find('input').val();
                var amount = quantity * price;
                tr.find('td').eq(3).find('input').val(amount);


           var amountshow = 0;
           var taxshow = 0;
           var totalshow = 0;
            
               $('.outsourceTbl').find("tr").children("td:nth-child(4)").find('input')
                    .each(function() {
                        $this = $(this);
                        amountshow += parseFloat($this.val());});
                   $('.outsourceTbl').find("tr").children("td:nth-child(5)").find('input')
                    .each(function() {
                        $this = $(this);
                        taxshow += parseFloat($this.val());});
                   $('.outsourceTbl').find("tr").children("td:nth-child(6)").find('input')
                    .each(function() {
                        $this = $(this);
                        totalshow += parseFloat($this.val());});

                $("#outsource_amount").text(amountshow);

                $("#outsource_tax_amount").text(taxshow);
                $("#outsource_total").text(totalshow);



                
            }
            
            
            
            
            function totalCalc()
            {
                var sign = $("#tax_as").val();
                var table = $("#productRows");
                var amount           = 0;
               
                var gst          = 0;
                var total            = 0;
                table.find("tr").children("td:nth-child(5)").find('input')
                    .each(function() {
                        $this = $(this);
                        amount += parseFloat($this.val());});

              

                table.find("tr").children("td:nth-child(9)").find('input')
                    .each(function() {
                        $this = $(this);
                        gst += parseFloat($this.val());});

                table.find("tr").children("td:nth-child(7)").find('input')
                    .each(function() {
                        $this = $(this);
                        total += parseInt($this.val());});
                $("#amount").text(Math.ceil(amount));
              
                $("#tax_amount").text(Math.ceil(gst));
                $("#total").text(Math.ceil(total));
            }



    </script>
<script>

$(document).ready(function(){
    
    $('.servicesTab').hide();
    $('.outsourceTab').hide();
    
    
$('.services').click(function(){
   
    $(this).removeClass('btn btn-secondary');
    $('.products').removeClass('btn btn-default');
    $('.outsourceservices').removeClass('btn btn-default');
    
    $(this).addClass('btn btn-default');
    $('.products').addClass('btn btn-secondary');
    $('.outsourceservices').addClass('btn btn-secondary');
    $('.productsTab').fadeOut(1000);
    $('.outsourceTab').fadeOut(1000);
    $('.servicesTab').fadeIn(1000);
    
}); 
    
    $('.products').click(function(){
   
    $(this).removeClass('btn btn-secondary');
    $('.services').removeClass('btn btn-default');
    $('.outsourceservices').removeClass('btn btn-default');
    
    $(this).addClass('btn btn-default');
    $('.services').addClass('btn btn-secondary');
    $('.outsourceservices').addClass('btn btn-secondary');
    $('.productsTab').fadeIn(1000);
    $('.servicesTab').fadeOut(1000);
         $('.outsourceTab').fadeOut(1000);
    
}); 
    
   $('.outsourceservices').click(function(){
   
    $(this).removeClass('btn btn-secondary');
    $('.services').removeClass('btn btn-default');
    $('.products').removeClass('btn btn-default');
    
    $(this).addClass('btn btn-default');
    $('.services').addClass('btn btn-secondary');
    $('.products').addClass('btn btn-secondary');
    $('.productsTab').fadeOut(1000);
    $('.servicesTab').fadeOut(1000);
    $('.outsourceTab').fadeIn(1000);
}); 
    
    
    $('.carType').change(function(){
        
        var val = $(this).val();
        
        $.post('getcar.bytype',{val:val,_token:'{{csrf_token()}}'},function(data){
            
            $('.carName').html(data);
        })
        
        
    });
    
    
    
    
  
    
    
    

});





</script>


@endsection

