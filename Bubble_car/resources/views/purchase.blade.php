@extends('Inc.app')

@section('content')
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">

        <h2 class="text-light">Purchase <b style="text-transform: capitalize"></b> </h2>
    <hr>



    <div class="md-card light-bg">
        <div class="md-card-toolbar" style="display: none;">
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-toggle" id="m1" style="display: none; opacity: 1; transform: scale(1);"></i>
            </div>
        </div>
        <div class="md-card-content" >
            <div id="msg"></div>
            <div class="uk-grid" data-uk-grid-margin="">

                <div class="uk-width-medium-1-1 uk-row-first">
                    <table id="dt_tableExport" class="uk-table table-bordered table-responsive table-hover">
                       <thead>
                       <tr>
                           <th>#</th>
                           <th>Product Name</th>
                           <th>Sailing Price</th>
                           <th>Class</th>
                           <th>Brand</th>
                           <th>Model</th>
                           <th>Type</th>
                           <th>Category</th>
                           <th>Location</th>
                           <th>Warehouse</th>
                           <th>Action</th>
                       </tr>
                       </thead>
                       <tbody id="itemsTable">

                       @foreach($table as $i)

                           <tr id="unSel{{$i->id}}">
                               <td>{{$i->id}}</td>
                               <td>{{$i->name}}</td>
                               <td>{{$i->sailing_price}}</td>
                               <td>{{$i->class_name}}</td>
                               <td>{{$i->brand_name}}</td>
                               <td>{{$i->model_name}}</td>
                               <td>{{$i->type_name}}</td>
                               <td>{{$i->category_name}}</td>
                               <td>{{$i->location_name}}</td>
                               <td>{{$i->warehouse_name}}</td>

                               <td>
                                   <input title="" type="checkbox" onchange="mkSelect(this,'{{$i->id}}')" data-switchery data-switchery-color="#673ab7"  id="switch_demo_primary" />
                               </td>
                           </tr>
                       @endforeach

                       </tbody>



                   </table>

                    <div class="uk-form-row">
                        <div class="uk-width-1-1 uk-margin-top">
                            <button  type="button" onclick="proceed()" class="md-btn md-btn-primary uk-float-right">Continue</button>

                        </div>
                    </div>
                </div>
            </div>



        </div>

    </div>

    <div class="md-card md-card-collapsed light-bg" style="display: none;">
        <div class="md-card-toolbar" style="display: none;">
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-toggle" id="m2" style="display: none; opacity: 1; transform: scale(1);"></i>
            </div>
        </div>
        <div class="md-card-content" >
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-8-10 uk-push-1-10 uk-row-first">
                    <table class="uk-table table-bordered uk-border-rounded table-responsive table-hover" id="tableSelected">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Sailing Price</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                            <th>Duty Tax(#)</th>
                            <th>Duty Tax(%)</th>
                            <th>Gst Tax(%)</th>
                            <th>Net Amount</th>
                        </tr>
                        </thead>
                        <tbody id="itemsSelected">



                        </tbody>

                    </table>

                    <div class="uk-form-row">
                        <div class="uk-width-1-1 uk-margin-top">
                            <button  type="button" onclick="proceed()" class="md-btn uk-float-right">Back</button>
                            <button  type="button" onclick="afSelect();proceed2();" class="md-btn md-btn-primary uk-float-right">Continue</button>

                        </div>
                    </div>
                </div>
            </div>



        </div>

    </div>


    <div class="md-card md-card-collapsed light-bg" style="display: none;">
        <div class="md-card-toolbar" style="display: none;">
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-toggle" id="m3" style="display: none; opacity: 1; transform: scale(1);"></i>
            </div>
        </div>
        <div class="md-card-content" >
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-6-10 uk-push-2-10  uk-row-first">
                    <table class="uk-table table- table-bordered table-responsive table-hover">

                        <tbody>

                        <tr>
                            <td style="padding: 20px 10px;">Select Vendor</td>
                            <td>
                                <select title="" id="vendor_id" data-md-selectize name="unit"  style="width: 150px">
                                    <option selected>Select...</option>
                                    @foreach($vendor as $i)
                                        <option value="{{$i->id}}">{{$i->company_name}}</option>
                                    @endforeach
                                </select>

                            </td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">Total Items</td>
                            <td style="padding: 20px 10px;" id="items_quantity" ></td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">Total Amount</td>
                            <td style="padding: 20px 10px;" id="t_amount" ></td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">Total Duty Tax(#)</td>
                            <td style="padding: 20px 10px;" id="t_duty_no" ></td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">Total Duty Tax(%)</td>
                            <td style="padding: 20px 10px;" id="t_duty" ></td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">Total Gst Tax(%)</td>
                            <td style="padding: 20px 10px;" id="t_gst" ></td>
                        </tr>



                        <tr>
                            <td style="padding: 20px 10px;">Total Net Amount</td>
                            <td contenteditable="false" style="padding: 20px 10px;"  id="t_net_amount"></td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">No. of Containers</td>
                            <td contenteditable="true" style="padding: 20px 10px;"  id="no_container"></td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">GD Number</td>
                            <td contenteditable="true" style="padding: 20px 10px;"  id="gd_no"></td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">GD Date</td>
                            <td  style=";position: relative"  >
                                <input readonly
                                       style="border: none;position: absolute;width: 100%;height: 100%;background: rgba(000,000,000,0)"
                                       title=""  type="text" id="gd_date" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                            </td>
                        </tr>


                        <tr>
                            <td style="padding: 20px 10px;">BL Number</td>
                            <td contenteditable="true" style="padding: 20px 10px;"  id="bl_no"></td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">BL Date</td>
                            <td  style=";position: relative"  >
                                <input readonly
                                       style="border: none;position: absolute;width: 100%;height: 100%;background: rgba(000,000,000,0)"
                                       title=""  type="text" id="bl_date" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                            </td>
                        </tr>

                        <tr>
                            <td style="padding: 20px 10px;">Date of Purchase</td>
                            <td  style=";position: relative"  >
                                <input readonly
                                       style="border: none;position: absolute;width: 100%;height: 100%;background: rgba(000,000,000,0)"
                                       title=""  type="text" id="purchase_date" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                            </td>
                        </tr>

                        </tbody>

                    </table>
                    <div class="uk-form-row">
                        <div class="uk-width-1-1 uk-margin-top">
                            <button  type="button" onclick="proceed2()" class="md-btn uk-float-right">Back</button>
                            <button  type="submit" id="create" onclick="create()" class="md-btn md-btn-primary uk-float-right">Create</button>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>





        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>



        <script>

            $(document).ready(function () {
                $('.md-card-collapsed').css('display','Inherit');
            });

            function mkSelect(x,y)
            {
                if( $(x).is(':checked'))
                {
                    var id   =  $("#unSel"+y).find('td').eq(0).html();
                    var name =  $("#unSel"+y).find('td').eq(1).html();
                    var unit =  $("#unSel"+y).find('td').eq(2).html();
                    $("#itemsSelected").append
                    (
                        '<tr id="sel'+y+'">' +
                        '<td>'+id+'</td>' +
                        '<td>'+name+'</td>' +
                        '<td>'+unit+'</td>' +
                        '<td contenteditable="true" ></td>'+
                        '<td contenteditable="false" onclick="cal($(this))" ></td>'+
                        '<td contenteditable="true"  onclick="cal($(this))" ></td>'+
                        '<td contenteditable="true"  onclick="cal($(this))" ></td>'+
                        '<td contenteditable="true"  onclick="cal($(this))" ></td>'+
                        '<td contenteditable="true"  onclick="cal($(this))" ></td>'+
                        '<td contenteditable="false" onclick="cal($(this))" ></td>'+
                        '</td>'
                    );
                }else{
                    $('#sel'+y).remove();
                }
            }


            function cal(elm)
            {
                elm.closest('tr').find('td').eq(4).html(parseInt(parseFloat(elm.closest('tr').find('td').eq(2).html()) * parseFloat(elm.closest('tr').find('td').eq(3).html())));
                var duty_no =parseInt(parseInt(elm.closest('tr').find('td').eq(4).html()) + parseInt(elm.closest('tr').find('td').eq(5).html()));
                var duty =parseInt(parseFloat(elm.closest('tr').find('td').eq(4).html()) / 100 * parseFloat(elm.closest('tr').find('td').eq(6).html()));
                var gst =parseInt(parseFloat(elm.closest('tr').find('td').eq(4).html()) / 100 * parseFloat(elm.closest('tr').find('td').eq(7).html()));
                elm.closest('tr').find('td').eq(8).html(parseInt(duty_no)+parseInt(duty)+parseInt(gst));

            }

            function afSelect()
            {
               $("#items_quantity").text(document.getElementById("itemsSelected").childElementCount);
            }

            function proceed()
            {
                $("#m1").click();
                $("#m2").click();
            }

            function proceed2()
            {
                $("#m2").click();
                $("#m3").click();
                totalCalc();
                taxCalc();
            }


            function create() {

                var qty=$("#items_quantity").text();

                $.ajax({
                    url:"purchase.create",
                    type:"POST",
                    data:{
                        _token     :'{{csrf_token()}}',
                        vendor_id  :$('#vendor_id').val(),
                        items_qty  :qty,
                        amount  :$("#t_amount").text(),
                        duty_no  :$("#t_duty_no").text(),
                        duty  :$("#t_duty").text(),
                        gst  :$("#t_gst").text(),
                        net_amount  :$("#t_net_amount").text(),
                        container  :$("#no_container").text(),
                        gd_no  :$("#gd_no").text(),
                        gd_date  :$("#gd_date").val(),
                        bl_no  :$("#bl_no").text(),
                        bl_date  :$("#bl_date").val(),
                        purchase_date  :$("#purchase_date").val()
                    },
                    beforeSend:function () {


                    },
                    complete:function (e) {

                        for($i=0;$i<qty;$i++) {

                            var i_id      = $("#itemsSelected").find('tr').eq($i).find('td').eq(0).text();
                            var i_price     = $("#itemsSelected").find('tr').eq($i).find('td').eq(2).text();
                            var i_qty  = $("#itemsSelected").find('tr').eq($i).find('td').eq(3).text();
                            var i_amount  = $("#itemsSelected").find('tr').eq($i).find('td').eq(4).text();
                            var i_duty_no  = $("#itemsSelected").find('tr').eq($i).find('td').eq(5).text();
                            var i_duty  = $("#itemsSelected").find('tr').eq($i).find('td').eq(6).text();
                            var i_gst  = $("#itemsSelected").find('tr').eq($i).find('td').eq(7).text();
                            var i_net_amount  = $("#itemsSelected").find('tr').eq($i).find('td').eq(8).text();

                            $.ajax({
                                url : "purchase.create.log",
                                type: "POST",
                                data: {
                                    _token: '{{csrf_token()}}',
                                    i :$i,
                                    id  : i_id,
                                    price : i_price,
                                    qty : i_qty,
                                    amount : i_amount,
                                    duty_no : i_duty_no,
                                    duty : i_duty,
                                    gst : i_gst,
                                    net_amount : i_net_amount
                                },
                                beforeSend: function () {
                                    console.log("-----------------------------------");
                                },
                                complete: function (e) {
                                    console.log(e);
                                }

                            });
                        }
                        console.log(e);


                    }
                });
            }
            







            function totalCalc()
            {
                var table = $("#tableSelected");
                    var amount    = 0;
                    var duty_no   = 0;
                    var duty      = 0;
                    var gst       = 0;
                    var net_amount= 0;
                    table.find("tr").children("td:nth-child(5)")
                        .each(function()
                        {
                            $this = $(this);
                            amount += parseInt($this.text());
                        });
                    table.find("tr").children("td:nth-child(6)")
                    .each(function()
                    {
                        $this = $(this);
                        duty_no += parseInt($this.text());
                    });
                    table.find("tr").children("td:nth-child(7)")
                    .each(function()
                    {
                        $this = $(this);
                        duty += parseInt($this.text());
                    });
                    table.find("tr").children("td:nth-child(8)")
                    .each(function()
                    {
                        $this = $(this);
                        gst += parseInt($this.text());
                    });
                    table.find("tr").children("td:nth-child(9)")
                    .each(function()
                    {
                        $this = $(this);
                        net_amount += parseInt($this.text());
                    });

                    $("#t_amount").text(amount);
                    $("#t_duty_no").text(duty_no);
                    $("#t_duty").text(duty);
                    $("#t_gst").text(gst);
                    $("#t_net_amount").text(net_amount);
            }


            function taxCalc() {

                var amount =  parseFloat($("#t_amount").text());
                var tax    =  parseFloat($("#tax_amount").text());
                var trading = null;
                var tax_percent = null;


                if(! $('#cb_per_no').is(':checked')){
                    tax_percent = (amount/100) * tax;
                    tax = tax_percent;

                }

                if( $('#cb_add_sub').is(':checked')){

                    trading = parseFloat(amount) - parseFloat(tax);

                }else{
                    trading = parseFloat(amount) + parseFloat(tax);
                }

                $("#trading_amount").text(parseInt(trading));

                if(!trading)
                $("#trading_amount").text(amount);



            }




    </script>

@endsection

