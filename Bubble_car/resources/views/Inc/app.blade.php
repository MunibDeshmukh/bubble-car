
<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="app_theme_b"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    {{--<link rel="icon" type="image/png" href="/assets/img/favicon-16x16.png" sizes="16x16">--}}
    {{--<link rel="icon" type="image/png" href="/assets/img/favicon-32x32.png" sizes="32x32">--}}
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <title>Bubble Car</title>

    <!-- additional styles for plugins -->
    <!-- weather icons -->
    <!-- additional styles for plugins -->
    <!-- htmleditor (codeMirror) -->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/bower_components/codemirror/lib/codemirror.css') }}">
    <!-- select2 -->
    <link rel="stylesheet" href="{{ asset('/bower_components/select2/dist/css/select2.min.css') }}">

    <!-- uikit -->
    <link rel="stylesheet" href="{{ asset('/bower_components/uikit/css/uikit.almost-flat.min.css') }}" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="{{ asset('/assets/icons/flags/flags.min.css') }}" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="{{ asset('/assets/css/style_switcher.min.css') }}" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="{{ asset('/assets/css/main.min.css') }}" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="{{ asset('/assets/css/themes/themes_combined.min.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('/code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css') }}">


    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="{{ asset('/bower_components/matchMedia/matchMedia.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/bower_components/matchMedia/matchMedia.addListener.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('/assets/css/ie.css') }}" media="all">


    <![endif]-->



    <!-- Material+Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
       .light-bg{
           background:rgba(255,255,255,0.9) !important;
       }
        .text-light{
            color: white !important;
        }
        tr th{
            font-size: 13px;font-weight: bolder !important;
        }
        .modal {
  text-align: center;
}

@media screen and (min-width: 768px) { 
  .modal:before {
    display: inline-block;
    vertical-align: middle;
    content: " ";
    height: 100%;
  }
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
    </style>


    <script type="text/javascript" src="{{ asset('/assets/js/jquery.min.js') }}"></script>




</head>
<body class="header_full " style="background: white !important;">
<div style="background-image: url('/assets/img/bg.jpg');background-position:center;background-repeat: no-repeat;background-size: cover;position: fixed;top: 0;bottom: 0;left: 0;right: 0;z-index: -10000"></div>


<!-- main header -->
<header id="header_main">
    <div class="header_main_content">
        <nav class="uk-navbar">

            <!-- main sidebar switch -->
            <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                <span class="sSwitchIcon"></span>
            </a>

<h3 style="z-index: -10;text-align: center;margin-top: 15px;color: white;text-transform: capitalize;position: fixed;width: 100%;">
    {{session('login_workstation_name')}}
</h3>
            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">
                    @if(session('login_user_id')==1)
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="workstation.selector" class="user_action_image">
                            <strong style="text-transform: capitalize">Change Workstation</strong>
                        </a>
                    </li>
                    @endif
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="#" class="user_action_image">
                            <strong style="text-transform: capitalize">{{session('login_name')}}</strong>
                        </a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav js-uk-prevent">
                                <li><a href="user.logout">Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

</header><!-- main header end -->
<!-- main sidebar -->
<aside  id="sidebar_main" class="accordion_mode light-bg">

    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="/" class="sSidebar_hide sidebar_logo_large">
                {{--<img class="logo_regular" src="/assets/img/logo_main.png" alt="" height="15" width="71"/>--}}
                {{--<img class="logo_light" src="/assets/img/logo_main_white.png" alt="" height="15" width="71"/>--}}
            </a>
            <a href="/" class="sSidebar_show sidebar_logo_small">
                {{--   --}}
            </a>
        </div>

    </div>

    <div class="menu_section">
        <ul>
            <li class="current_section" title="Dashboard">
                <a href="/">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Dashboard</span>
                </a>

            </li>
            @if(Session::get('login_user_id')==1)
            <li title="Dashboard">
                <a href="workstation.selector">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Change Workstation</span>
                </a>

            </li>


            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Workstation</span>
                </a>
                <ul>
                    <li><a href="workstation.create">Create</a></li>
                    <li><a href="workstation.list">Manage</a></li>
                </ul>

            </li>
            @endif
        <!--     <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Variation</span>
                </a>
                <ul>
                    <li><a href="variation.create">Create</a></li>
                    <li><a href="variation.list">Manage</a></li>
                </ul>

            </li> -->
            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Brand</span>
                </a>
                <ul>
                    <li><a href="brand.create">Create</a></li>
                    <li><a href="brand.list">Manage</a></li>
                </ul>

            </li>
            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Category</span>
                </a>
                <ul>
                    <li><a href="category.create">Create</a></li>
                    <li><a href="category.list">Manage</a></li>
                </ul>

            </li>
            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Cars</span>
                </a>
                <ul>
                    <li><a href="carselection.create">Create</a></li>
                    <li><a href="carselection.list">Manage</a></li>
                </ul>

            </li>
            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">User</span>
                </a>
                <ul>
                    <li><a href="user.create">Create</a></li>
                    <li><a href="user.list">Manage</a></li>
                </ul>

            </li>

         <!--    <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">person</i></span>
                    <span class="menu_title">Accounts</span>
                </a>
                <ul>
                    <li>
                        <a href="#">Banks</a>
                        <ul>
                            <li>
                                <a href="bank.create">Add New</a>
                            </li>
                            <li>
                                <a href="bank.list">All Banks</a>
                            </li>
                        </ul>
                    </li>



                    <li>
                        <a href="#">Expense Heads</a>
                        <ul>
                            <li>
                                <a href="account.head.create">Create</a>
                            </li>
                            <li>
                                <a href="account.head.list">Manage</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#">Payable Expense</a>
                        <ul>
                            <li>
                                <a href="expense.payable.create">Create</a>
                            </li>
                            <li>
                                <a href="expense.payable.list">Manage</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#">Receivable Expense</a>
                        <ul>
                            <li>
                                <a href="expense.receivable.create">Create</a>
                            </li>
                            <li>
                                <a href="expense.receivable.list">Manage</a>
                            </li>
                        </ul>
                    </li>







                </ul>
            </li> -->

                <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Units</span>
                </a>
                <ul>
                    <li><a href="unit.create">Add New</a></li>
                    <li><a href="unit.list">Manage</a></li>
                </ul>

            </li>

            </li>
            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Taxes</span>
                </a>
                <ul>
                    <li><a href="tax.create">Add New</a></li>
                    <li><a href="tax.list">Manage</a></li>
                </ul>

            </li>


            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Vendors</span>
                </a>
                <ul>
                    <li><a href="vendor.create">Create</a></li>
                    <li><a href="vendor.list">Manage</a></li>
                </ul>

            </li>
        
            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Out Source Vendor</span>
                </a>
                <ul>
                    <li><a href="outSourcevendor.create">Create</a></li>
                    <li><a href="outSourcevendor.list">Manage</a></li>
                </ul>

            </li>

            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Customers</span>
                </a>
                <ul>
                    <li><a href="customer.create">Create</a></li>
                    <li><a href="customer.list">Manage</a></li>
                </ul>

            </li>

            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Services</span>
                </a>
                <ul>
                    <li><a href="services.create">Create</a></li>
                    <li><a href="services.list">Manage</a></li>
                </ul>

            </li>
        
        
        
            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Out Services Services</span>
                </a>
                <ul>
                    <li><a href="outsourceservices.create">Create</a></li>
                    <li><a href="outsourceservices.list">Manage</a></li>
                </ul>

            </li>



            <li>
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Products</span>
                </a>
                        <ul>
                            <li>
                                <a href="product.create">Create </a>
                            </li>
                            <li>
                                <a href="product.list">Manage</a>
                            </li>
                           {{--  <li>
                                <a href="#">Classes</a>
                                <ul>
                                    <li>
                                        <a href="product.class.create">Create</a>
                                    </li>
                                    <li>
                                        <a href="product.class.list">Manage</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Brands</a>
                                <ul>
                                    <li>
                                        <a href="product.brand.create">Create</a>
                                    </li>
                                    <li>
                                        <a href="product.brand.list">Manage</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Types</a>
                                <ul>
                                    <li>
                                        <a href="product.type.create">Create</a>
                                    </li>
                                    <li>
                                        <a href="product.type.list">Manage</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Categories</a>
                                <ul>
                                    <li>
                                        <a href="product.category.create">Create</a>
                                    </li>
                                    <li>
                                        <a href="product.category.list">Manage</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Models</a>
                                <ul>
                                    <li>
                                        <a href="product.model.create">Create</a>
                                    </li>
                                    <li>
                                        <a href="product.model.list">Manage</a>
                                    </li>
                                </ul>
                            </li>
 --}}
                        </ul>


            </li>
<!--             <li>
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE241;</i></span>
                    <span class="menu_title">Sales</span>
                </a>
                <ul>
                    <li>
                        <a href="#">Gst</a>
                        <ul>
                            <li>
                                <a href="gst.sale.create">Create</a>
                            </li>
                            <li>
                                <a href="gst.sale.list">Manage</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Local</a>
                        <ul>
                            <li>
                                <a href="local.sale.create">Create</a>
                            </li>
                            <li>
                                <a href="local.sale.list">Manage</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Rental</a>
                        <ul>
                             <li>
                                <a href="rental.sale.create">Create </a>
                            </li>
                            <li>
                                <a href="rental.sale.list">Manage</a>
                            </li>

                        </ul>
                    </li>

                    <li>
                        <a href="#">Receivables</a>
                        <ul>
                            <li>
                                <a href="customer.payable.create">Create </a>
                            </li>
                            <li>
                                <a href="customer.payable.list">Manage</a>
                            </li>

                        </ul>
                    </li>

                </ul>


            </li> -->
            <!-- <li>
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE241;</i></span>
                    <span class="menu_title">Purchase</span>
                </a>
                <ul> -->
                    <li>
                        <a href="#">
                            <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Purchase</span>
                </a>
                        <ul>
                            <li>
                                <a href="purchase.create">Create</a>
                            </li>
                            <li>
                                <a href="purchase.list">Manage</a>
                            </li>
                            
                        </ul>
                    </li>
        
        
               <li>
                        <a href="#">
                            <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Sales</span>
                </a>
                        <ul>
                           <li>
                        <a href="#">
                            <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Job Card</span>
                </a>
                        <ul>
                           
 <li>
                                <a href="cashsales.create"> Create Job Card</a>
                            </li>

                            <li>
                                <a href="viewcashsales.list">Manage Job Card</a>
                            </li>
                           
                        </ul>
                    </li>

                           
                        </ul>
                    </li>

                      <li>
                        <a href="#">
                            <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Generate Grn</span>
                </a>
                        <ul>
                            <li>
                                <a href="purchase.grn">Generate</a>
                            </li>
                            <li>
                                <a href="grn.view">View</a>
                            </li>
                            
                        </ul>
                    </li>
        
           
             <li title="Dashboard">
                <a href="stockAdjusment">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Stock Adjusment</span>
                </a>


            </li>
            
             <li title="Dashboard">
                <a href="damage">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Damage</span>
                </a>


            </li>
            
        
        
                    <li title="Dashboard">
                <a href="inventory.view">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">View Inventory</span>
                </a>
                  <a href="stock.view">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Stock View</span>
                </a>

            </li>
               <!--      <li>
                        <a href="#">Local Purchase</a>
                        <ul>
                            <li>
                                <a href="local.purchase.create">Create</a>
                            </li>
                            <li>
                                <a href="local.purchase.list">Manage</a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="#">Payables</a>
                        <ul>
                            <li>
                                <a href="vendor.payable.create">Create </a>
                            </li>
                            <li>
                                <a href="vendor.payable.list">Manage</a>
                            </li>

                        </ul>
                    </li>


                </ul>


            </li> -->
       <!--      <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Warehouse</span>
                </a>
                <ul>
                    <li><a href="warehouse.create">Create</a></li>
                    <li><a href="warehouse.list">Manage</a></li>
                </ul>

            </li>
            <li title="vendors">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                    <span class="menu_title">Location</span>
                </a>
                <ul>
                    <li><a href="location.create">Create</a></li>
                    <li><a href="location.list">Manage</a></li>
                </ul>

            </li> -->




<!-- 
            <li>
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE241;</i></span>
                    <span class="menu_title">Payroll</span>
                </a>
                <ul>

                    <li>
                        <a href="#">Staff</a>
                        <ul>
                            <li>
                                <a href="staff.create">Create</a>
                            </li>
                            <li>
                                <a href="staff.list">Manage</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Advance</a>
                        <ul>
                            <li>
                                <a href="advance.create">Create</a>
                            </li>
                            <li>
                                <a href="advance.list">Manage</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Loan</a>
                        <ul>
                            <li>
                                <a href="loan.create">Create</a>
                            </li>
                            <li>
                                <a href="loan.list">Manage</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Salary</a>
                        <ul>
                            <li>
                                <a href="salary.create">Create</a>
                            </li>
                            <li>
                                <a href="#">Manage</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Models</a>
                        <ul>
                            <li>
                                <a href="product.model.create">Create</a>
                            </li>
                            <li>
                                <a href="product.model.list">Manage</a>
                            </li>
                        </ul>
                    </li>

                </ul>


            </li> -->


        </ul>
    </div>
</aside>
<!-- main sidebar end -->

<div id="page_content">
    <div id="page_content_inner">
        @yield('content')
    </div>
</div>
<script>

    @if(!session('login_name'))
        window.location="user.logout";
    @endif
</script>
<!-- common functions -->
<script src="{{ asset('/assets/js/common.min.js') }}"></script>
<!-- uikit functions -->
<script src="{{ asset('/assets/js/uikit_custom.min.js') }}"></script>
<!-- altair common functions/helpers -->
<script src="{{ asset('/assets/js/altair_admin_common.min.js') }}"></script>

<!-- page specific plugins -->
<!-- jquery ui -->
<script src="{{ asset('/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- ionrangeslider -->
<script src="{{ asset('/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') }}"></script>
<!-- htmleditor (codeMirror) -->
<script src="{{ asset('/assets/js/uikit_htmleditor_custom.min.js') }}"></script>
<!-- inputmask -->
<script src="{{ asset('/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
<!-- select2 -->
<script src="{{ asset('/bower_components/select2/dist/js/select2.min.js') }}"></script>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--  forms advanced functions -->
<script src="{{ asset('/assets/js/pages/forms_advanced.min.js') }}"></script>
     
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- page scripts -->
@yield('page-scripts')
<!-- page scripts -->
<script>
    $(document).ready(function () {
        $("#page_content_inner").css('display','block');


        $('.viewRecipe').click(function(){

            var id = $(this).attr('id');
            alert(id);
        });

    });

</script>

</body>
</html>