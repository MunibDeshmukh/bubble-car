@extends('Inc.window')

@section('content')
    <style>
        th{font-weight: bolder !important; ;}
    </style>
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    <table class="table ">

        <tbody><tr>

            <td colspan="2">
                <div style="border-bottom: 1px solid black;border-top: 1px solid black;">
                    <b>KHAN BUSINESS PRODUCTS</b><br>Ph. # 36364027, 36364037, 36802400-1<br>Fax # 36802400 Cell: 0333-210261-3<br>khanbusinessproducts@yahoo.com
                </div>
            </td>
            <td colspan="3" style="text-align: center"><b style="font-size: 36px">KHAN's</b></td>
            <td colspan="2">
                <div style="border-bottom: 1px solid black;border-top: 1px solid black;">
                    Anwar-ul-Uloom, SP-10, Block-10,<br>Gulberg, F. B. Area Adj, Gulberg <br>Police Station Karachi<br>
                    E-mail:khanbusinessproducts@gmail.com
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <h3  style="text-align: center">BILLING</h3>
    <table style="margin-top: 20px;width: 100%;">
        <tr><td style="width: 150px;">Invoice No.</td> <td>{{$voucher_no}}</td><td style="text-align: right !important;">Date {{date('d-m-Y')}}</td></tr>
        <tr><td>Company Name</td> <td>{{$company_name}}</td></tr>
        <tr><td>Contact Person Name</td> <td>{{$person_name}}</td></tr>
    </table>


    <table style="width: 100%" class="uk-table table-bordered table-responsive table-hover" >
                       <thead>
                       <tr>
                           <th>SNo</th>
                           <th>Item Code</th>
                           <th>Rate</th>
                           <th>Quantity</th>
                           <th>Amount</th>
                       </tr>
                       </thead>
                       <tbody id="itemsTable">


                       <?php $sno =1; ?>
                       @foreach($table as $i)

                           <tr id="unSel{{$i->id}}">
                               <td>{{$sno}}</td>
                               <td>{{$i->product_name}}</td>
                               <td>{{$i->price}}</td>
                               <td>{{$i->item_quantity}}</td>
                               <td>{{$i->amount}}</td>
                           </tr>
                           <?php $sno++; ?>
                       @endforeach
                       </tbody>
                   </table>
    <h3  style="text-align: center">MISC BILLING</h3>
    <table style="width: 100%" class="uk-table table-bordered table-responsive table-hover" >
        <thead>
        <tr>
            <th style="text-align: center">Description</th>
            <th style="width: 100px;">Amount</th>
        </tr>
        </thead>
        <tbody style="text-align: center">

        <tr>
            <td >SRB NO: <b style="text-transform: uppercase">{{$srb}} </b> SALES TAX ON SERVICES </td>
            <td>{{$tax1}}</td>
        </tr>
        <tr>
            <td >SRB NO: <b style="text-transform: uppercase">{{$srb}} </b> ADDITIONAL TAX</td>
            <td>{{$tax2}}</td>
        </tr>


        <tr>
            <th style="text-align: right">TOTAL</th>
            <th>{{$total_amount}}</th>

        </tr>
        <tr class="hidden-print">
            <td colspan="4" class="etxt">
                <button onclick="window.print();" class="uk-button-primary   uk-button uk-align-center uk-float-right"><i class="material-icons md-light no_margin">print</i></button>
                <button onclick="window.close();" class="uk-button-danger uk-button uk-align-center uk-float-right"><i class="material-icons md-light no_margin">close</i></button>
            </td>

        </tr>
        </tbody>
    </table>




@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>




@endsection

