
<div class="row">
<div class="col-sm-12">
    <h1>Products</h1>
    
    
<table id="" class="table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    
                    <th>product Name </th>
                    <th>item Quantity</th>
                    <th>Price</th>
                    <th>Amount</th>
                    <th>Tax</th>
                    <th>Receivable</th>
                    
                </tr>
                </thead>



               <tbody>
                   @foreach ($table as $i)

                       <tr>
                           <td>{{$i->productName}}</td>
                           <td>{{$i->item_quantity}}</td>
                           <td>{{$i->price}}</td>
                           <td>{{$i->amount}}</td>
                           <td>{{$i->tax}}</td>
                           <td>{{$i->receivable}}</td>
                         
                          

                       </tr>

                   @endforeach
               </tbody>
            </table>
    
    </div>
    </div>


<div class="row">
<div class="col-sm-12">
    <h1>Services</h1>
    
    
<table id="" class="table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    
                    <th>Service Name </th>
                    <th>item Quantity</th>
                    <th>Price</th>
                    <th>Amount</th>
                    <th>Tax</th>
                    <th>Receivable</th>
                    
                </tr>
                </thead>



               <tbody>
                   @foreach ($table2 as $i)

                       <tr>
                           <td>{{$i->serviceName}}</td>
                           <td>{{$i->item_quantity}}</td>
                           <td>{{$i->price}}</td>
                           <td>{{$i->amount}}</td>
                           <td>{{$i->tax}}</td>
                           <td>{{$i->receivable}}</td>
                         
                          

                       </tr>

                   @endforeach
               </tbody>
            </table>
    
    </div>
    </div>

<div class="row">
<div class="col-sm-12">
    <h1>Out Source Service</h1>
    
    
<table id="" class="table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    
                    <th>Out Source Service Name </th>
                    <th>item Quantity</th>
                    <th>Price</th>
                    <th>Amount</th>
                    <th>Tax</th>
                    <th>Receivable</th>
                    
                </tr>
                </thead>



               <tbody>
                   @foreach ($table3 as $i)

                       <tr>
                           <td>{{$i->outSourceServiceName}}</td>
                           <td>{{$i->item_quantity}}</td>
                           <td>{{$i->price}}</td>
                           <td>{{$i->amount}}</td>
                           <td>{{$i->tax}}</td>
                           <td>{{$i->receivable}}</td>
                         
                          

                       </tr>

                   @endforeach
               </tbody>
            </table>
    
    </div>
    </div>