@extends('Inc.app')

@section('content')


    <h2 class="text-light">Product Create</h2>
    <hr>


    <div class="md-card">

        <div class="md-card-content" style="display: block;">
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-6-10 uk-push-2-10 uk-row-first">
                    <form action="product.create" method="post">

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif

                        {{csrf_field()}}
                        <div style="width: 70%;margin-left: 15%">
                        </div>
                        <table style="width: 100%">
                            <tbody>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Product Name</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="name" name="name" value="{{old('name')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Description</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="desc" name="desc" value="{{old('desc')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>
                            {{-- <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Class </td>
                                <td>
                                    <select title="" id="class" data-md-selectize name="class"  onchange="getBrands(this.value)" >
                                        <option selected>Select...</option>
                                        @foreach($class as $i)
                                            <option value="{{$i->id}}">{{$i->name}}</option>
                                        @endforeach

                                    </select>
                                </td>
                            </tr> --}}
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Brand </td>
                                <td>
                                <select title="" id="brand"  class="md-input "   name="brand"  >
                                    <option selected>Select...</option>
                                    @foreach ($brand as $i)
                                        <option value="{{$i->id}}">{{$i->name}}</option>
                                    @endforeach
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Variation </td>
                                <td>
                                    <select title="" id="model"  class="md-input "  name="variation"  >
                                        <option selected>Select...</option>
                                        @foreach ($variation as $i)
                                        <option value="{{$i->id}}">{{$i->name}}</option>
                                    @endforeach
                                    </select>
                                </td>
                            </tr>
                           {{--  <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Product Type </td>
                                <td>
                                    <select title="" id="type" data-md-selectize name="type"   >
                                        <option selected>Select...</option>
                                       @foreach ($brand as $i)
                                        <option value="{{$i->id}}">{{$i->name}}</option>
                                    @endforeach

                                    </select>
                                </td>
                            </tr> --}}
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Category  </td>
                                <td>
                                    <select title="" id="category" data-md-selectize name="category"   >
                                        <option selected>Select...</option>
                                        @foreach($category as $i)
                                            <option value="{{$i->id}}">{{$i->name}}</option>
                                        @endforeach

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Unit  </td>
                                <td>
                                    <select title="" id="category" data-md-selectize name="unit"   >
                                        <option selected>Select...</option>
                                        @foreach($unit as $i)
                                            <option value="{{$i->name}}">{{$i->name}}</option>
                                        @endforeach

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Size</td>
                                <td>
                                    <select title="" id="location" data-md-selectize name="type"  onchange="getCars(this.value)" >
                                        <option selected>Select...</option>
                                        <option value="small">Small</option>
                                        <option value="medium">Medium</option>
                                        <option value="high">High</option>
                                        <option value="specialized">Specialized</option>

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Cars </td>
                                <td>
                                    <select title="" id="cars"  name="car[]"  multiple >
                                        <option selected>Select...</option>
                                        

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Product Code</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="retail_price" name="product_code" value="{{old('retail_price')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Outside Code</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="outside_code" name="outside_code" value="{{old('f_o_b')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>
                            
                           

                            </tbody>
                        </table>
                        <div class="uk-form-row">
                            <div class="uk-width-1-1 uk-margin-top">
                                <button type="submit" name="save" class="md-btn md-btn-primary uk-float-right">Create</button>
                                <a href="" class="md-btn md-btn uk-float-right">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>

    </div>





        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>

<script>
    function getCars(e) {

        $.ajax({
            url:"product.get_cars",
            type:"GET",
            data:{
                
                type:e
            },
            success:function (e) {
                console.log(e);
                $('#cars').append(e);

            }
        });
    }


    function getModels(e) {
        $.ajax({
            url:"product.get_models",
            type:"POST",
            data:{
                _token: '{{csrf_token()}}',
                brand_id:e,
                class_id:$("#class").val()

            },
            complete:function (e) {
                console.log(e);
                json = JSON.parse(e.responseText);
                $.each(json,function (index,value) {

                    $("#model").append("<option value="+value['id']+">"+value['name']+"</option>");



                })

            }
        });
    }


</script>

@endsection

