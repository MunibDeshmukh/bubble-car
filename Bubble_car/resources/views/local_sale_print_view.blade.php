@extends('Inc.window')

@section('content')
    <style>
        th{font-weight: bolder !important;}

    </style>

    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    <table class="table " style="width: 100%;">

        <tbody><tr>

            <td colspan="2">
                <div style="border-bottom: 1px solid black;border-top: 1px solid black;">
                    <b>KHAN BUSINESS PRODUCTS</b><br>Ph. # 36364027, 36364037, 36802400-1<br>Fax # 36802400 Cell: 0333-210261-3<br>khanbusinessproducts@yahoo.com
                </div>
            </td>
            <td colspan="3" style="text-align: center"><b style="font-size: 36px">KHAN's</b></td>
            <td colspan="2">
                <div style="border-bottom: 1px solid black;border-top: 1px solid black;">
                    Anwar-ul-Uloom, SP-10, Block-10,<br>Gulberg, F. B. Area Adj, Gulberg <br>Police Station Karachi<br>
                    E-mail:khanbusinessproducts@gmail.com
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <h3  style="text-align: center">BILLING</h3>
    <table style="margin-top: 20px;width: 100%;">
        <tr><td style="width: 150px;">Invoice No.</td> <td>{{$voucher_no}}</td><td style="text-align: right !important;">Date {{date('d-m-Y')}}</td></tr>
        <tr><td>Company Name</td> <td>{{$company_name}}</td></tr>
        <tr><td>Contact Person Name</td> <td>{{$person_name}}</td></tr>
    </table>

                    <table style="width: 100%" class="uk-table table-bordered table-responsive table-hover" >
                       <thead>
                       <tr>
                           <th>SNo</th>
                           <th>Item Code</th>
                           <th>Description of Goods.</th>
                           <th>Rate</th>
                           <th>Quantity</th>
                           <th>Amount</th>
                       </tr>
                       </thead>
                       <tbody id="itemsTable">

                       <?php $sno =1; ?>
                       @foreach($table as $i)

                           <tr id="unSel{{$i->id}}">
                               <td>{{$sno}}</td>
                               <td>{{$i->model}}</td>
                               <td>{{$i->product_desc}}</td>
                               <td>{{$i->price}}</td>
                               <td>{{$i->item_quantity}}</td>
                               <td>{{$i->amount}}</td>
                           </tr>
                           <?php $sno++; ?>
                       @endforeach
                       </tbody>
                   </table>
    <h3  style="text-align: center">MISC BILLING</h3>
    <table style="width: 100%" class="uk-table table-bordered table-responsive table-hover" >
        <thead>
        <tr>
            <th style="text-align: center">Description</th>
            <th style="width: 100px;">Amount</th>
        </tr>
        </thead>
        <tbody style="text-align: center">

        <tr>
            <td>Discount </td>
            <td>{{$discount}}</td>
        </tr>

        <tr>
            <th style="text-align: right"><span style="background: #333;padding: 5px 40px;color: white">TOTAL</span></th>
            <th>{{$total_amount}}</th>

        </tr>

        <tr class="hidden-print">
            <td colspan="4" class="etxt">
                <button onclick="window.print();" class="uk-button-primary   uk-button uk-align-center uk-float-right"><i class="material-icons md-light no_margin">print</i></button>
                <button onclick="window.close();" class="uk-button-danger uk-button uk-align-center uk-float-right"><i class="material-icons md-light no_margin">close</i></button>
            </td>

        </tr>
        </tbody>
    </table>
        <table style="width: 100%" class="uk-table table-bordered table-responsive table-hover" >
            <tr>
                <td style="text-align: left;width: 100px;">RUPEES</td>
                <td style="text-transform: capitalize" id="inwords"></td>

            </tr>
        </table>



@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>


    <script>

        $(document).ready(function () {
           inWords({{$total_amount}});
        });








        var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
        var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

        function inWords (num) {
            if ((num = num.toString()).length > 9) return 'overflow';
            n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
            if (!n) return; var str = '';
            str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
            str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
            str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
            str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
            str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
            $("#inwords").text(str);
        }










    </script>

@endsection

