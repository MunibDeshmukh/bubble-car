@extends('Inc.app')

@section('content')
    <style>
      #productRows input {padding: 4px;width: 100%;}
      .md-card .md-card-content {
          padding: 5px !important;
      }
        #product_search{
            max-height: 200px;
            height: auto;
            overflow: scroll;
            overflow-x: hidden;
            position: absolute;
            background: white;
            width: 100%;
            margin-left: 1px;
            -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
            box-shadow: 0 1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
        }
      #product_search ul {width: 100%;}
      #product_search ul li{padding:0 2px;cursor: pointer;border-left: 5px solid white }
      #product_search ul li:hover{color: royalblue;border-left: 5px solid royalblue;}



    </style>
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">
    <div class="md-card light-bg">
        <div class="md-card-toolbar" >
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-toggle" id="m1" style="display: none; opacity: 1; transform: scale(1);"></i>
            </div>
        </div>

        <div class="md-card-content form" >
            <div class="md-card-content" >
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-1 uk-row-first">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                        <div class="uk-grid">
                            <div class="uk-width-medium-2-5">
                                <div class="parsley-row">
                                    <select title="" id="vendor_id" class="md-input">
                                        <option value="empty"  selected>Select Vendors...</option>
                                        @foreach($vendors as $i)
                                            <option value="{{$i->id}}">{{$i->company_name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <div class="md-input-wrapper md-input-filled"><label>Voucher No.</label><input readonly type="text" class="md-input" id="voucher_no" required="" value="{{$voucher}}"><span class="md-input-bar"></span></div>
                                </div>
                            </div>


                            <div class="uk-width-medium-1-5">
                                <div class="parsley-row">
                                    <div class="md-input-wrapper"><label>Sale Date</label><input type="text" class="md-input" id="date" data-uk-datepicker="{format:'YYYY-MM-DD'}"><span class="md-input-bar"></span></div>

                                </div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                <select title="" id="tax_as" class="md-input">
                                    <option value="empty" selected>Tax Detect As...</option>
                                    <option value="1"  >Percentage</option>
                                    <option value="0"  >Number</option>

                                </select>
                            </div>
                            <div class="uk-width-medium-1-5">
                                    <div class="md-input-wrapper"><label>No. of Containers</label><input type="text" class="md-input" id="container" required="" ><span class="md-input-bar"></span></div>
                                </div>
                            <div class="uk-width-medium-1-5">
                                    <div class="md-input-wrapper"><label>GD No.</label><input  type="text" class="md-input" id="gd_no" required="" ><span class="md-input-bar"></span></div>
                                </div>
                            <div class="uk-width-medium-1-5">
                                    <div class="md-input-wrapper"><label>GD Date</label><input type="text" class="md-input" id="gd_date" data-uk-datepicker="{format:'YYYY-MM-DD'}"><span class="md-input-bar"></span></div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                    <div class="md-input-wrapper"><label>BL No.</label><input  type="text" class="md-input" id="bl_no" ><span class="md-input-bar"></span></div>
                            </div>
                            <div class="uk-width-medium-1-5">
                                    <div class="md-input-wrapper"><label>BL Date</label><input type="text" class="md-input" id="bl_date" data-uk-datepicker="{format:'YYYY-MM-DD'}"><span class="md-input-bar"></span></div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>





        </div>

            <div class="md-card-content" >
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-1 uk-row-first">
                        <table style="width: 100%" class="uk-table table-bordered" >
                            <thead>
                            <tr style="text-align: center" >
                                <td>Product</td>
                                <td>Price</td>
                                <td>Quantity</td>
                                <td>Amount</td>
                                <td>GST(%)</td>
                                <td>Total</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="productRows">

                            </tbody>

                            <tfoot style="text-align: center">
                            <tr >
                                <td></td>
                                <td></td>
                                <td>Total</td>
                                <td id="amount"></td>
                                <td id="gst"></td>
                                <td id="total"></td>
                                <td></td>
                            </tr>
                            </tfoot>


                        </table>
                    </div>
                    <div class="uk-width-medium-1-1 uk-margin-top">
                        <table style="width: 100%">
                            <tr>
                                <td style="position: relative;padding-left: 7px;max-width: 50px;width: auto">
                                    <input type="text" style="padding: 4px;width: 100%" onkeyup="productSearch(this.value)" placeholder="Search Here...">
                                    <div id="product_search">
                                        <ul style="list-style: none;" id="product_list">

                                        </ul>
                                    </div>
                                </td>
                                <td width="30" style="padding-left: 20px"><i class="material-icons"  data-uk-tooltip="{pos:'right'}" title="Live search for products !" style="cursor:pointer;">info</i></td>
                                <td><button onclick="create()" id="create" class="uk-button uk-button-primary " style="float: right">Save</button></td>
                            </tr>
                        </table>
                </div>


                </div>

            </div>





<br>

    </div>






        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>



        <script>



            function create() {

                var isValid = true;
                $("#productRows").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });

                $(".form").find('select').each(function() {
                    if ($(this).val() == "empty") {
                        isValid = false;
                    }
                });

                $(".form").find('input').each(function() {
                    if ($(this).val() == "") {
                        isValid = false;
                    }
                });


                if(isValid==true) {

                var qty =  $("#productRows > tr").length;
                var tr = $("#productRows");
                $.ajax({
                    url:"local.purchase.create",
                    type:"POST",
                    data:{
                        _token   :'{{csrf_token()}}',
                        qty      :qty,
                        gst      :$("#gst").text(),
                        amount   :$("#amount").text(),
                        total    :$("#total").text(),
                        date     :$("#date").val(),
                        gd_no    :$("#gd_no").val(),
                        gd_date  :$("#gd_date").val(),
                        bl_date  :$("#bl_date").val(),
                        bl_no    :$("#bl_no").val(),
                        voucher  :$("#voucher_no").val(),
                        vendor   :$('#vendor_id').val(),
                        container:$('#container').val()
                    },
                    beforeSend:function () {
                        $("#create").prop('disabled')


                    },
                    success:function (e) {

                        for($i=0;$i<qty;$i++) {

                            var i_id       = tr.find('tr').eq($i).find('td').eq(0).find('input').data('id');
                            var i_price    = tr.find('tr').eq($i).find('td').eq(1).find('input').val();
                            var i_qty      = tr.find('tr').eq($i).find('td').eq(2).find('input').val();
                            var i_amount   = tr.find('tr').eq($i).find('td').eq(3).find('input').val();
                            var i_gst      = tr.find('tr').eq($i).find('td').eq(7).find('input').val();
                            var i_total    = tr.find('tr').eq($i).find('td').eq(5).find('input').val();

                            $.ajax({
                                url : "local.purchase.create.log",
                                type: "POST",
                                data: {
                                    _token: '{{csrf_token()}}',
                                    id  : i_id,
                                    qty : i_qty,
                                    price :  i_price,
                                    amount : i_amount,
                                    gst:i_gst,
                                    total:i_total
                                },
                                beforeSend: function () {
                                    console.log("-----------------------------------");
                                },
                                complete: function (e) {
                                    console.log(e);
                                }
                            });
                        }

                        console.log(e);


                    }
                    ,complete:function (e) {
                        window.location="local.purchase.create.success";
                    }
                });

                }else{
                    alert("Please fill all files first!")
                }



            }

            

            function productSearch(e) {
                $.ajax({
                    url:"product.by_name",
                    type:"POST",
                    data: {
                        _token:'{{csrf_token()}}',
                         q:e
                    },
                    complete:function (e){
//                        console.log(e);
                        var json = JSON.parse(e.responseText);
                        $("#product_list").html(null);
                        $.each(json, function(i, item) {
                        var name =item['name'];
                            $("#product_list").append("<li onclick='productRow("+item['id']+")'>"+name+"</li>");
                        });
                    }
                })
            }


            function productRow(e) {
                if ($("#productRows").find("#selectedRow" + e).html() == null) {
                    var tr = "tr";
                    $.ajax({
                        url: "product.by_id",
                        type: "POST",
                        data: {
                            _token: '{{csrf_token()}}',
                            q: e
                        },
                        complete: function (e) {
//                        console.log(e);
                            var json = JSON.parse(e.responseText);
                            $.each(json, function (i, item) {
//                            console.log(item['name']);

                                $("#productRows").append(
                                    '<tr id="selectedRow'+item['id']+'">' +
                                    '<td><input onkeyup="productRowCalc($(this))" type="text"   value="' + item['name'] + '" data-id="'+item['id']+'" ></td>' +
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" value="' + item['sailing_price']+'"></td>' +
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="Quantity"></td>'+
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="Amount"></td>'+
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="gst(%) Tax"></td>'+
                                    '<td><input onkeyup="productRowCalc($(this))" type="number" placeholder="Total Amount"></td>' +
                                    '<td style="width: 30px !important;"><input type="button" value="X" onclick="productRowRemove($(this))"></td>'+
                                    '<td ><input type="number" ></td>'+//hidden field for text amount
                                    '</tr>'
                                );

                            });
                        }
                    })
                }
            }

            function productRowRemove(elm)
            {
                elm.closest('tr').remove();
            }


            function productRowCalc(elm)
            {
                var sign = $("#tax_as").val();
                var tr   = elm.closest('tr');
                var unit = parseFloat(tr.find('td').eq(3).find('input').val()) / 100;
                tr.find('td').eq(3).find('input').val(Math.ceil(tr.find('td').eq(1).find('input').val()) * parseFloat(tr.find('td').eq(2).find('input').val()));
                if(sign==1){
                    var amount = unit * parseFloat(tr.find('td').eq(4).find('input').val());
                    tr.find('td').eq(7).find('input').val(amount)
                }else {tr.find('td').eq(7).find('input').val(tr.find('td').eq(4).find('input').val());}

                tr.find('td').eq(5).find('input').val(parseFloat(tr.find('td').eq(3).find('input').val()) +
                    parseFloat(tr.find('td').eq(7).find('input').val()));
                totalCalc();
            }

            function totalCalc()
            {
                var sign = $("#tax_as").val();
                var table = $("#productRows");
                var amount           = 0;
                var gst              = 0;
                var total            = 0;
                table.find("tr").children("td:nth-child(4)").find('input')
                    .each(function() {
                        $this = $(this);
                        amount += parseFloat($this.val());});

                table.find("tr").children("td:nth-child(8)").find('input')
                    .each(function() {
                        $this = $(this);
                        gst += parseFloat($this.val());});

                table.find("tr").children("td:nth-child(6)").find('input')
                    .each(function() {
                        $this = $(this);
                        total += parseInt($this.val());});

                $("#amount").text(Math.ceil(amount));
                $("#gst").text(Math.ceil(gst));
                $("#total").text(Math.ceil(total));
            }



    </script>

@endsection

