@extends('Inc.window')

@section('content')
    <!-- style switcher -->
    <link rel="stylesheet" href="assets/css/style_switcher.min.css" media="all">


                    <table id="dt_tableExport" class="uk-table table-bordered table-responsive table-hover" >
                       <thead>
                       <tr>
                           <th>#</th>
                           <th>Product Name</th>
                           <th>Sailing Price</th>
                           <th>Quantity</th>
                           <th>Amount</th>
                           <th>Sales Tax</th>
                           <th>Additional Tax</th>
                           <th>Payable</th>

                       </tr>
                       </thead>
                       <tbody id="itemsTable">

                       @foreach($table as $i)

                           <tr id="unSel{{$i->id}}">
                               <td>{{$i->id}}</td>
                               <td>{{$i->product_name}}</td>
                               <td>{{$i->sailing_price}}</td>
                               <td>{{$i->quantity}}</td>
                               <td>{{$i->amount}}</td>
                               <td>{{$i->tax1}}</td>
                               <td>{{$i->tax2}}</td>
                               <td>{{$i->payable}}</td>

                           </tr>
                       @endforeach

                       </tbody>



                   </table>




@endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>




@endsection

