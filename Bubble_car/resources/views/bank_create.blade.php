@extends('Inc.app')

@section('content')


    <h2 class="text-light">Account Create</h2>
    <hr>


    <div class="md-card">

        <div class="md-card-content" style="display: block;">
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-6-10 uk-push-2-10 uk-row-first">
                    <form action="bank.create" method="post">

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif

                        {{csrf_field()}}
                        <div style="width: 70%;margin-left: 15%">
                        </div>
                        <table style="width: 100%">
                            <tbody>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; width: auto;">Account Type</td>
                                <td>
                                    <div class="md-input-wrapper">
                                        <select class="md-input " onchange="selectType(this.value)" name="type">
                                            <option value="" selected>select...</option>
                                            <option value="bank">Bank</option>
                                            <option value="cash">Cash</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr id="nameTr" hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Name</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="name" name="name" value="{{old('name')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr id="addressTr" hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Address</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="address" name="address" value="{{old('address')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr id="codeTr" hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Code</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="code" name="code" value="{{old('code')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr id="numberTr" hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Account Number</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="account_no" name="account_no" value="{{old('account_no')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>



                            <tr id="titleTr" hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Account Title</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="account_title" name="account_title" value="{{old('account_title')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            <tr id="personTr" hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Contact Person</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="contact_person" name="contact_person" value="{{old('contact_person')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>


                            <tr id="emailTr" hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Email</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="email" name="email" value="{{old('email')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>
                            <tr id="balanceTr" hidden>
                                <td style="padding:15px 0;max-width: 50px; ">Opening Balance</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="opening_balance" name="opening_balance" value="{{old('opening_balance')}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>


                            </tbody>
                        </table>
                        <div class="uk-form-row">
                            <div class="uk-width-1-1 uk-margin-top">
                                <button type="submit" name="save" class="md-btn md-btn-primary uk-float-right">Create</button>
                                <a href="" class="md-btn md-btn uk-float-right">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>

    </div>





        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>

    <script>
        function selectType(e) {
            if(e=="bank"){
                $("#nameTr").prop('hidden',false);
                $("#addressTr").prop('hidden',false);
                $("#codeTr").prop('hidden',false);
                $("#numberTr").prop('hidden',false);
                $("#titleTr").prop('hidden',false);
                $("#personTr").prop('hidden',false);
                $("#emailTr").prop('hidden',false);
                $("#balanceTr").prop('hidden',false);


            }else if(e=="cash") {

                $("#nameTr").prop('hidden',false);
                $("#addressTr").prop('hidden',true);
                $("#codeTr").prop('hidden',true);
                $("#numberTr").prop('hidden',true);
                $("#titleTr").prop('hidden',true);
                $("#personTr").prop('hidden',true);
                $("#emailTr").prop('hidden',true);
                $("#balanceTr").prop('hidden',false);
            }
        }
    </script>

@endsection

