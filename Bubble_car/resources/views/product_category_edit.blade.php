@extends('Inc.app')

@section('content')


    <h2 class="text-light">Product Category Edit</h2>
    <hr>



    <div class="md-card">
        <div class="md-card-content" style="display: block;">
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-6-10 uk-push-2-10 uk-row-first">
                    <form action="product.category.update" method="post">

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ session()->get('message') }}
                                </div>
                            </div>
                        @endif

                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif

                        {{csrf_field()}}
                        <div style="width: 70%;margin-left: 15%">
                        </div>
                        <table style="width: 100%">
                            <tbody>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Unit ID</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="id" name="id" value="{{$id}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:15px 0;max-width: 50px; ">Unit Name</td>
                                <td>
                                    <div class="md-input-wrapper"><input type="text" autocomplete="off" class="md-input md-input-small" id="name" name="name" value="{{$name}}"><span class="md-input-bar "></span></div>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                        <div class="uk-form-row">
                            <div class="uk-width-1-1 uk-margin-top">
                                <button  type="submit" name="update" class="md-btn md-btn-primary uk-float-right">Update</button>
                                <a href="" class="md-btn md-btn uk-float-right">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>

    </div>





        @endsection
@section('page-scripts')

    <!-- page specific plugins -->
    <!-- datatables -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="bower_components/jszip/dist/jszip.min.js"></script>
    <script src="bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="bower_components/datatables-buttons/js/buttons.print.js"></script>

    <!-- datatables custom integration -->
    <script src="assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="assets/js/pages/plugins_datatables.min.js"></script>



@endsection

