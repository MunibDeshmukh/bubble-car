<?php

Route::post('user.login','LoginController@login');
Route::get('user.logout','LoginController@logout');
Route::get('sales',function (){return view('sale_types');});
Route::get('purchases',function (){return view('purchase_types');});
Route::get('payroll',function (){return view('hrpayroll_types');});
Route::get('products',function (){return view('product_types');});
Route::get('accounts',function (){return view('account_types');});



Route::get('brand.create','BrandController@showCreatePage');
Route::post('brand.create','BrandController@submitCreatePage');
Route::get('brand.list','BrandController@showAllPage');
Route::get('brand.edit.{id}','BrandController@EditPage');
Route::post('brand.update','BrandController@submitUpdatePage');
Route::get('brand.delete.{id}','BrandController@destroy');

Route::get('variation.create','VariationController@showCreatePage');
Route::post('variation.create','VariationController@submitCreatePage');
Route::get('variation.list','VariationController@showAllPage');
Route::get('variation.edit.{id}','VariationController@EditPage');
Route::post('variation.update','VariationController@submitUpdatePage');
Route::get('variation.delete.{id}','VariationController@destroy');

Route::get('category.create','CategoryController@showCreatePage');
Route::post('category.create','CategoryController@submitCreatePage');
Route::get('category.list','CategoryController@showAllPage');
Route::get('category.edit.{id}','CategoryController@EditPage');
Route::post('category.update','CategoryController@submitUpdatePage');
Route::get('category.delete.{id}','CategoryController@destroy');

Route::get('carselection.create','CarSelectionController@showCreatePage');
Route::post('carselection.create','CarSelectionController@submitCreatePage');
Route::get('carselection.list','CarSelectionController@showAllPage');
Route::get('carselection.edit.{id}','CarSelectionController@EditPage');
Route::post('carselection.update','CarSelectionController@submitUpdatePage');
Route::get('carselection.delete.{id}','CarSelectionController@destroy');




Route::get('workstation.create','WorkstationController@showCreatePage');
Route::post('workstation.create','WorkstationController@submitCreatePage');
Route::get('workstation.list','WorkstationController@showAllPage');
Route::get('workstation.edit.{id}','WorkstationController@EditPage');
Route::post('workstation.update','WorkstationController@submitUpdatePage');
Route::get('workstation.delete.{id}','WorkstationController@destroy');

Route::get('/workstation/setting/{id}',function (){return view('workstation_setting');});
Route::post('/workstation/setting','WorkstationController@setting');




Route::get('user.create','UserController@showCreatePage');
Route::post('user.create','UserController@submitCreatePage');
Route::get('user.list','UserController@showAllPage');
Route::get('user.edit.{id}','UserController@EditPage');
Route::post('user.update','UserController@submitUpdatePage');
Route::get('user.delete.{id}','UserController@destroy');


Route::get('vendor.create','VendorController@showCreatePage');
Route::post('vendor.create','VendorController@submitCreatePage');
Route::get('vendor.list','VendorController@showAllPage');
Route::get('vendor.edit.{id}','VendorController@EditPage');
Route::post('vendor.update','VendorController@submitUpdatePage');
Route::get('vendor.delete.{id}','VendorController@destroy');

Route::get('outSourcevendor.create','OutSourceVendorController@showCreatePage');
Route::post('outSourcevendor.create','OutSourceVendorController@submitCreatePage');
Route::get('outSourcevendor.list','OutSourceVendorController@showAllPage');
Route::get('outSourcevendor.edit.{id}','OutSourceVendorController@EditPage');
Route::post('outSourcevendor.update','OutSourceVendorController@submitUpdatePage');
Route::get('outSourcevendor.delete.{id}','OutSourceVendorController@destroy');



Route::get('customer.create','CustomerController@showCreatePage');
Route::post('customer.create','CustomerController@submitCreatePage');
Route::get('customer.list','CustomerController@showAllPage');
Route::get('customer.edit.{id}','CustomerController@EditPage');
Route::post('customer.update','CustomerController@submitUpdatePage');
Route::get('customer.delete.{id}','CustomerController@destroy');


Route::get('services.create','ServiceController@showCreatePage');
Route::post('service.create','ServiceController@submitCreatePage');
    Route::get('services.list','ServiceController@showAll');

Route::get('outsourceservices.create','OutSourceServiceController@showCreatePage');
Route::post('outsourceservices.create','OutSourceServiceController@submitCreatePage');
 Route::get('outsourceservices.list','OutSourceServiceController@showAll');
Route::get('outsourceservices.edit.{id}','OutSourceServiceController@EditPage');
Route::post('outsourceservices.update','OutSourceServiceController@submitUpdatePage');
Route::get('outSourcevendor.delete.{id}','OutSourceServiceController@destroy');

// Route::get('product.class.create','ProductClassController@showCreatePage');
// Route::post('product.class.create','ProductClassController@submitCreatePage');
// Route::get('product.class.list','ProductClassController@showAllPage');
// Route::get('product.class.edit.{id}','ProductClassController@EditPage');
// Route::post('product.class.update','ProductClassController@submitUpdatePage');
// Route::get('product.class.delete.{id}','ProductClassController@destroy');


// Route::get('product.category.create','ProductCategoryController@showCreatePage');
// Route::post('product.category.create','ProductCategoryController@submitCreatePage');
// Route::get('product.category.list','ProductCategoryController@showAllPage');
// Route::get('product.category.edit.{id}','ProductCategoryController@EditPage');
// Route::post('product.category.update','ProductCategoryController@submitUpdatePage');
// Route::get('product.category.delete.{id}','ProductCategoryController@destroy');


// Route::get('product.brand.create','ProductBrandController@showCreatePage');
// Route::post('product.brand.create','ProductBrandController@submitCreatePage');
//a Route::get('product.brand.list','ProductBrandController@showAllPage');
// Route::get('product.brand.edit.{id}','ProductBrandController@EditPage');
// Route::post('product.brand.update','ProductBrandController@submitUpdatePage');
// Route::get('product.brand.delete.{id}','ProductBrandController@destroy');


// Route::get('product.model.create','ProductModelController@showCreatePage');
// Route::post('product.model.create','ProductModelController@submitCreatePage');
// Route::get('product.model.list','ProductModelController@showAllPage');
// Route::get('product.model.edit.{id}','ProductModelController@EditPage');
// Route::post('product.model.update','ProductModelController@submitUpdatePage');
// Route::get('product.model.delete.{id}','ProductModelController@destroy');
// Route::post('product.get_brands','ProductModelController@getBrands');

// Route::get('product.type.create','ProductTypeController@showCreatePage');
// Route::post('product.type.create','ProductTypeController@submitCreatePage');
// Route::get('product.type.list','ProductTypeController@showAllPage');
// Route::get('product.type.edit.{id}','ProductTypeController@EditPage');
// Route::post('product.type.update','ProductTypeController@submitUpdatePage');
// Route::get('product.type.delete.{id}','ProductTypeController@destroy');


Route::get('product.create','ProductController@showCreatePage');
Route::post('product.create','ProductController@submitCreatePage');
Route::get('product.list','ProductController@showAllPage');
Route::get('product.edit.{id}','ProductController@EditPage');
Route::post('product.update','ProductController@submitUpdatePage');
Route::get('product.delete.{id}','ProductController@destroy');
Route::get('product.get_cars','ProductController@getCars');

/*--------------------------------------------------------------------------
| Taxes Routes
|--------------------------------------------------------------------------*/

Route::get('tax.create','TaxController@showCreatePage');
Route::post('tax.create','TaxController@submitCreatePage');
Route::get('tax.list','TaxController@showAllPage');
Route::get('tax.edit.{id}','TaxController@EditPage');
Route::post('tax.update','TaxController@submitUpdatePage');
Route::get('tax.delete.{id}','TaxController@destroy');

/*--------------------------------------------------------------------------
| Unit Routes
|--------------------------------------------------------------------------*/

Route::get('unit.create','UnitController@showCreatePage');
Route::post('unit.create','UnitController@submitCreatePage');
Route::get('unit.list','UnitController@showAllPage');
Route::get('unit.edit.{id}','UnitController@EditPage');
Route::post('unit.update','UnitController@submitUpdatePage');
Route::get('unit.delete.{id}','UnitController@destroy');


Route::get('location.create','LocationController@showCreatePage');
Route::post('location.create','LocationController@submitCreatePage');
Route::get('location.list','LocationController@showAllPage');
Route::get('location.edit.{id}','LocationController@EditPage');
Route::post('location.update','LocationController@submitUpdatePage');
Route::get('location.delete.{id}','LocationController@destroy');


Route::get('warehouse.create','WarehouseController@showCreatePage');
Route::post('warehouse.create','WarehouseController@submitCreatePage');
Route::get('warehouse.list','WarehouseController@showAllPage');
Route::get('warehouse.edit.{id}','WarehouseController@EditPage');
Route::post('warehouse.update','WarehouseController@submitUpdatePage');
Route::get('warehouse.delete.{id}','WarehouseController@destroy');


Route::get('bank.create','BankController@showCreatePage');
Route::post('bank.create','BankController@submitCreatePage');
Route::get('bank.list','BankController@showAllPage');
Route::get('bank.edit.{id}','BankController@EditPage');
Route::post('bank.update','BankController@submitUpdatePage');
Route::get('bank.delete.{id}','BankController@destroy');


Route::get('staff.create','StaffController@showCreatePage');
Route::post('staff.create','StaffController@submitCreatePage');
Route::get('staff.list','StaffController@showAllPage');
Route::get('staff.edit.{id}','StaffController@EditPage');
Route::post('staff.update','StaffController@submitUpdatePage');
Route::get('staff.delete.{id}','StaffController@destroy');


Route::get('loan.create','LoanController@showCreatePage');
Route::post('loan.create','LoanController@submitCreatePage');
Route::get('loan.list','LoanController@showAllPage');
Route::get('loan.edit.{id}','LoanController@EditPage');
Route::post('loan.update','LoanController@submitUpdatePage');
Route::get('loan.delete.{id}','LoanController@destroy');


Route::get('advance.create','AdvanceController@showCreatePage');
Route::post('advance.create','AdvanceController@submitCreatePage');
Route::get('advance.list','AdvanceController@showAllPage');
Route::get('advance.edit.{id}','AdvanceController@EditPage');
Route::post('advance.update','AdvanceController@submitUpdatePage');
Route::get('advance.delete.{id}','AdvanceController@destroy');


Route::get('salary.create','SalaryController@showCreatePage');
Route::post('salary.create','SalaryController@submitCreatePage');


//purchase//
Route::get('purchase.create','PurchaseController@showCreatePage');
Route::post('purchase.create','PurchaseController@submitCreatePageForPurchase');
Route::get('purchase.edit.{id}','PurchaseController@EditPage');
Route::post('purchase.create.log','PurchaseController@submitCreatePageForLog');
Route::get('purchase.create.success','PurchaseController@createdMessage');
Route::get('purchase.list','PurchaseController@showAll');//show all record
Route::get('purchase.print.{id}','PurchaseController@showSingleRecipe');
Route::get('purchase.print.{id}','PurchaseController@showSingleRecipe');
Route::post('recipe.create.log','RecipeController@submitCreatePageForLog');
Route::get('purchase.grn','PurchaseController@grn');
Route::get('grn.view','PurchaseController@grnView');
Route::get('grn.insert','PurchaseController@grninsert');
Route::get('grn.getProduct','PurchaseController@grnGetproduct');

//purchase//

//cash sales
Route::get('cashsales.create','SalesController@showCreatePage');
Route::post('saleProduct.create','SalesController@submitCreatePageForsaleProduct');
Route::post('saleProduct.create.log','SalesController@submitCreatePageForsaleproductLog');
Route::post('getcar.bytype','SalesController@getcarname');
Route::post('services.by_name','SalesController@serviceQueryByName');
Route::post('services.by_id','SalesController@serviceQueryById');
Route::post('saleService.create.log','SalesController@submitCreatePageForsaleServicesLog');
Route::post('outsource.by_name','SalesController@outsourceQueryByName');
Route::post('outsource.by_id','SalesController@outsourceQueryById');
Route::post('saleoutsource.create.log','SalesController@submitCreatePageForsaleoutsourceLog');
Route::get('viewcashsales.list','SalesController@viewcashsales');
Route::post('jobcard.detail','SalesController@viewjobCard');
///cash Sales

//credit sales

//Route::get('creditsales.create','CreditSalesController@showCreatePage');
//Route::post('creditsaleProduct.create','CreditSalesController@submitCreatePageForsaleProduct');
//Route::post('creditsaleProduct.create.log','CreditSalesController@submitCreatePageForsaleproductLog');
//Route::post('creditsaleService.create.log','CreditSalesController@submitCreatePageForsaleServicesLog');
//Route::post('creditsaleoutsource.create.log','CreditSalesController@submitCreatePageForsaleoutsourceLog');
//Route::get('viewcreditsales.list','CreditSalesController@viewcreditsales');

///credit Sales
    
    
//purchase//
Route::get('local.purchase.create','LocalPurchaseController@showCreatePage');
Route::post('local.purchase.create','LocalPurchaseController@submitCreatePageForPurchase');
Route::post('local.purchase.create.log','LocalPurchaseController@submitCreatePageForLog');
Route::get('local.purchase.create.success','LocalPurchaseController@createdMessage');
Route::get('local.purchase.list','LocalPurchaseController@showAll');//show all record
Route::get('local.purchase.print.{id}','LocalPurchaseController@showSingleRecipe');
Route::post('local.recipe.create.log','LocalPurchaseController@submitCreatePageForLog');
//purchase//



//Route::get('gst.sale.create','GSTSaleController@showCreatePage');//show page//
//Route::post('gst.sale.create','GSTSaleController@submitCreatePageForPurchase');//save sale post//
//Route::post('gst.sale.create.log','GSTSaleController@submitCreatePageForLog');//save sale post log//
//Route::post('gst.sale.cancel','GSTSaleController@saleCancel');//cancel gst record
//Route::get('gst.sale.create.success','GSTSaleController@createdMessage');
//Route::get('gst.sale.list','GSTSaleController@showAll');//show all record
//Route::get('gst.sale.deleted.list','GSTSaleController@allDeletedSale');//show all record
//Route::get('gst.sale.print.{id}','GSTSaleController@showSingleRecipe');
//
//
//Route::get('local.sale.create','LocalSaleController@showCreatePage');//show page//
//Route::post('local.sale.create','LocalSaleController@submitCreatePageForPurchase');//save sale post//
//Route::post('local.sale.create.log','LocalSaleController@submitCreatePageForLog');//save sale post log//
//Route::post('local.sale.cancel','LocalSaleController@saleCancel');//cancel gst record
//Route::get('local.sale.create.success','LocalSaleController@createdMessage');
//Route::get('local.sale.list','LocalSaleController@showAll');//show all record
//Route::get('local.sale.deleted.list','LocalSaleController@allDeletedSale');//show all record
//Route::get('local.sale.print.{id}','LocalSaleController@showSingleRecipe');
//
//
//Route::get('rental.sale.create','RentalSaleController@showCreatePage');
//Route::post('rental.sale.create','RentalSaleController@submitCreatePageForPurchase');
//Route::post('rental.sale.create.log','RentalSaleController@submitCreatePageForLog');
////success message//
//Route::get('rental.sale.create.success','RentalSaleController@createdMessage');
//
//
//Route::get('rental.sale.list','RentalSaleController@showPageAll');
//Route::get('rental.sale.logs.view.{id}','RentalSaleController@showSinglePurchaseLog');
//Route::get('rental.sale.delete.{id}','RentalSaleController@destroy');
//
//
//Route::get('rental.sale.reading.create.success.{id}','RentalSaleReadingController@createdMessage');
//Route::get('rental.sale.reading.create.{id}','RentalSaleReadingController@showCreatePage');
//Route::post('rental.sale.reading.create','RentalSaleReadingController@submitCreatePage');
////success message//
//Route::get('rental.sale.reading.list.{id}','RentalSaleReadingController@showAllPage');
//Route::post('rental.sale.reading.product.log.create','RentalSaleReadingController@submitCreateProductLogPage');
//Route::post('rental.sale.reading.misc.log.create','RentalSaleReadingController@submitCreateMiscLogPage');
//Route::get('rental.sale.reading.print.view.{id}','RentalSaleReadingController@printView');
////ajax calls/
//Route::post('rental.sale.get','RentalSaleReadingController@getLastSale');
//Route::post('rental.sale.products.get','RentalSaleReadingController@getSaleProducts');
//Route::post('rental.sale.reading.get','RentalSaleReadingController@getSaleReading');
//
//
//Route::get('rental.sale.quantity.create.{id}','RentalSaleQuantityController@showCreatePage');
//Route::post('rental.sale.quantity.create','RentalSaleQuantityController@submitCreatePage');
////success message//
//Route::get('rental.sale.quantity.create.success','RentalSaleQuantityController@createdMessage');
//Route::get('rental.sale.quantity.list.{id}','RentalSaleQuantityController@showAllPage');
//Route::post('rental.sale.quantity.product.log.create','RentalSaleQuantityController@submitCreateProductLogPage');
//Route::post('rental.sale.quantity.misc.log.create','RentalSaleQuantityController@submitCreateMiscLogPage');
//Route::get('rental.sale.quantity.print.view.{id}','RentalSaleQuantityController@printView');
////ajax calls/
//Route::post('rental.sale.get','RentalSaleQuantityController@getLastSale');
//Route::post('rental.sale.products.quantity.get','RentalSaleQuantityController@getSaleProducts');



Route::get('customer.payable.create','CustomerPayableController@showCreatePage');
Route::post('get.customer.info','CustomerPayableController@getCustomerInfo');
Route::post('customer.payable.create','CustomerPayableController@submitCreatePage');
Route::get('customer.payable.list','CustomerPayableController@showAllPage');
Route::get('customer.payable.edit.{id}','CustomerPayableController@EditPage');
Route::post('customer.payable.update','CustomerPayableController@submitUpdatePage');
Route::get('customer.payable.delete.{id}','CustomerPayableController@destroy');





Route::get('vendor.payable.create','VednorPayableController@showCreatePage');
Route::post('get.vendor.info','VednorPayableController@getVendorInfo');
Route::post('vendor.payable.create','VednorPayableController@submitCreatePage');
Route::get('vendor.payable.list','VednorPayableController@showAllPage');
Route::get('vendor.payable.edit.{id}','VednorPayableController@EditPage');
Route::post('vendor.payable.update','VednorPayableController@submitUpdatePage');
Route::get('vendor.payable.delete.{id}','VednorPayableController@destroy');



Route::get('account.head.create','AccountsHeadController@index');
Route::post('account.head.create','AccountsHeadController@submitCreatePage');
Route::get('account.head.list','AccountsHeadController@showAllPage');
Route::get('account.head.edit.{id}','AccountsHeadController@EditPage');
Route::post('account.head.update','AccountsHeadController@submitUpdatePage');
Route::get('account.head.delete.{id}','AccountsHeadController@destroy');



Route::get('expense.payable.create','ExpensePayableController@showCreatePage');
Route::post('expense.payable.create','ExpensePayableController@submitCreatePage');
Route::get('expense.payable.list','ExpensePayableController@showAllPage');

Route::get('expense.receivable.create','ExpenseReceivableController@showCreatePage');
Route::post('expense.receivable.create','ExpenseReceivableController@submitCreatePage');
Route::get('expense.receivable.list','ExpenseReceivableController@showAllPage');
Route::post('get.refundable.balance','ExpenseReceivableController@refundableBalance');

Route::get('bank.ledger.{id}','AccountController@accountLedger');




Route::get('inventory.view','InventoryController@showInventory');
Route::get('stock.view','InventoryController@showStock');


Route::get('stockAdjusment','StockController@showStockAdjusment');



Route::post('AdjustStock.manage','StockController@StockManage');


Route::get('damage','StockController@showDamage');



Route::get('/login', function () {
    return view('login');
});


Route::get('/', function () {
    return view('home');
});


Route::get('workstation.selector', function () {
    return view('workstation_selector')->with('table',\App\Workstation::all());
});


Route::get('workstation.change.{id}', function ($id) {
    session(['login_workstation_id'=> $id]);
    $wid = \App\Workstation::where('id','=',$id)->first();
    session(['login_workstation_name'=> $wid->name]);
    return redirect('/');
});


//search product by name//
Route::post('product.by_name','ProductController@productQueryByName');

//search product by id//
Route::post('product.by_id','ProductController@productQueryById');



