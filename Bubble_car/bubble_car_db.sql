-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2018 at 01:36 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bubble_car_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `workstation_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `password`, `picture`, `workstation_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin123', NULL, 1, '2018-08-18 21:30:22', '2018-08-18 21:42:28');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `wid`, `created_at`, `updated_at`) VALUES
(1, 'Toyota', 1, '2018-10-22 23:08:55', '2018-10-22 23:08:55'),
(2, 'Indus Corolla', 1, '2018-10-25 04:13:37', '2018-10-25 04:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `car_selections`
--

CREATE TABLE `car_selections` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_selections`
--

INSERT INTO `car_selections` (`id`, `name`, `type`, `wid`, `created_at`, `updated_at`) VALUES
(1, 'Indus Corrolla', 'medium', 1, '2018-10-25 04:19:09', '2018-10-25 04:19:09'),
(2, 'Cultus', 'medium', 1, '2018-10-26 06:46:56', '2018-10-26 06:46:56');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `wid`, `created_at`, `updated_at`) VALUES
(1, 'Testing', 1, '2018-10-22 23:14:57', '2018-10-22 23:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `creditsale_outsource_service_logs`
--

CREATE TABLE `creditsale_outsource_service_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `Outservices_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `receivable` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `creditsale_product_logs`
--

CREATE TABLE `creditsale_product_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `receivable` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `creditsale_service_logs`
--

CREATE TABLE `creditsale_service_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `services_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `receivable` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `credit_sales`
--

CREATE TABLE `credit_sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `customerId` int(11) NOT NULL,
  `voucherNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `taxType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxAction` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxDetectAs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxValue` int(11) NOT NULL,
  `payable` int(11) NOT NULL,
  `date` date NOT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `credit_sales`
--

INSERT INTO `credit_sales` (`id`, `customerId`, `voucherNo`, `carType`, `carName`, `item_quantity`, `total_amount`, `taxType`, `taxAction`, `taxDetectAs`, `taxValue`, `payable`, `date`, `wid`, `created_at`, `updated_at`) VALUES
(1, 1, 'KHI-456', 'medium', 'Indus Corrolla', 1, 12000, 'Gst', '0', '1', 17, 12204, '2018-11-16', 1, NULL, NULL),
(3, 1, 'KHI-457', 'medium', 'Indus Corrolla', 1, 4000, 'Gst', '0', '1', 17, 4480, '2018-11-16', 1, NULL, NULL),
(6, 1, 'KHI-458', 'medium', 'Indus Corrolla', 1, 14400, 'Gst', '0', '1', 17, 15840, '2018-11-16', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `person_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rental_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ntn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `srb` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_nature` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_balance` int(11) NOT NULL,
  `gst_number` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `company_name`, `person_name`, `contact`, `email`, `address`, `customer_type`, `rental_type`, `ntn`, `srb`, `business_nature`, `opening_balance`, `gst_number`, `wid`, `created_at`, `updated_at`) VALUES
(1, 'Info', 'Jhon', '03233232323', 'jhon@gmail.com', 'karachi pakistan', 'rental', 'reading', '123', '345', 'free', 1500, '6466', 1, '2018-09-24 16:53:17', '2018-10-15 03:28:53'),
(2, 'tech', 'tech', '03233232323', 'sdf@sd.om', 'karachi', 'rental', 'reading', '313', '1313', 'free', 1500, '', 1, '2018-09-24 18:42:14', '2018-09-24 18:42:14');

-- --------------------------------------------------------

--
-- Table structure for table `grns`
--

CREATE TABLE `grns` (
  `id` int(10) UNSIGNED NOT NULL,
  `grNo` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `voucherNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `gr_no` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `gr_no`, `product_id`, `product_name`, `voucher_no`, `product_quantity`, `product_price`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Testing', 'KHI-123', 4, 20000, NULL, '2018-11-08 02:46:03'),
(2, 1, 2, 'asdasd', 'KHI-124', 5, 5000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `code`, `unit`, `created_at`, `updated_at`) VALUES
(8, 'ddd', 'dd', 9, '2018-08-28 10:41:11', '2018-08-28 10:41:11'),
(9, 'jdd', '111', 9, '2018-08-29 10:49:27', '2018-08-29 10:49:27');

-- --------------------------------------------------------

--
-- Table structure for table `measurments`
--

CREATE TABLE `measurments` (
  `id` int(10) UNSIGNED NOT NULL,
  `ServiceId` int(11) NOT NULL,
  `measurmentQuantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `measurments`
--

INSERT INTO `measurments` (`id`, `ServiceId`, `measurmentQuantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 50, 2000, NULL, NULL),
(2, 2, 56, 565656, NULL, NULL),
(3, 1, 50, 120000, NULL, NULL),
(4, 2, 100, 5000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(56, '2014_10_12_000000_create_users_table', 1),
(57, '2014_10_12_100000_create_password_resets_table', 1),
(58, '2018_07_29_155002_create_vendors_table', 1),
(60, '2018_07_29_155036_create_items_table', 1),
(61, '2018_07_29_155055_create_recipes_table', 1),
(62, '2018_07_29_155514_create_units_table', 1),
(63, '2018_07_29_155837_create_recipe_logs_table', 1),
(64, '2018_08_01_095046_create_purchases_table', 1),
(65, '2018_08_01_095826_create_taxes_table', 1),
(67, '2018_09_12_193655_create_local_purchases_table', 2),
(68, '2018_09_12_193728_create_local_purchase_logs_table', 2),
(69, '2018_09_13_080255_create_local_sales_table', 2),
(70, '2018_09_13_080559_create_local_sale_logs_table', 2),
(71, '2018_10_22_125545_create_workstations', 2),
(72, '2018_10_22_171853_create_brands_table', 3),
(73, '2018_10_22_171914_create_variations_table', 3),
(74, '2018_10_22_172032_create_categories_table', 3),
(75, '2018_10_22_172126_create_car_selections_table', 3),
(76, '2018_10_26_103439_create_product_cars_table', 4),
(77, '2018_10_26_121015_create_vendor_payables_table', 5),
(79, '2018_11_05_110717_create_grns_table', 6),
(80, '2018_11_07_073418_create_purchase_logs_table', 7),
(81, '2018_11_07_083138_create_inventories_table', 8),
(82, '2018_11_07_115151_create_stocks_table', 9),
(83, '2018_11_08_075838_create_services_table', 10),
(84, '2018_11_08_080145_create_service_prices_table', 10),
(85, '2018_11_08_080413_create_service_recipes_table', 10),
(86, '2018_11_09_113107_create_measurments_table', 11),
(87, '2018_11_12_112541_create_outsourcevendors_table', 12),
(88, '2018_11_12_124543_create_outsourceservices_table', 13),
(89, '2018_11_13_075444_create_sale_products_table', 14),
(90, '2018_11_13_085413_create_sales_table', 15),
(91, '2018_11_13_085833_create_sales_table', 16),
(92, '2018_11_13_090026_create_sale_product_logs_table', 17),
(93, '2018_11_14_090527_create_sale_services_logs_table', 18),
(94, '2018_11_14_090746_create_sale_out_source_services_logs_table', 18),
(95, '2018_11_16_072103_create_credit_sales_table', 19),
(96, '2018_11_16_072149_create_creditsale_product_logs_table', 19),
(97, '2018_11_16_072211_create_creditsale_service_logs_table', 19),
(98, '2018_11_16_072230_create_creditsale_outsource_service_logs_table', 19);

-- --------------------------------------------------------

--
-- Table structure for table `outsourceservices`
--

CREATE TABLE `outsourceservices` (
  `id` int(10) UNSIGNED NOT NULL,
  `serviceName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outsourceservices`
--

INSERT INTO `outsourceservices` (`id`, `serviceName`, `created_at`, `updated_at`) VALUES
(1, 'Out Source Service Testing', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `outsourcevendors`
--

CREATE TABLE `outsourcevendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `person_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cnic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_balance` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outsourcevendors`
--

INSERT INTO `outsourcevendors` (`id`, `company_name`, `person_name`, `contact`, `email`, `cnic`, `address`, `opening_balance`, `wid`, `created_at`, `updated_at`) VALUES
(1, 'Bol', 'Sajid', '03043981077', 'q.sajid36@gmail.com', '423016868701-1', 'liyari agra taj colony', '250000', 1, '2018-11-12 06:46:53', '2018-11-12 07:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `pro_code` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `outside_code` varchar(191) NOT NULL,
  `brand` varchar(191) NOT NULL,
  `variation` varchar(191) NOT NULL,
  `category` varchar(191) NOT NULL,
  `type` varchar(191) NOT NULL,
  `cars` varchar(191) NOT NULL,
  `unit` varchar(191) NOT NULL,
  `wid` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `pro_code`, `description`, `outside_code`, `brand`, `variation`, `category`, `type`, `cars`, `unit`, `wid`, `updated_at`, `created_at`) VALUES
(1, 'Testing', '12121', 'asdad', '1212', '1', '1', '1', 'medium', 'Indus Corrolla,Cultus', 'kg', 1, '2018-10-26 13:37:36', '2018-10-26 06:55:54'),
(2, 'asdasd', '112', 'asdas', '121', '1', '1', '1', 'medium', 'Indus Corrolla,Cultus', 'kg', 1, '2018-10-26 08:13:30', '2018-10-26 08:13:30');

-- --------------------------------------------------------

--
-- Table structure for table `product_cars`
--

CREATE TABLE `product_cars` (
  `id` int(10) UNSIGNED NOT NULL,
  `pro_id` int(11) NOT NULL,
  `wid` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_cars`
--

INSERT INTO `product_cars` (`id`, `pro_id`, `wid`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Indus Corrolla', '2018-10-26 06:55:54', '2018-10-26 06:55:54'),
(2, 1, 1, 'Cultus', '2018-10-26 06:55:54', '2018-10-26 06:55:54'),
(3, 2, 1, 'Indus Corrolla', '2018-10-26 08:13:30', '2018-10-26 08:13:30'),
(4, 2, 1, 'Cultus', '2018-10-26 08:13:30', '2018-10-26 08:13:30');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `voucher_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_quantity` int(11) DEFAULT NULL,
  `total_amount` int(11) DEFAULT NULL,
  `tax_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_action` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_detect_as` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_value` int(11) DEFAULT NULL,
  `payable` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `vendor_id`, `voucher_no`, `item_quantity`, `total_amount`, `tax_type`, `tax_action`, `tax_detect_as`, `tax_value`, `payable`, `date`, `wid`, `created_at`, `updated_at`) VALUES
(1, 1, 'KHI-123', 200, 1000000, 'Gst', '0', '1', NULL, 1017000, '2018-11-15', 1, '2018-11-15 08:50:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_logs`
--

CREATE TABLE `purchase_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `payable` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_logs`
--

INSERT INTO `purchase_logs` (`id`, `product_id`, `purchase_id`, `item_quantity`, `price`, `amount`, `tax`, `payable`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 78, 10000, 1000000, 17, 1017000, 'pending', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `customerId` int(11) NOT NULL,
  `voucherNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `taxType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxAction` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxDetectAs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxValue` int(11) NOT NULL,
  `payable` int(11) NOT NULL,
  `date` date NOT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `customerId`, `voucherNo`, `carType`, `carName`, `item_quantity`, `total_amount`, `taxType`, `taxAction`, `taxDetectAs`, `taxValue`, `payable`, `date`, `wid`, `created_at`, `updated_at`) VALUES
(1, 1, 'KHI-456', 'medium', 'Indus Corrolla', 3, 33800, 'Gst', '0', '1', 17, 39546, '2018-11-22', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sale_out_source_services_logs`
--

CREATE TABLE `sale_out_source_services_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `Outservices_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `receivable` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sale_out_source_services_logs`
--

INSERT INTO `sale_out_source_services_logs` (`id`, `Outservices_id`, `sale_id`, `item_quantity`, `price`, `amount`, `tax`, `receivable`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 12, 1200, 14400, 17, 16848, 'pending', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sale_product_logs`
--

CREATE TABLE `sale_product_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `receivable` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sale_product_logs`
--

INSERT INTO `sale_product_logs` (`id`, `product_id`, `sale_id`, `item_quantity`, `price`, `amount`, `tax`, `receivable`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 12, 1200, 14400, 17, 16848, 'pending', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sale_services_logs`
--

CREATE TABLE `sale_services_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `services_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `receivable` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sale_services_logs`
--

INSERT INTO `sale_services_logs` (`id`, `services_id`, `sale_id`, `item_quantity`, `price`, `amount`, `tax`, `receivable`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, 500, 5000, 17, 5850, 'pending', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `ServiceName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `ServiceName`, `created_at`, `updated_at`) VALUES
(1, 'Testing Services', NULL, NULL),
(2, 'Testing2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_prices`
--

CREATE TABLE `service_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `serviceId` int(11) NOT NULL,
  `small` int(11) NOT NULL,
  `medium` int(11) NOT NULL,
  `large` int(11) NOT NULL,
  `extraLarge` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_prices`
--

INSERT INTO `service_prices` (`id`, `serviceId`, `small`, `medium`, `large`, `extraLarge`, `created_at`, `updated_at`) VALUES
(1, 1, 500, 1000, 2000, 4000, NULL, NULL),
(2, 2, 100, 200, 300, 400, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_recipes`
--

CREATE TABLE `service_recipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipeType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serviceId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productQuantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_recipes`
--

INSERT INTO `service_recipes` (`id`, `recipeType`, `serviceId`, `productId`, `productQuantity`, `created_at`, `updated_at`) VALUES
(1, 'Small', 1, 1, 40, NULL, NULL),
(2, 'Medium', 2, 1, 21, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `gr_no` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `gr_no`, `product_id`, `product_name`, `voucher_no`, `product_quantity`, `product_price`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Testing', 'KHI-123', 2, 20000, NULL, NULL),
(2, 1, 1, 'Testing', 'KHI-124', 2, 1000, NULL, NULL),
(3, 1, 2, 'asdasd', 'KHI-124', 5, 5000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(6, 'Gst', 17, '2018-10-10 06:33:42', '2018-10-10 06:33:42'),
(7, 'Local', 12, '2018-10-26 07:43:14', '2018-10-26 07:43:14');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `created_at`, `updated_at`) VALUES
(9, 'pppp', '2018-08-28 10:40:44', '2018-08-28 10:40:44'),
(10, 'kg', '2018-10-10 06:12:07', '2018-10-10 06:12:07'),
(11, 'gram', '2018-10-10 06:12:18', '2018-10-10 06:12:18'),
(12, 'Ltr', '2018-10-10 06:12:35', '2018-10-10 06:12:35'),
(13, 'ml', '2018-10-10 06:12:43', '2018-10-10 06:12:43'),
(14, 'oz', '2018-10-26 08:10:13', '2018-10-26 08:10:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `person_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cnic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_balance` int(11) NOT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `company_name`, `person_name`, `contact`, `email`, `cnic`, `address`, `opening_balance`, `wid`, `created_at`, `updated_at`) VALUES
(1, 'Kreatix', 'Talha', '0312121212', 'talha@gmail.com', '12121212121212', '12121', 1000, 1, '2018-10-28 06:29:19', '2018-10-28 06:29:19');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_payables`
--

CREATE TABLE `vendor_payables` (
  `id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `purchase` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `purchase_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendor_payables`
--

INSERT INTO `vendor_payables` (`id`, `vendor_id`, `purchase`, `payment`, `balance`, `purchase_date`, `transaction_type`, `wid`, `created_at`, `updated_at`) VALUES
(3, 1, '0', 1000, -1000, '2018-10-28', 'opening Balance', 1, NULL, NULL),
(4, 1, '6840', 0, 5840, '2018-10-28', 'purchase', 1, NULL, NULL),
(5, 1, '702', 0, 6542, '2018-10-29', 'purchase', 1, NULL, NULL),
(6, 1, '144000', 0, 150542, '2018-11-05', 'purchase', 1, NULL, NULL),
(7, 1, '198900', 0, 349442, '2018-11-07', 'purchase', 1, NULL, NULL),
(8, 1, '198900', 0, 548342, '2018-11-07', 'purchase', 1, NULL, NULL),
(9, 1, '7020', 0, 555362, '2018-11-07', 'purchase', 1, NULL, NULL),
(10, 1, '6000', 0, 561362, '2018-11-13', 'purchase', 1, NULL, NULL),
(11, 1, '1017000', 0, 1578362, '2018-11-15', 'purchase', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `workstations`
--

CREATE TABLE `workstations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale_voucher` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock_voucher` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `workstations`
--

INSERT INTO `workstations` (`id`, `name`, `sale_voucher`, `stock_voucher`, `created_at`, `updated_at`) VALUES
(1, 'Karachi', 'KHI-456', 'KHI-123', '2018-10-29 04:50:15', '2018-10-29 04:50:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_selections`
--
ALTER TABLE `car_selections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creditsale_outsource_service_logs`
--
ALTER TABLE `creditsale_outsource_service_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creditsale_product_logs`
--
ALTER TABLE `creditsale_product_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creditsale_service_logs`
--
ALTER TABLE `creditsale_service_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_sales`
--
ALTER TABLE `credit_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grns`
--
ALTER TABLE `grns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `measurments`
--
ALTER TABLE `measurments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outsourceservices`
--
ALTER TABLE `outsourceservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outsourcevendors`
--
ALTER TABLE `outsourcevendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_cars`
--
ALTER TABLE `product_cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `purchase_logs`
--
ALTER TABLE `purchase_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_out_source_services_logs`
--
ALTER TABLE `sale_out_source_services_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_product_logs`
--
ALTER TABLE `sale_product_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_services_logs`
--
ALTER TABLE `sale_services_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_prices`
--
ALTER TABLE `service_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_recipes`
--
ALTER TABLE `service_recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_payables`
--
ALTER TABLE `vendor_payables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workstations`
--
ALTER TABLE `workstations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `car_selections`
--
ALTER TABLE `car_selections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `creditsale_outsource_service_logs`
--
ALTER TABLE `creditsale_outsource_service_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `creditsale_product_logs`
--
ALTER TABLE `creditsale_product_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `creditsale_service_logs`
--
ALTER TABLE `creditsale_service_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `credit_sales`
--
ALTER TABLE `credit_sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grns`
--
ALTER TABLE `grns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `measurments`
--
ALTER TABLE `measurments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `outsourceservices`
--
ALTER TABLE `outsourceservices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `outsourcevendors`
--
ALTER TABLE `outsourcevendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_cars`
--
ALTER TABLE `product_cars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchase_logs`
--
ALTER TABLE `purchase_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sale_out_source_services_logs`
--
ALTER TABLE `sale_out_source_services_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sale_product_logs`
--
ALTER TABLE `sale_product_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sale_services_logs`
--
ALTER TABLE `sale_services_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_prices`
--
ALTER TABLE `service_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_recipes`
--
ALTER TABLE `service_recipes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendor_payables`
--
ALTER TABLE `vendor_payables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `workstations`
--
ALTER TABLE `workstations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
