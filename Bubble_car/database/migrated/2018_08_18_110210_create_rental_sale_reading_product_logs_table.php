<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentalSaleReadingProductLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_sale_reading_product_logs', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('sale_id');
            $table->integer('product_id');
            $table->integer('reading');
            $table->integer('quantity');
            $table->integer('rate');
            $table->integer('amount');
            $table->integer('net_amount');
            $table->integer('min_copy');
            $table->integer('amount_remaining_copy');
            $table->integer('total_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_sale_reading_product_logs');
    }
}
