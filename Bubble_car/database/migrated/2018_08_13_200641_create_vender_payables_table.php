<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenderPayablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vender_payables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->integer('purchase');
            $table->integer('payment');
            $table->integer('balance');
            $table->string('payment_mode');
            $table->string('bank_name');
            $table->string('branch_name');
            $table->string('account_title');
            $table->string('account_no');
            $table->string('cheque_no');
            $table->string('transaction_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vender_payables');
    }
}
