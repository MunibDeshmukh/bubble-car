<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
//id :id,
//                            name : name,
//                            salary : salary,
//                            working_days :working_days,
//                            per_day : per_day,
//                            attended_day : attended_day,
//                            net_salary : net_salary,
//                            loan_amount : loan_amount,
//                            due_loan_amount : due_loan_amount,
//                            loan_installment : loan_installment,
//                            advance_amount : advance_amount,
//                            detect:detect,
//                            payable :payable
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id');
            $table->integer('working_days');
            $table->integer('per_day');
            $table->integer('attended_day');
            $table->integer('net_salary');
            $table->integer('loan_amount');
            $table->integer('due_loan_amount');
            $table->integer('loan_installment');
            $table->integer('advance_amount');
            $table->integer('payable');
            $table->integer('due');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
