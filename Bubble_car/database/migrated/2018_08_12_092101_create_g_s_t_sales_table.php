<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGSTSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_s_t_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->integer('item_quantity');
            $table->integer('amount');
            $table->integer('tax1');
            $table->integer('tax2');
            $table->integer('payable');
            $table->integer('invoice_no');
            $table->date('sale_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_s_t_sales');
    }
}
