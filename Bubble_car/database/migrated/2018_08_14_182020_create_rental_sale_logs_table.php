<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentalSaleLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_sale_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sale_id');
            $table->integer('quantity');
            $table->integer('amount');
            $table->integer('tax1');
            $table->integer('tax2');
            $table->integer('payable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_sale_logs');
    }
}
