<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefundableExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refundable_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expense_head_id')->unsigned();
            $table->foreign('expense_head_id')->references('id')->on('expense_heads')->onDelete('cascade');
            $table->integer('credit');
            $table->integer('debit');
            $table->integer('balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refundable_expenses');
    }
}
