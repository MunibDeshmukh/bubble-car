<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('desc');
            $table->integer('class');
            $table->integer('brand');
            $table->integer('model');
            $table->integer('type');
            $table->integer('category');
            $table->integer('location');
            $table->integer('warehouse');
            $table->integer('retail_price');
            $table->string('f_o_b');
            $table->integer('sailing_price');
            $table->string('level');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
