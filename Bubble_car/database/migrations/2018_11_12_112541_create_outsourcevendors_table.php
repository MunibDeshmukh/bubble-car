<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutsourcevendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outsourcevendors', function (Blueprint $table) {
            $table->increments('id');
               $table->string('company_name');
            $table->string('person_name');
            $table->string('contact');
            $table->string('email');
            $table->string('cnic');
            $table->string('address');
            $table->string('ntn');
            $table->string('srb');
            $table->string('business_nature');
            $table->integer('opening_balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outsourcevendors');
    }
}
