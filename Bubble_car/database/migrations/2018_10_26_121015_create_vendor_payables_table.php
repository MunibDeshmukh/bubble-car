<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorPayablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_payables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->string('purchase');
            $table->integer('payment');
            $table->integer('balance');
            $table->string('purchase_date');
            $table->string('transaction_type');
            $table->integer('wid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_payables');
    }
}
