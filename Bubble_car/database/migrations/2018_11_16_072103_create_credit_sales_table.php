<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_sales', function (Blueprint $table) {
             $table->increments('id');
            $table->integer('customerId');
            $table->string('voucherNo');
            $table->integer('item_quantity');
            $table->integer('total_amount');
            $table->string('taxType');
            $table->string('taxAction');
            $table->string('taxDetectAs');
            $table->integer('taxValue');
            $table->integer('payable');
            $table->date('date');
            $table->integer('wid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_sales');
    }
}
