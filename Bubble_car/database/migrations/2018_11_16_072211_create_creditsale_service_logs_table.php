<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsaleServiceLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditsale_service_logs', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('services_id');
            $table->integer('sale_id');
            $table->integer('item_quantity');
            $table->integer('price');
            $table->integer('amount');
            $table->integer('tax');
            $table->integer('receivable');
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditsale_service_logs');
    }
}
