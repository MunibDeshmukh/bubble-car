<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleOutSourceServicesLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_out_source_services_logs', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('Outservices_id');
            $table->integer('sale_id');
            $table->integer('item_quantity');
            $table->integer('price');
            $table->integer('amount');
            $table->integer('tax');
            $table->integer('receivable');
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_out_source_services_logs');
    }
}
