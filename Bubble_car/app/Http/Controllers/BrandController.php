<?php

namespace App\Http\Controllers;
use Session;
use App\Brand;
use Illuminate\Http\Request;
use App\Vendor;
use Illuminate\Support\Facades\DB;

class BrandController extends Controller
{
    function showCreatePage()
    {

        return view('brand_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            // 'stock_no' => 'required',
            // 'sale_no' => 'required',
        ]);



        $table = new Brand();
        $table->name = $request->name;
        $table->wid = Session::get('login_user_id');
        // $table->sale_voucher = $request->sale_no;
        // $table->stock_voucher = $request->stock_no;
        $table->save();


        return redirect('brand.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = new Brand();
        return view('brand_list')->with('table', $table->where('wid','=',Session::get('login_workstation_id'))->get());
    }


    function EditPage($id)
    {

        $table = Brand::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'brand_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {



        $this->Validate($request, [
            'name' => 'required',
        ]);



        Brand::where('id', $request->id)->update(array(
            'name'=> $request->name,

    ));

        return redirect('brand.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Brand::where('id', $request->id)->delete();

        return redirect('brand.list')->with('message', $request->name . 'is deleted !');


    }


    function setting(Request $request)
    {


        Brand::where('id', $request->id)->delete();

        return redirect('Brand.list')->with('message', $request->name . 'is deleted !');


    }





}








