<?php

namespace App\Http\Controllers;

use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductCategoryController extends Controller
{
    function showCreatePage()
    {

        return view('product_category_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);


        $table = new ProductCategory();
        $table->name = $request->name;
        $table->wid =session('app_kb_login_workstation_id');
        $table->save();


        return redirect('product.category.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table =  ProductCategory::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('product_category_list')->with('table', $table->all());
    }


    function EditPage($id)
    {

        $table = ProductCategory::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'product_category_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);



        ProductCategory::where('id', $request->id)->update(array(
            'name'=> $request->name
                ));

        return redirect('product.category.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        ProductCategory::where('id', $request->id)->delete();

        return redirect('product.category.list')->with('message', $request->name . 'is deleted !');


    }

}








