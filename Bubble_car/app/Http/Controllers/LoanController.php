<?php

namespace App\Http\Controllers;

use App\Staff;
use App\Loan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanController extends Controller
{
    function showCreatePage()
    {

        $table = Staff::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('loan_create')->with('table',$table);
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'staff' => 'required',
            'amount' => 'required',
            'installment' => 'required'

        ]);


        $table = new Loan();
        $table->staff_id = $request->staff;
        $table->amount = $request->amount;
        $table->installment = $request->installment;
        $table->due = $request->amount;
        $table->wid =session('app_kb_login_workstation_id');
        $table->save();


        return redirect('loan.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = DB::table('loans')
            ->join('staff','loans.staff_id','=','staff.id')
            ->where('loans.wid','=',session('app_kb_login_workstation_id'))
            ->get(['loans.*','staff.name As staff_name']);


        return view('loan_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = DB::table('loans')
            ->join('staff','loans.staff_id','=','staff.id')
            ->where('loans.id','=',$id)
            ->first(['loans.*','staff.id As staff_id']);

        if ($table) {

                return view('loan_edit', array(
                    'id' => $table->id,
                    'staff_id' => $table->staff_id,
                    'amount' => $table->amount,
                    'installment' => $table->installment,
                    'staff' => Staff::all()
                ));
        } else {
            return $table;
       }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'staff' => 'required',
            'amount' => 'required',
            'installment' => 'required'
            ]);



        Loan::where('id', $request->id)->update(array(
            'staff_id' => $request->staff,
            'amount' => $request->amount,
            'installment' => $request->installment,
                ));

        return redirect('loan.list')->with('message', $request->staff_id . 'is updated !');


    }

    function destroy(Request $request)
    {


        Loan::where('id', $request->id)->delete();

        return redirect('loan.list')->with('message', $request->name . 'is deleted !');


    }

}








