<?php

namespace App\Http\Controllers;

use App\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffController extends Controller
{
    function showCreatePage()
    {

        return view('staff_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'nic' => 'required',
            'salary' => 'required',
        ]);


        $table = new Staff();
        $table->name = $request->name;
        $table->address = $request->address;
        $table->contact = $request->contact;
        $table->nic= $request->nic;
        $table->salary= $request->salary;
        $table->wid=session('app_kb_login_workstation_id');
        $table->save();


        return redirect('staff.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = new Staff();
        return view('staff_list')->with('table', $table->where('wid','=',session('app_kb_login_workstation_id'))->get());
    }


    function EditPage($id)
    {

        $table = Staff::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'staff_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                    'address' => $i->address,
                    'contact' => $i->contact,
                    'nic' => $i->nic,
                    'salary' => $i->salary,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);



        Staff::where('id', $request->id)->update(array(
                    'name' => $request->name,
                    'address' => $request->address,
                    'contact' => $request->contact,
                    'nic' => $request->nic,
                    'salary' => $request->salary,
                ));

        return redirect('staff.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Staff::where('id', $request->id)->delete();

        return redirect('staff.list')->with('message', $request->name . 'is deleted !');


    }

}








