<?php

namespace App\Http\Controllers;
use Session;
use App\Workstation;
use Illuminate\Http\Request;
use App\Vendor;
use Illuminate\Support\Facades\DB;
use App\CarSelection;

class CarSelectionController extends Controller
{
    function showCreatePage()
    {

        return view('carselection_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            // 'stock_no' => 'required',
            // 'sale_no' => 'required',
        ]);



        $table = new CarSelection();
        $table->name = $request->name;
        $table->type = $request->type;
        $table->wid = Session::get('login_user_id');
        // $table->sale_voucher = $request->sale_no;
        // $table->stock_voucher = $request->stock_no;
        $table->save();


        return redirect('carselection.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = new CarSelection();
        return view('carselection_list')->with('table', $table->where('wid','=',Session::get('login_workstation_id'))->get());
    }


    function EditPage($id)
    {

        $table = CarSelection::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'carselection_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {



        $this->Validate($request, [
            'name' => 'required',
        ]);



        CarSelection::where('id', $request->id)->update(array(
            'name'=> $request->name,

    ));

        return redirect('carselection.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        CarSelection::where('id', $request->id)->delete();

        return redirect('carselection.list')->with('message', $request->name . 'is deleted !');


    }


    function setting(Request $request)
    {


        CarSelection::where('id', $request->id)->delete();

        return redirect('carselection.list')->with('message', $request->name . 'is deleted !');


    }





}








