<?php

namespace App\Http\Controllers;

use App\AccountLedger;
use App\Customer;
use App\Banks;
use App\ExpenseHead;
use App\PayableExpense;
use App\RefundableExpense;
use App\VenderPayable;
use App\GSTSale;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExpensePayableController  extends Controller
{
    function showCreatePage()
    {
        return view('expense_payable_create',array(
            'head' =>ExpenseHead::where('wid','=',session('app_kb_login_workstation_id'))->get(),
            'bank' =>Banks::where('wid','=',session('app_kb_login_workstation_id'))->get(),
        ));
    }

    function submitCreatePage(Request $request)
    {
        $this->validate($request, [
            'head_id' => 'required',
            'amount_paid' => 'required',
            'refundable' => 'required',
            'account_id' => 'required',
        ]);


        $table = new PayableExpense();
        $table->expense_head_id = $request['head_id'];
        $table->amount = $request->amount_paid;
        $table->refundable = $request->refundable;
        $table->wid = session('app_kb_login_workstation_id');
        $table->save();

        if($request->refundable==1) {
            $table = new RefundableExpense();
            $table->expense_head_id = $request['head_id'];
            $table->credit = $request->amount_paid;
            $table->debit = 0;
            $table->balance = $request->amount_paid;
            $table->save();
        }

        $last = PayableExpense::orderBy('id','desc')->first();
        $ledger = new AccountController();

        $ledger->newPayable($request->account_id,$request->amount_paid,$last['expense_head_id'],190,'payable_expense');



        return redirect('expense.payable.create')->with('message', $request->name . 'is added !');


    }


    function showAllPage()
    {

        $table = DB::table('expense_heads')
            ->join('payable_expenses','payable_expenses.expense_head_id','=','expense_heads.id')
            ->get([
                'expense_heads.name as head_name',
                'payable_expenses.*'
                ]);


        return view('expense_payable_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = DB::table('loans')
            ->join('staff','loans.staff_id','=','staff.id')
            ->where('loans.id','=',$id)
            ->first(['loans.*','staff.id As staff_id']);

        if ($table) {

                return view('loan_edit', array(
                    'id' => $table->id,
                    'staff_id' => $table->staff_id,
                    'amount' => $table->amount,
                    'installment' => $table->installment,
                    'staff' => Staff::all()
                ));
        } else {
            return $table;
       }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'staff' => 'required',
            'amount' => 'required',
            'installment' => 'required'
            ]);



        Loan::where('id', $request->id)->update(array(
            'staff_id' => $request->staff,
            'amount' => $request->amount,
            'installment' => $request->installment,
                ));

        return redirect('loan.list')->with('message', $request->staff_id . 'is updated !');


    }

    function destroy(Request $request)
    {


        Loan::where('id', $request->id)->delete();

        return redirect('loan.list')->with('message', $request->name . 'is deleted !');


    }

    function getVendorInfo(Request $request)
    {
        //get last record from this customer //
        $last = VenderPayable::where('vendor_id','=',$request->vendor_id)->orderBy('id','desc')->first();
        //end query//

        if($last) {

            //get customer last detail of customer by $last var//
            $response = DB::table('vender_payables')->join('vendors', 'vender_payables.vendor_id', '=', 'vendors.id')->where('vender_payables.id', '=', $last->id)
                ->get(['vender_payables.*', 'vendors.contact As vendor_contact']);
            //end query//


            return $response;
        }
    }

}








