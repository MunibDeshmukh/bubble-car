<?php

namespace App\Http\Controllers;

use App\AccountLedger;
use App\Http\Controllers\AccountController;
use App\Banks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BankController extends Controller
{
    function showCreatePage()
    {

        return view('bank_create');
    }

    function submitCreatePage(Request $request)
    {
        $this->validate($request,[
           'type'=> 'required'
        ]);

        if($request->type=='bank') {
            $this->validate($request,
                ['name' => 'required', 'address' => 'required', 'code' => 'required',
                    'account_no' => 'required', 'account_title' => 'required',
                    'contact_person' => 'required', 'email' => 'required',
                    'opening_balance' => 'required']
            );
        }

        if($request->type=='cash') {$this->validate($request, ['name' => 'required', 'opening_balance' => 'required']);}

        $table =new Banks();
        $table->name = $request->name;
        $table->address = $request->address;
        $table->code = $request->code;
        $table->account_no= $request->account_no;
        $table->account_title = $request->account_title;
        $table->contact_person = $request->contact_person;
        $table->email= $request->email;
        $table->opening_balance = $request->opening_balance;
        $table->account_type = $request->type;
        $table->wid=session('app_kb_login_workstation_id');
        $table->save();

        $account = new AccountController();
        $account->newAccount($request->opening_balance);





        return redirect('bank.create')->with('message', $request->name . 'is added !');

    }

    function showAllPage()
    {

        $table = new Banks();
        return view('bank_list')->with('table', $table->where('wid','=',session('app_kb_login_workstation_id'))->get());
    }


    function EditPage($id)
    {

        $table = Banks::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'bank_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                    'address' => $i->address,
                    'code' => $i->code,
                    'account_no' => $i->account_no,
                    'account_title' => $i->account_title,
                    'contact_person' => $i->contact_person,
                    'email' => $i->email,
                    'opening_balance' => $i->opening_balance,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'opening_balance' => 'required',
            ]);



        Banks::where('id', $request->id)->update(array(
            'name' => $request->name,
            'address' => $request->address,
            'code' => $request->code,
            'account_no' => $request->account_no,
            'account_title' => $request->account_title,
            'contact_person' => $request->contact_person,
            'email' => $request->email,
            'opening_balance' => $request->opening_balance,

                ));

        return redirect('bank.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Banks::where('id', $request->id)->delete();

        return redirect('bank.list')->with('message', $request->name . 'is deleted !');


    }

}








