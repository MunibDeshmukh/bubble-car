<?php

namespace App\Http\Controllers;

use App\Workstation;
use Illuminate\Http\Request;
use App\Vendor;
use Illuminate\Support\Facades\DB;

class WorkstationController extends Controller
{
    function showCreatePage()
    {

        return view('workstation_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'stock_no' => 'required',
            'sale_no' => 'required',
        ]);



        $table = new Workstation();
        $table->name = $request->name;
        $table->sale_voucher = $request->sale_no;
        $table->stock_voucher = $request->stock_no;
        $table->save();


        return redirect('workstation.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = new Workstation();
        return view('workstation_list')->with('table', $table->all());
    }


    function EditPage($id)
    {

        $table = Workstation::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'workstation_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {



        $this->validate($request, [
            'name' => 'required',
        ]);



        Workstation::where('id', $request->id)->update(array(
            'name'=> $request->name,

    ));

        return redirect('workstation.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Workstation::where('id', $request->id)->delete();

        return redirect('workstation.list')->with('message', $request->name . 'is deleted !');


    }


    function setting(Request $request)
    {


        Workstation::where('id', $request->id)->delete();

        return redirect('workstation.list')->with('message', $request->name . 'is deleted !');


    }





}








