<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Brand;
use App\Service;
use App\ServicePrice;
use App\ServiceRecipe;
use App\Measurment;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    function showCreatePage()
    {

        $table = DB::table('products')
            ->join('categories'   ,'products.category'     ,'=' ,'categories.id')
            ->join('brands'   ,'products.brand'     ,'=' ,'brands.id')
            ->get([
                'products.*',
                'categories.name as category_name',
                'brands.name as brand_name',
            ]);

        return view('service_create')->with('table', $table->all());;
    }

    function submitCreatePage(Request $request)
    {


        $result = DB::Insert("Insert Into `services`(`ServiceName`) VALUES (?)",
            [
                $request->serviceName
            ]);

        $sid = Service::orderBy('id','desc')->first();
        $serviceId = intval($sid['id']);

        $result = DB::Insert("Insert Into `service_prices`(`serviceId`, `small`, `medium`, `large`, `extraLarge`) VALUES (?,?,?,?,?)",
            [

                $serviceId,
                $request->smallPrice,
                $request->mediumPrice,
                $request->largePrice,
                $request->extralargePrice
            ]);
        $i = 0;
       foreach ($request->proId as $proid) {
           $result = DB::Insert("Insert Into `service_recipes`(`recipeType`,`serviceId`, `productId`, `productQuantity`) VALUES (?,?,?,?)",
               [
                   $request->recipeType,
                   $serviceId,
                   $proid,
                   $request->proQuantity[$i]
               ]);
           $i++;
       }
        $result = DB::Insert("Insert Into `measurments`(`ServiceId`, `measurmentQuantity`, `price`) VALUES (?,?,?)",
            [
                $serviceId,
                $request->measurmentQuantity,
                $request->price
            ]);



    }

    function showAll()
    {

        $table = DB::table('services')
            ->join('service_prices'   ,'services.id','=','service_prices.serviceId')
            ->get([
                'services.*',
                'service_prices.*'
            ]);


        return view('service_list')->with('table', $table->all());
    }




}
