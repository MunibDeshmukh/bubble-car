<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerPayable;
use App\GSTSale;
use App\Purchase;
use App\PurchaseLog;
use App\VenderPayable;
use App\Workstation;
use Illuminate\Http\Request;
use  App\Service;
use  App\Recipe;
use  App\Tax;
use  App\Vendor;
use Illuminate\Support\Facades\DB;

class GSTSaleController extends Controller
{

    function showCreatePage()
    {
        $count = DB::table('g_s_t_sales')->count();
        $voucher = null;
        if($count==0) {
            $x = Workstation::where('id', '=', session('app_kb_login_workstation_id'))->first();
            $arr  = explode(' ',$x['gst_sale_voucher_no']);
            $num  = $arr[1];
            $char = $arr[0];
            $voucher = $char."-".$num;
        }
        else {
            $x = GSTSale::orderby('id', 'desc')->first();
            $arr  = explode('-',$x['voucher_no']);
            $num  = intval($arr[1])+1;
            $char = $arr[0];
            $voucher = $char."-".$num;
        }

        return view('gst_sale_create',array(
            'customers' =>Customer::where('wid','=',session('app_kb_login_workstation_id'))->get(),
            'voucher'   =>$voucher
        ));


    }

    function submitCreatePageForPurchase(Request $request)
    {

        $result = DB::Insert("Insert Into `g_s_t_sales`(  `customer_id`, `item_quantity`, `amount`, `tax_sale_num`, `tax_additional_sale_num`, `payable`, `sale_date`,`voucher_no`,`wid`) VALUES (?,?,?,?,?,?,?,?,?)",
            [
                $request->customer,
                $request->qty,
                $request->amount,
                $request->tax1,
                $request->tax2,
                $request->total,
                $request->date,
                $request->voucher,
                session('app_kb_login_workstation_id')
               ]);

        $last = CustomerPayable::where('wid','=',session('app_kb_login_workstation_id'))
                ->where('customer_id','=',$request->customer)->orderBy('id','desc')->first();


        $a = intval($request->total)+intval($last['balance']);

        $result = DB::Insert("Insert Into `customer_payables`(  `customer_id`, `credit`, `debit`, `balance`,`transaction_date`,`transaction_type` ,`wid`) VALUES (?,?,?,?,?,?,?)",
            [
                $request->customer,
                $request->total,
                0,
                $a,
                date('Y-m-d'),
                'gst_sale',
                session('app_kb_login_workstation_id')
            ]);

        return $request->all();

    }


    function submitCreatePageForLog(Request $request)
    {

//        if($request->i==0) {
            $purchase_id = null;
            $table = GSTSale::orderby('id','desc')->first();

            if ($table->id) {


                $result = DB::Insert("Insert Into `g_s_t_sale_logs`(  `product_id`, `purchase_id`, `item_quantity`, `price`, `amount`, `tax_sale_num`, `tax_additional_sale_num`, `payable` ) VALUES (?,?,?,?,?,?,?,?)",
                    [
                        $request->id,
                        $table->id,
                        $request->qty,
                        $request->price,
                        $request->amount,
                        $request->tax1,
                        $request->tax2,
                        $request->total
                    ]);

                return 1;

            }

            return 0;
//        }



    }


    function showAll()
    {

        $table = DB::table('g_s_t_sales')
            ->join('customers'   ,'g_s_t_sales.customer_id'     ,'=' ,'customers.id')
            ->where('g_s_t_sales.wid','=',session('app_kb_login_workstation_id'))
//           ->where('g_s_t_sales.canceled','=',0)
            ->get([
                'g_s_t_sales.*',
                'customers.company_name as company_name',
                'customers.person_name as person_name',
            ]);


        return view('gst_sale_list')->with('table', $table->all());
    }


    function EditPage($id)
    {

        $table = Service::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'service_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                    'category' => $i->category,
                    'price' => $i->price
                ));
            }
        } else {
            return $table;
        }
    }



    function fetchItem()
    {

        $table = DB::table('items')
            ->join('units','items.unit','=','units.id')
            ->get(['items.*','units.name As unit_name']);


       return $table;

    }



    function  showSingleRecipe($id)
    {
        $table = DB::table('g_s_t_sale_logs')
            ->join('products','g_s_t_sale_logs.product_id','=','products.id')
            ->join('g_s_t_sales','g_s_t_sale_logs.purchase_id','=','g_s_t_sales.id')
            ->join('customers','g_s_t_sales.customer_id','=','customers.id')
            ->where('g_s_t_sale_logs.purchase_id','=',$id)
            ->get(['g_s_t_sale_logs.*','products.name As product_name','customers.*','g_s_t_sales.voucher_no','g_s_t_sales.payable As total_amount','g_s_t_sales.tax_sale_num As tax1','g_s_t_sales.tax_additional_sale_num As tax2']);


        return view( 'gst_sale_print_view',array(
            'table'=>$table,
            'sale'=>GSTSale::where('id','=',$id)->first(),
            'company_name'=>$table[0]->company_name,
            'person_name'=>$table[0]->person_name,
            'voucher_no'=>$table[0]->voucher_no,
            'total_amount'=>$table[0]->total_amount,
            'srb'=>$table[0]->srb,
            'tax1'=>$table[0]->tax1,
            'tax2'=>$table[0]->tax2,

        ));

    }

        function createdMessage(){
            return redirect('gst.sale.create')->with('message',  'Sale saved !');
        }




}