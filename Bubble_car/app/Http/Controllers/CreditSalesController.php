<?php

namespace App\Http\Controllers;

use App\GSTSale;
use App\Purchase;
use App\purchase_log;
use App\VendorPayable;
use App\Workstation;
use App\Grn;
use App\Inventory;
use  App\Service;
use  App\ServiceRecipe;
use  App\Recipe;
use  App\Tax;
use  App\CarSelection;
use App\Customer;
use App\CreditSale;
use App\Creditsale_product_Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CreditSalesController extends Controller
{
    function showCreatePage()
    {
        $count = DB::table('credit_sales')->count();
        $voucher = null;
        if($count==0) {
            $x = Workstation::where('id', '=', session('login_workstation_id'))->first();
            // dd($x['stock_voucher']);
            $arr  = explode('-',$x['sale_voucher']);
            $num  = $arr[1];
            $char = $arr[0];
            $voucher = $char."-".$num;
        }
        else {
            $x = CreditSale::orderby('id', 'desc')->first();
            $arr  = explode('-',$x['voucherNo']);
            $num  = intval($arr[1])+1;
            $char = $arr[0];
            $voucher = $char."-".$num;
        }

        return view('credit_sale_create',array(
            'customer' =>Customer::where('wid','=',session('login_workstation_id'))->get(),
            'tax'=>Tax::all(),
            'voucher'=>$voucher,
            'car'=>CarSelection::select('type')->distinct()->get()
            
            
        ));


    }
    
        function submitCreatePageForsaleProduct(Request $request)
    {
            
     
                
                
        $result = DB::Insert("Insert Into `credit_sales`(`customerId`, `voucherNo`, `carType`, `carName` , `item_quantity`, `total_amount`, `taxType`, `taxAction`, `taxDetectAs`, `taxValue`, `payable`, `date`, `wid`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
            [
                $request->vendor,
                $request->voucher,
                $request->carType,
                $request->carName,
                $request->qty,
                $request->amount,
                $request->tax_type,
                $request->tax_action,
                $request->tax_as,
                $request->tax,
                $request->total,
                
                $request->date,
                // $request->voucher,
                session('login_workstation_id')
               ]);

    
         
    }
    

    
    function submitCreatePageForsaleproductLog(Request $request)
    {
         
        
        
        
        
        
//        if($request->i==0) {
            $sale_id = null;
            $table = CreditSale::orderby('id','desc')->first();

            if ($table->id) {


                $result = DB::Insert("Insert Into `creditsale_product_logs`( `product_id`, `sale_id`, `item_quantity`, `price`, `amount`, `tax`, `receivable`) VALUES (?,?,?,?,?,?,?)",
                    [
                        $request->id,
                        $table->id,
                        $request->qty,
                        $request->price,
                        $request->amount,
                       
                        $request->tax,
                        $request->total
                    ]);

               
                
                
          $get_quantity = purchase_log::where('product_id',$request->id)->first();
        
        $updated_quantity = intval($get_quantity['item_quantity']) - intval($request->qty);      
                
        DB::table('purchase_logs')
            ->where('product_id',$request->id)
            ->update(['item_quantity' => $updated_quantity]);
 return 1;
            }

            return 0;
//        }

        
      

    }
    
    
            function submitCreatePageForsaleServicesLog(Request $request)
    {

//        if($request->i==0) {
            $sale_id = null;
            $table = CreditSale::orderby('id','desc')->first();

            if ($table->id) {


                $result = DB::Insert("Insert Into `creditsale_service_logs`( `services_id`, `sale_id`, `item_quantity`, `price`, `amount`, `tax`, `receivable`) VALUES (?,?,?,?,?,?,?)",
                    [
                        $request->id,
                        $table->id,
                        $request->qty,
                        $request->price,
                        $request->amount,
                       
                        $request->tax,
                        $request->total
                    ]);
             
                
                 $gq = ServiceRecipe::where('serviceId',$request->id)->first();
        
        $uq = intval($gq['productQuantity']) - intval($request->qty);      
                
        DB::table('service_recipes')
            ->where('serviceId',$request->id)
            ->update(['productQuantity' => $uq]);    
                
                
                
             
                
                return 1;

            }

            return 0;
//        }



    }
    
                function submitCreatePageForsaleoutsourceLog(Request $request)
    {

//        if($request->i==0) {
            $sale_id = null;
            $table = CreditSale::orderby('id','desc')->first();

            if ($table->id) {


                $result = DB::Insert("Insert Into `creditsale_outsource_service_logs`( `Outservices_id`, `sale_id`, `item_quantity`, `price`, `amount`, `tax`, `receivable`) VALUES (?,?,?,?,?,?,?)",
                    [
                        $request->id,
                        $table->id,
                        $request->qty,
                        $request->price,
                        $request->amount,
                       
                        $request->tax,
                        $request->total
                    ]);

                return 1;

            }

            return 0;
//        }



    }
    
    
    function getcarname(Request $request){
        
         
     $table = CarSelection::where('type', '=',$request->val)->get();
        
        foreach($table as $tbl){
            
    echo  "<option value='{$tbl->name}'>{$tbl->name}</option>";
        }
        
    }
    
    
       function serviceQueryByName(Request $request){

  
    $table = DB::table('services')
            ->join('service_prices','services.id','=','service_prices.serviceId')
            ->join('service_recipes','service_recipes.serviceId','=','services.id')
            ->where('services.ServiceName','LIKE',$request->q.'%')
            ->get(['services.*','service_prices.*']);

return $table; 
          }
    
    
            function outsourceQueryByName(Request $request){

        
  return DB::select("select `id` , `serviceName` from outsourceservices WHERE  `serviceName` LIKE ? ORDER  BY `serviceName` ASC ",[$request->q.'%']);
 
  
          }
    
         function outsourceQueryById(Request $request){

        
  return DB::select("select `id` , `serviceName` from outsourceservices WHERE  `id` LIKE ? ORDER  BY `serviceName` ASC ",[$request->q.'%']);
 
  
          }
    
       function serviceQueryById(Request $request){

        
       // return DB::select("select `id` , `ServiceName` from services WHERE  `id` LIKE ? ORDER  BY `ServiceName` ASC ",[$request->q.'%']);

 $table = DB::table('services')
            ->join('service_prices','services.id','=','service_prices.serviceId')
            ->join('service_recipes','service_recipes.serviceId','=','services.id')
            ->where('services.id','LIKE',$request->q.'%')
            ->get(['services.*','service_prices.*']);

return $table;

    }
    
    
    
    
    
        function viewcreditsales()
         {
        
 $table = DB::table('creditsale_product_logs')
            ->join('products','creditsale_product_logs.product_id','=','products.id')
           ->get(['creditsale_product_logs.*','products.name as productName']);
        
$table1 = DB::table('creditsale_service_logs')
            ->join('services','creditsale_service_logs.services_id','=','services.id')
           ->get(['creditsale_service_logs.*','services.ServiceName as serviceName']);
        
$table2 = DB::table('creditsale_outsource_service_logs')
            ->join('outsourceservices','creditsale_outsource_service_logs.Outservices_id','=','outsourceservices.id')
           ->get(['creditsale_outsource_service_logs.*','outsourceservices.serviceName as serviceName']);
        

        return view('creditsales_list')->with(['table'=>$table,'table1'=>$table1,'table2'=>$table2]);
    }
    
    
    
    
    
    
    
}
