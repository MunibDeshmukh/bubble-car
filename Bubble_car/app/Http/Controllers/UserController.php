<?php

namespace App\Http\Controllers;

use App\Workstation;
use App\Admin;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    function showCreatePage()
    {

        return view('user_create')->with('table',Workstation::all());
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name'=>'required|min:3',
            'workstation'=>'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);



        $table = new Admin();
        $table->name = $request->name;
        $table->workstation_id = $request->workstation;
        $table->password = $request->password;
        $table->save();


        return redirect('user.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {
        $table = DB::table('admins')
            ->join('workstations','admins.workstation_id','=','workstations.id')
            ->get(['admins.*','workstations.name As workstation_name']);


        return view('user_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = Admin::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'user_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                    'password' => $i->password,
                    'workstation_id' => $i->workstation_id,
                    'table'=>Workstation::all()
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|min:3',
            'workstation_id'=>'required',
            'password' => 'required|min:6',
        ]);



        Admin::where('id', $request->id)->update(array(
            'name'=> $request->name,
            'name' => $request->name,
            'password' => $request->password,
            'workstation_id' => $request->workstation_id,

    ));

        return redirect('user.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Workstation::where('id', $request->id)->delete();

        return redirect('user.list')->with('message', $request->name . 'is deleted !');


    }

}








