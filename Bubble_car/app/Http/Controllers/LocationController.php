<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    function showCreatePage()
    {

        return view('location_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);


        $table = new Location();
        $table->name = $request->name;
        $table->wid =session('app_kb_login_workstation_id');
        $table->save();


        return redirect('location.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = new Location();
        return view('location_list')->with('table', $table->where('wid','=',session('app_kb_login_workstation_id'))->get());
    }


    function EditPage($id)
    {

        $table = Location::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'location_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);



        Location::where('id', $request->id)->update(array(
            'name'=> $request->name
                ));

        return redirect('location.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Location::where('id', $request->id)->delete();

        return redirect('location.list')->with('message', $request->name . 'is deleted !');


    }

}








