<?php

namespace App\Http\Controllers;

use App\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WarehouseController extends Controller
{
    function showCreatePage()
    {

        return view('warehouse_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);


        $table = new Warehouse();
        $table->name = $request->name;
        $table->wid =session('app_kb_login_workstation_id');
        $table->save();


        return redirect('warehouse.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = Warehouse::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('warehouse_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = Warehouse::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'warehouse_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);



        Warehouse::where('id', $request->id)->update(array(
            'name'=> $request->name
                ));

        return redirect('warehouse.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Warehouse::where('id', $request->id)->delete();

        return redirect('warehouse.list')->with('message', $request->name . 'is deleted !');


    }

}








