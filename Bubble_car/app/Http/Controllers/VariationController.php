<?php

namespace App\Http\Controllers;
use Session;
use App\Variation;
use Illuminate\Http\Request;
use App\Vendor;
use Illuminate\Support\Facades\DB;

class VariationController extends Controller
{
    function showCreatePage()
    {

        return view('variation_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            // 'stock_no' => 'required',
            // 'sale_no' => 'required',
        ]);



        $table = new Variation();
        $table->name = $request->name;
        $table->wid = Session::get('login_user_id');
        // $table->sale_voucher = $request->sale_no;
        // $table->stock_voucher = $request->stock_no;
        $table->save();


        return redirect('variation.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {
    		// dd(Session::get('login_workstation_id'));
        $table = new Variation();
        return view('variation_list')->with('table', $table->where('wid','=',Session::get('login_workstation_id'))->get());
    }


    function EditPage($id)
    {

        $table = Variation::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'variation_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {



        $this->Validate($request, [
            'name' => 'required',
        ]);



        Variation::where('id', $request->id)->update(array(
            'name'=> $request->name,

    ));

        return redirect('variation.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Variation::where('id', $request->id)->delete();

        return redirect('variation.list')->with('message', $request->name . 'is deleted !');


    }


    function setting(Request $request)
    {


        Variation::where('id', $request->id)->delete();

        return redirect('variation.list')->with('message', $request->name . 'is deleted !');


    }





}








