<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Variation;
use App\CarSelection;
use App\Category;
use App\ProductCars;
use App\Product;
use App\Units;
// use App\ProductClass;
// use App\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    function showCreatePage()
    {

        $brand = Brand::where('wid','=',session('login_workstation_id'))->get();
        $variation = Variation::where('wid','=',session('login_workstation_id'))->get();
        $cars = CarSelection::where('wid','=',session('login_workstation_id'))->get();
        $category = Category::where('wid','=',session('login_workstation_id'))->get();
        $unit=Units::all();
        // $location = Location::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('product_create', array(
            'brand'=>$brand,
            'variation'=>$variation,
            'cars'=>$cars,
            // 'type'=>$type,
            'category'=>$category,
            'unit'=>$unit
        ));
    }

    function submitCreatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'brand' => 'required',
            'variation' => 'required',
            'type' => 'required',
            'car' => 'required',
            'unit' => 'required',
            'category' => 'required',
            'product_code' => 'required',
            'outside_code' => 'required',

            
        ]);
         $w="";
        foreach ($request->car as $value) {
            // $b=explode(",", $value);
            //dd($value);
            //dd($b);
            $w .=$value.",";
        }
       
       $a=substr($w, 0,-1);

        $table = new Product();
        $table->name = $request->name;
        $table->description = $request->desc;
        $table->pro_code = $request->product_code;
        $table->brand = $request->brand;
        $table->outside_code = $request->outside_code;
        $table->type = $request->type;
        $table->category = $request->category;
        $table->variation = $request->variation;
        $table->cars = $a;
        $table->unit = $request->unit;
        
       
        $table->wid=session('login_workstation_id');
        $table->save();

        $last=Product::orderby('id','desc')->first();
        foreach ($request->car as $key) {
            $new=new ProductCars();
            $new->pro_id=$last['id'];
            $new->name=$key;
            $new->wid=session('login_workstation_id');
            $new->save();
        }


        return redirect('product.create')->with('message', $request->name . ' is added !');


    }

    function showAllPage()
    {
        $table = Product::where('wid','=',session('login_workstation_id'))->get();

        

        return view('product_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = DB::table('products')
            ->join('product_types','products.type','=','product_types.id')
            ->join('product_classes','products.class','=','product_classes.id')
            ->join('product_brands','products.brand','=','product_brands.id')
            ->join('product_categories','products.category','=','product_categories.id')
            ->where('products.id','=',$id)
            ->first([
                'products.*',
                'product_classes.id As class_id',
                'product_categories.id As category_id',
            ]);

        $category = ProductCategory::all();
        $type = ProductType::all();
        $class = ProductClass::all();
        $warehouse = Warehouse::all();
        $location = Location::all();

        if ($table) {
                return view('product_edit' , array(
                    'id' => $table->id,
                    'name' => $table->name,
                    'desc' => $table->desc,
                    'class_id' => $table->class,
                    'model_id' => $table->model,
                    'brand_id' => $table->brand,
                    'retail_price' => $table->retail_price,
                    'f_o_b' => $table->f_o_b,
                    'sailing_price' => $table->sailing_price,
                    'level' => $table->level,
                    'category_id' => $table->category,
                    'location_id' => $table->location,
                    'warehouse_id' => $table->warehouse,
                    'type_id' => $table->type,
                    'category' => $category,
                    'class' => $class,
                    'type' => $type,
                    'location' => $location,
                    'warehouse' => $warehouse,
                ));
         } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'class' => 'required',
            'brand' => 'required',
            'model' => 'required',
            'type' => 'required',
            'category' => 'required',
            'location' => 'required',
            'warehouse' => 'required',
            'retail_price' => 'required',
            'f_o_b' => 'required',
            'sailing_price' => 'required',
            'level' => 'required'
            ]);


        Product::where('id', $request->id)->update(array(
            'name' => $request->name,
        'desc' => $request->desc,
        'class' => $request->class,
        'brand' => $request->brand,
        'model' => $request->model,
        'type' => $request->type,
        'category' => $request->category,
        'location' => $request->location,
        'warehouse' => $request->warehouse,
        'retail_price' => $request->retail_price,
        'f_o_b' => $request->f_o_b,
        'sailing_price' => $request->sailing_price,
        'level' => $request->level,
                ));

        return redirect('product.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {
        Product::where('id', $request->id)->delete();
        return redirect('product.list')->with('message', $request->name . 'is deleted !');
    }

    function getCars(){


        $html=array();
       $table= CarSelection::where('type','=',$_GET['type'])->get();

            foreach ($table as $k) {
                array_push($html, '
                    <option value="'.$k->name.'">'.$k->name.'</option>
                    ');
            }
        return $html;



        // return $request->all();

    }


    function productQueryByName(Request $request){

        $wid = session('login_workstation_id');
 return DB::select("select `id` , `name` from products WHERE `wid` = ? AND `name` LIKE ? ORDER  BY `name` ASC ",[$wid,$request->q.'%']);
 

    }


    function productQueryById(Request $request){

        $wid = session('login_workstation_id');
        return DB::select("select `id` , `name`, `unit` from products WHERE `wid` = ? AND `id` = ? ",
                          [$wid,$request->q]);



    }






}








