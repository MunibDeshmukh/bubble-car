<?php

namespace App\Http\Controllers;

use App\ProductBrand;
use App\ProductClass;
use App\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductModelController extends Controller
{
    function showCreatePage()
    {

        $class = ProductClass::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('product_model_create')->with('class',$class);
    }

    function submitCreatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'class' => 'required',
            'brand' => 'required'
        ]);

        $table = new ProductModel();
        $table->name = $request->name;
        $table->class_id = $request->class;
        $table->brand_id = $request->brand;
        $table->wid =session('app_kb_login_workstation_id');
        $table->save();


        return redirect('product.model.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {
        $table = DB::table('product_models')
            ->join('product_classes','product_models.class_id','=','product_classes.id')
            ->join('product_brands','product_models.brand_id','=','product_brands.id')
            ->where('product_models.wid','=',session('app_kb_login_workstation_id'))
            ->get(['product_models.*','product_classes.name As class_name','product_brands.name As brand_name']);

        return view('product_model_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = DB::table('product_models')
            ->join('product_classes','product_models.class_id','=','product_classes.id')
            ->join('product_brands','product_models.brand_id','=','product_brands.id')
            ->where('product_models.id','=',$id)
            ->first(['product_models.*','product_classes.id As class_id','product_brands.name As brand_name','product_brands.id As brand_id']);



        if ($table) {
                return view('product_model_edit' , array(
                    'id' => $table->id,
                    'name' => $table->name,
                    'class_id' => $table->class_id,
                    'brand_name' => $table->brand_name,
                    'brand_id' => $table->brand_id,
                    'class'=>ProductClass::where('wid','=',session('app_kb_login_workstation_id'))->get()
                ));
         } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);



        ProductModel::where('id', $request->id)->update(array(
            'name'=> $request->name,
            'class_id'=> $request->class,
            'brand_id'=> $request->brand
                ));

        return redirect('product.model.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        ProductModel::where('id', $request->id)->delete();

        return redirect('product.model.list')->with('message', $request->name . 'is deleted !');


    }

    function getBrands(Request $request){

        return ProductBrand::where('class_id','=',$request->id)->get();

    }

}








