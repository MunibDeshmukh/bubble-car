<?php

namespace App\Http\Controllers;

use App\GSTSale;
use App\Purchase;
use App\LocalPurchase;
use App\LocalPurchaseLogs;
use App\VenderPayable;
use App\Workstation;
use Illuminate\Http\Request;
use  App\Vendor;
use Illuminate\Support\Facades\DB;

class LocalPurchaseController extends Controller
{

    function showCreatePage()
    {
        $count = DB::table('local_purchases')->count();
        $voucher = null;
        if($count==0) {
            $x = Workstation::where('id', '=', session('app_kb_login_workstation_id'))->first();
            $arr  = explode(' ',$x['local_purchases_voucher_no']);
            $num  = $arr[1];
            $char = $arr[0];
            $voucher = $char."-".$num;
        }
        else {
            $x = LocalPurchase::orderby('id', 'desc')->first();
            $arr  = explode('-',$x['voucher_no']);
            $num  = intval($arr[1])+1;
            $char = $arr[0];
            $voucher = $char."-".$num;
        }

        return view('local_purchase_create',array(
            'vendors' =>Vendor::where('wid','=',session('app_kb_login_workstation_id'))->get(),
            'voucher'   =>$voucher
        ));


    }

    function submitCreatePageForPurchase(Request $request)
    {

        $result = DB::Insert("Insert Into `local_purchases`(  `vendor_id`, `item_quantity`, `container_no`, `gd_no`, `gd_date`, `bl_no`, `bl_date`, `amount`, `tax_gst_in_percent`, `payable`, `purchasing_date`,`voucher_no`,`wid`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
            [
                $request->vendor,
                $request->qty,
                $request->container,
                $request->gd_no,
                $request->gd_date,
                $request->bl_no,
                $request->bl_date,
                $request->amount,
                $request->gst,
                $request->total,
                $request->date,
                $request->voucher,
                session('app_kb_login_workstation_id')
               ]);

        $last = VenderPayable::where('wid','=',session('app_kb_login_workstation_id'))
                ->where('vendor_id','=',$request->vendor)->orderBy('id','desc')->first();


        $a = intval($request->total)+intval($last['balance']);

        $result = DB::Insert("Insert Into `vender_payables`(  `vendor_id`, `purchase`, `payment`, `balance`,`purchase_date`,`transaction_type` ,`wid`) VALUES (?,?,?,?,?,?,?)",
            [
                $request->vendor,
                $request->total,
                0,
                $a,
                date('Y-m-d'),
                'purchase',
                session('app_kb_login_workstation_id')
            ]);

        return $request->all();

    }


    function submitCreatePageForLog(Request $request)
    {

//        if($request->i==0) {
            $purchase_id = null;
            $table = LocalPurchase::orderby('id','desc')->first();

            if ($table->id) {


                $result = DB::Insert("Insert Into `local_purchase_logs`(  `product_id`, `purchase_id`, `item_quantity`, `price`, `amount`,`tax_gst_in_percent`, `payable` ) VALUES (?,?,?,?,?,?,?)",
                    [
                        $request->id,
                        $table->id,
                        $request->qty,
                        $request->price,
                        $request->amount,
                        $request->gst,
                        $request->total
                    ]);

                return 1;

            }

            return 0;
//        }



    }


    function showAll()
    {

        $table = DB::table('local_purchases')
            ->join('vendors'   ,'local_purchases.vendor_id'     ,'=' ,'vendors.id')
            ->where('local_purchases.wid','=',session('app_kb_login_workstation_id'))
//           ->where('g_s_t_sales.canceled','=',0)
            ->get([
                'local_purchases.*',
                'vendors.company_name as company_name',
                'vendors.person_name as person_name',
            ]);


        return view('local_purchase_list')->with('table', $table->all());
    }


    function EditPage($id)
    {

        $table = Service::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'service_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                    'category' => $i->category,
                    'price' => $i->price
                ));
            }
        } else {
            return $table;
        }
    }



    function fetchItem()
    {

        $table = DB::table('items')
            ->join('units','items.unit','=','units.id')
            ->get(['items.*','units.name As unit_name']);


       return $table;

    }



    function  showSingleRecipe($id)
    {

        $table = DB::table('local_purchase_logs')
            ->join('products','local_purchase_logs.product_id','=','products.id')
            ->join('local_purchases','local_purchase_logs.purchase_id','=','local_purchases.id')
            ->join('vendors','local_purchases.vendor_id','=','vendors.id')
            ->where('local_purchase_logs.purchase_id','=',$id)
            ->get(['local_purchase_logs.*','products.name As product_name','vendors.*','local_purchases.voucher_no','local_purchases.payable As total_amount','local_purchases.tax_gst_in_percent As gst']);



        return view( 'local_purchase_print_view',array(
            'table'=>$table,
            'company_name'=>$table[0]->company_name,
            'person_name'=>$table[0]->person_name,
            'voucher_no'=>$table[0]->voucher_no,
            'total_amount'=>$table[0]->total_amount,
            'srb'=>$table[0]->srb,
            'gst'    =>$table[0]->gst

        ));

    }

        function createdMessage(){
            return redirect('local.purchase.create')->with('message',  'Purchase saved !');
        }




}