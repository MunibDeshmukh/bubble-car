<?php

namespace App\Http\Controllers;

use App\Customer;
use App\RentalSale;
use App\RentalSaleReading;
use App\Workstation;
use App\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RentalSaleController extends Controller
{


    function showCreatePage()
    {
        return view('rental_sale_create',array(
            'customers' =>Customer::where('wid','=',session('app_kb_login_workstation_id'))->get(),
        ));
    }

    function submitCreatePageForPurchase(Request $request)
    {
        $voucher=null;
        DB::Insert("Insert Into `rental_sales`(  `customer_id`,`date`,`rental_type` ,`wid`) VALUES (?,?,?,?)",[$request->customer,$request->date,$request->type,session('app_kb_login_workstation_id')]);
        $sale = RentalSale::where('wid','=',session('app_kb_login_workstation_id'))->orderBy('id','desc')->first();
        $count = RentalSaleReading::where('wid','=',session('app_kb_login_workstation_id'))->count();
        if($count==0) {
            $x = Workstation::where('id', '=', session('app_kb_login_workstation_id'))->first();
            $arr  = explode(' ',$x['rental_sale_vousher_no']);
            $num  = $arr[1];
            $char = $arr[0];
            $voucher = $char."-".$num;
        }
        else {
            $x = RentalSaleReading::orderby('id', 'desc')->first();
            $arr  = explode('-',$x['voucher_no']);
            $num  = intval($arr[1])+1;
            $char = $arr[0];
            $voucher = $char."-".$num;
        }

        DB::Insert("Insert Into `rental_sale_readings`( `sale_id`,`date`, `voucher_no`,`wid`) VALUES (?,?,?,?)", [$sale['id'], $request->date, $voucher, session('app_kb_login_workstation_id')]);
        return 1;
    }


    function submitCreatePageForLog(Request $request)
    {
        $reading = RentalSaleReading::where('wid','=',session('app_kb_login_workstation_id'))->orderBy('id','desc')->first();
        DB::Insert("Insert Into `rental_sale_logs`( `sale_id`, `product_id`, `wid`) VALUES (?,?,?)", [$reading['sale_id'], $request->id, session('app_kb_login_workstation_id')]);
        if($request->type=='reading') {
            DB::Insert("Insert Into `rental_sale_reading_product_logs`( `reading_id`, `product_id`,`current_reading`, `min_copy`) VALUES (?,?,?,?)", [$reading['id'], $request->id,$request->reading,$request->copy,             ]);
        }
        if($request->type=='quantity') {
            DB::Insert("Insert Into `rental_sale_reading_product_logs`( `reading_id`, `product_id`) VALUES (?,?)", [$reading['id'], $request->id,]);
        }


            return 0;





    }

   function showPageAll(){

       $table = DB::table('rental_sales')
           ->join('customers'   ,'rental_sales.customer_id'     ,'=' ,'customers.id')
           ->get([
               'rental_sales.*',
               'customers.company_name as company_name',
               'customers.person_name as person_name',
           ]);

           return view('rental_sale_list')->with('table',$table);

   }

    function showAllPage()
    {

        $table = new Service();
        return view('service_list')->with('table', $table->all());
    }


    function EditPage($id)
    {

        $table = Service::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'service_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                    'category' => $i->category,
                    'price' => $i->price
                ));
            }
        } else {
            return $table;
        }
    }



    function fetchItem()
    {

        $table = DB::table('items')
            ->join('units','items.unit','=','units.id')
            ->get(['items.*','units.name As unit_name']);


       return $table;

    }


    function  showSinglePurchaseLog($id)
    {
        $table = DB::table('rental_sale_logs')
            ->join('products','rental_sale_logs.product_id','=','products.id')
            ->where('sale_id','=',$id)
            ->get(['rental_sale_logs.*','products.name As product_name']);

        return view( 'rental_sale_log_view',array(
            'table'=>$table,
        ));

    }


    function createdMessage()
    {
        return redirect('rental.sale.create')->with('message', 'Rental sale is added!');
    }

}