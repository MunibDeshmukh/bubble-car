<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    function showCreatePage()
    {

        return view('customer_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'company_name' => 'required',
            'contact_person_name' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'opening_balance' => 'required',
            'ntn' => 'required',
            'srb' => 'required',
            'business_nature'=>'required',
            'customer_type'=>'required'
        ]);

        if($request->customer_type=='rental')
        {
            $this->validate($request, [
                'rental_type'=>'required'
            ]);
            $rental = $request->rental_type;
        }else{
            $rental='';
        }


        $table = new Customer();
        $table->company_name = $request->company_name;
        $table->person_name = $request->contact_person_name;
        $table->contact = $request->contact;
        $table->email = $request->email;
        $table->address = $request->address;
        $table->opening_balance = $request->opening_balance;
        $table->ntn = $request->ntn;
        $table->srb = $request->srb;
        $table->business_nature= $request->business_nature;
        $table->customer_type= $request->customer_type;
        $table->rental_type= $rental;
        $table->wid=session('app_kb_login_workstation_id');
        $table->save();


        $last = null;
        $table = DB::select("select * from `customers` ORDER  BY `id` DESC  limit 1");
        foreach ($table as $i) {
            $last = $i->id;
        }
        $balance=0-intval($request->opening_balance);
        $result = DB::Insert("Insert Into `customer_payables`(  `customer_id`,`credit`, `debit`, `balance`,`transaction_date`,`transaction_type` ,`wid`) VALUES (?,?,?,?,?,?,?)",
            [
                $last,
                0,
                $request->opening_balance,
                $balance,
                date('Y-m-d'),
                '101',
                session('app_kb_login_workstation_id')
            ]);


//        $account = new AccountController();
//        $account->newReceivable($request->account_id,$request->amount_paid,$last['id'],'316','receive from customer');


        return redirect('customer.create')->with('message', $request->contact_person_name . 'is added !');


    }

    function showAllPage()
    {

        $table = new Customer();
        return view('customer_list')->with('table', $table->where('wid','=',session('app_kb_login_workstation_id'))->get());
    }


    function EditPage($id)
    {

        $table = Customer::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'customer_edit',array(
                    'id' => $i->id,
                    'customer_type' => $i->customer_type,
                    'rental_type' => $i->rental_type,
                    'company_name' => $i->company_name,
                    'person_name' => $i->person_name,
                    'contact' => $i->contact,
                    'email' => $i->email,
                    'address' => $i->address,
                    'opening_balance' => $i->opening_balance,
                    'ntn' => $i->ntn,
                    'srb' => $i->srb,
                    'business_nature' => $i->business_nature,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {

        $this->validate($request, [
            'customer_type' => 'required',
            'rental_type' => 'required',
            'company_name' => 'required',
            'contact_person_name' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'opening_balance' => 'required',
            'ntn' => 'required',
            'srb' => 'required',
            'business_nature'=>'required'
        ]);

        if($request->customer_type=='rental')
        {
            $this->validate($request, [
                'rental_type'=>'required'
            ]);
            $rental = $request->rental_type;
        }else{
            $rental='';
        }

        Customer::where('id', $request->id)->update(array(

            'customer_type' => $request->customer_type,
            'rental_type' => $rental,
            'company_name'=> $request->company_name,
            'person_name' => $request->contact_person_name,
            'contact'     => $request->contact,
            'email'       => $request->email,
            'address'     => $request->address,
            'opening_balance' => $request->opening_balance,
            'ntn'         =>  $request->ntn,
            'srb'         =>  $request->srb,
            'business_nature' => $request->business_nature,

    ));

        return redirect('customer.list')->with('message', $request->contact_person_name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Customer::where('id', $request->id)->delete();

        return redirect('customer.list')->with('message', $request->contact_person_name . 'is deleted !');


    }

}








