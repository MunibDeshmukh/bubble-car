<?php

namespace App\Http\Controllers;

use App\ProductClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductClassController extends Controller
{
    function showCreatePage()
    {

        return view('product_class_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);


        $table = new ProductClass();
        $table->name = $request->name;
        $table->wid =session('app_kb_login_workstation_id');
        $table->save();


        return redirect('product.class.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = ProductClass::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('product_class_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = ProductClass::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'product_class_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);



        ProductClass::where('id', $request->id)->update(array(
            'name'=> $request->name
                ));

        return redirect('product.class.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        ProductClass::where('id', $request->id)->delete();

        return redirect('product.class.list')->with('message', $request->name . 'is deleted !');


    }

}








