<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Banks;
use App\VenderPayable;
use App\GSTSale;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VednorPayableController extends Controller
{
    function showCreatePage()
    {

        return view('vendor_payable_create',array(
            'customer' =>Vendor::where('wid','=',session('app_kb_login_workstation_id'))->get(),
            'bank' =>Banks::where('wid','=',session('app_kb_login_workstation_id'))->get(),
        ));
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'vendor_id' => 'required',
            'amount_paid' => 'required',
            'account_id'=>'required'

        ]);




        $table = DB::select("select * from `vender_payables` where `vendor_id`=? ORDER  BY `id` DESC  limit 1",[$request->vendor_id]);
        foreach ($table as $i) {
            $last_payable = $i->balance;
        }

        $a = intval($last_payable) - intval($request->amount_paid);

        $result = DB::Insert("Insert Into `vender_payables`(  `vendor_id`, `purchase`, `payment`, `balance`,`purchase_date`,`transaction_type`,`wid` ) VALUES (?,?,?,?,?,?,?)",
            [
                $request->vendor_id,
                0,
                $request->amount_paid,
                $a,
                date('Y-m-d'),
                'payment',
                session('app_kb_login_workstation_id')
            ]);
         $account = new AccountController();
        $account->newPayable($request->account_id,$request->amount_paid,$request->vendor_id,210,'vendor_payable');

        return redirect('vendor.payable.create')->with('message', $request->name . 'is added !');


    }


    function showAllPage()
    {

        $table = DB::table('vendor_payables')
            ->join('vendors','vendor_payables.vendor_id','=','vendors.id')
            ->where('vendor_payables.wid','=',session('login_workstation_id'))
                ->orderby('id','desc')
            ->get([
                'vendor_payables.*',
                'vendors.contact As vendor_contact',
                'vendors.company_name As company_name',
                ]);


        return view('vendor_payable_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = DB::table('loans')
            ->join('staff','loans.staff_id','=','staff.id')
            ->where('loans.id','=',$id)
            ->first(['loans.*','staff.id As staff_id']);

        if ($table) {

                return view('loan_edit', array(
                    'id' => $table->id,
                    'staff_id' => $table->staff_id,
                    'amount' => $table->amount,
                    'installment' => $table->installment,
                    'staff' => Staff::all()
                ));
        } else {
            return $table;
       }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'staff' => 'required',
            'amount' => 'required',
            'installment' => 'required'
            ]);



        Loan::where('id', $request->id)->update(array(
            'staff_id' => $request->staff,
            'amount' => $request->amount,
            'installment' => $request->installment,
                ));

        return redirect('loan.list')->with('message', $request->staff_id . 'is updated !');


    }

    function destroy(Request $request)
    {


        Loan::where('id', $request->id)->delete();

        return redirect('loan.list')->with('message', $request->name . 'is deleted !');


    }

    function getVendorInfo(Request $request)
    {
        //get last record from this customer //
        $last = VenderPayable::where('vendor_id','=',$request->vendor_id)->orderBy('id','desc')->first();
        //end query//

        if($last) {

            //get customer last detail of customer by $last var//
            $response = DB::table('vender_payables')->join('vendors', 'vender_payables.vendor_id', '=', 'vendors.id')->where('vender_payables.id', '=', $last->id)
                ->get(['vender_payables.*', 'vendors.contact As vendor_contact']);
            //end query//


            return $response;
        }
    }

}








