<?php

namespace App\Http\Controllers;

use App\Staff;
use App\Advance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdvanceController extends Controller
{
    function showCreatePage()
    {

        $table = Staff::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('advance_create')->with('table',$table);
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'staff' => 'required',
            'amount' => 'required',

        ]);


        $table = new Advance();
        $table->staff_id = $request->staff;
        $table->amount = $request->amount;
        $table->due = $request->amount;
        $table->wid=session('app_kb_login_workstation_id');
        $table->save();


        return redirect('advance.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = DB::table('advances')
            ->join('staff','advances.staff_id','=','staff.id')
            ->where('advances.wid','=',session('app_kb_login_workstation_id'))
            ->get(['advances.*','staff.name As staff_name']);


        return view('advance_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = DB::table('advances')
            ->join('staff','advances.staff_id','=','staff.id')
            ->where('advances.id','=',$id)
            ->first(['advances.*','staff.id As staff_id']);

        if ($table) {

                return view('advance_edit', array(
                    'id' => $table->id,
                    'staff_id' => $table->staff_id,
                    'amount' => $table->amount,
                    'staff' => Staff::all()
                ));
        } else {
            return $table;
       }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'staff' => 'required',
            'amount' => 'required',

            ]);



        Advance::where('id', $request->id)->update(array(
            'staff_id' => $request->staff,
            'amount' => $request->amount,
                ));

        return redirect('advance.list')->with('message', $request->staff_id . 'is updated !');


    }

    function destroy(Request $request)
    {


        Advance::where('id', $request->id)->delete();

        return redirect('advance.list')->with('message', $request->name . 'is deleted !');


    }

}








