<?php

namespace App\Http\Controllers;
use Session;
use App\Category;
use Illuminate\Http\Request;
use App\Vendor;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    function showCreatePage()
    {

        return view('category_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            // 'stock_no' => 'required',
            // 'sale_no' => 'required',
        ]);



        $table = new Category();
        $table->name = $request->name;
        $table->wid = Session::get('login_user_id');
        // $table->sale_voucher = $request->sale_no;
        // $table->stock_voucher = $request->stock_no;
        $table->save();


        return redirect('category.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = new Category();
        return view('category_list')->with('table', $table->where('wid','=',Session::get('login_workstation_id'))->get());
    }


    function EditPage($id)
    {

        $table = Category::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'category_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {



        $this->Validate($request, [
            'name' => 'required',
        ]);



        Category::where('id', $request->id)->update(array(
            'name'=> $request->name,

    ));

        return redirect('category.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Category::where('id', $request->id)->delete();

        return redirect('category.list')->with('message', $request->name . 'is deleted !');


    }


    function setting(Request $request)
    {


        Category::where('id', $request->id)->delete();

        return redirect('Category.list')->with('message', $request->name . 'is deleted !');


    }





}








