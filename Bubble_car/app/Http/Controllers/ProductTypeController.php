<?php

namespace App\Http\Controllers;

use App\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductTypeController extends Controller
{
    function showCreatePage()
    {

        return view('product_type_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);


        $table = new ProductType();
        $table->name = $request->name;
        $table->wid =session('app_kb_login_workstation_id');
        $table->save();


        return redirect('product.type.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = ProductType::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('product_type_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = ProductType::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'product_type_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);



        ProductType::where('id', $request->id)->update(array(
            'name'=> $request->name
                ));

        return redirect('product.type.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        ProductType::where('id', $request->id)->delete();

        return redirect('product.type.list')->with('message', $request->name . 'is deleted !');


    }

}








