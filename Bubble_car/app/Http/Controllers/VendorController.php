<?php

namespace App\Http\Controllers;

use App\Purchase;
use Illuminate\Http\Request;
use App\Vendor;
use Illuminate\Support\Facades\DB;

class VendorController extends Controller
{

    function showCreatePage()
    {

        return view('vendor_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'company_name' => 'required',
            'contact_person_name' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'cnic' => 'required',
            'address' => 'required',
            'opening_balance' => 'required',
            
        ]);



        $vendor_id = null;
        $a = DB::select("select * from `vendors` ORDER  BY `id` DESC  limit 1");
        foreach ($a as $i) {
            $vendor_id = $i->id;
        }


        $table = new Vendor();
        $table->company_name = $request->company_name;
        $table->person_name = $request->contact_person_name;
        $table->contact = $request->contact;
        $table->email = $request->email;
        $table->cnic = $request->cnic;
        $table->address = $request->address;
        $table->opening_balance = $request->opening_balance;
        
        $table->wid=session('login_workstation_id');
        $table->save();

        $last = null;
        $table = DB::select("select * from `vendors` ORDER  BY `id` DESC  limit 1");
        foreach ($table as $i) {
            $last = $i->id;
        }
        $balance=0-intval($request->opening_balance);
        $result = DB::Insert("Insert Into `vendor_payables`(  `vendor_id`, `purchase`, `payment`, `balance`,`purchase_date`,`transaction_type` ,`wid`) VALUES (?,?,?,?,?,?,?)",
            [
                $last,
                0,
                $request->opening_balance,
                $balance,
                date('Y-m-d'),
                'opening Balance',
                session('login_workstation_id')
            ]);

        return redirect('vendor.create')->with('message', $request->contact_person_name . 'is added !');


    }

    function showAllPage()
    {

        $table = new Vendor();
        return view('vendor_list')->with('table', $table->where('wid','=',session('app_kb_login_workstation_id'))->get());
    }


    function EditPage($id)
    {

        $table = Vendor::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'vendor_edit',array(
                    'id' => $i->id,
                    'company_name' => $i->company_name,
                    'person_name' => $i->person_name,
                    'contact' => $i->contact,
                    'email' => $i->email,
                    'cnic' => $i->cnic,
                    'address' => $i->address,
                    'opening_balance' => $i->opening_balance,
                    'ntn' => $i->ntn,
                    'srb' => $i->srb,
                    'business_nature' => $i->business_nature,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {



        $this->validate($request, [
            'company_name' => 'required',
            'contact_person_name' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'cnic' => 'required',
            'address' => 'required',
            'ntn' => 'required',
            'srb' => 'required',
            'business_nature'=>'required'
        ]);



        Vendor::where('id', $request->id)->update(array(
            'company_name'=> $request->company_name,
            'person_name' => $request->contact_person_name,
            'contact'     => $request->contact,
            'email'       => $request->email,
            'cnic'        => $request->cnic,
            'address'     => $request->address,
            'opening_balance' => $request->opening_balance,
            'ntn'         =>  $request->ntn,
            'srb'         =>  $request->srb,
            'business_nature' => $request->business_nature,

    ));

        return redirect('vendor.list')->with('message', $request->contact_person_name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Vendor::where('id', $request->id)->delete();

        return redirect('vendor.list')->with('message', $request->contact_person_name . 'is deleted !');


    }

}








