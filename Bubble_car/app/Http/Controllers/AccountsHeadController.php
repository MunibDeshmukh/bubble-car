<?php

namespace App\Http\Controllers;

use App\ExpenseHead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountsHeadController extends Controller
{
    function index()
    {

        return view('account_head_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);


        $table = new ExpenseHead();
        $table->name = $request->name;
        $table->wid =session('app_kb_login_workstation_id');
        $table->save();


        return redirect('account.head.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table =  ExpenseHead::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('account_head_list')->with('table', $table->all());
    }


    function EditPage($id)
    {

        $table = ExpenseHead::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'account_head_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
            ]);



        ExpenseHead::where('id', $request->id)->update(array(
            'name'=> $request->name
                ));

        return redirect('account.head.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        ExpenseHead::where('id', $request->id)->delete();

        return redirect('account.head.list')->with('message', $request->name . 'is deleted !');


    }

}








