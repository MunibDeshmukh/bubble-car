<?php

namespace App\Http\Controllers;

use App\Purchase;
use Illuminate\Http\Request;
use App\Outsourcevendor;
use Illuminate\Support\Facades\DB;

class OutSourceVendorController extends Controller
{
       function showCreatePage()
    {

        return view('outSourcevendor_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'company_name' => 'required',
            'contact_person_name' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'cnic' => 'required',
            'address' => 'required',
            'opening_balance' => 'required',
            
        ]);



        $vendor_id = null;
        $a = DB::select("select * from `outsourcevendors` ORDER  BY `id` DESC  limit 1");
        foreach ($a as $i) {
            $vendor_id = $i->id;
        }


        $table = new Outsourcevendor();
        $table->company_name = $request->company_name;
        $table->person_name = $request->contact_person_name;
        $table->contact = $request->contact;
        $table->email = $request->email;
        $table->cnic = $request->cnic;
        $table->address = $request->address;
        $table->opening_balance = $request->opening_balance;
        
        $table->wid=session('login_workstation_id');
        $table->save();

        $last = null;
        $table = DB::select("select * from `outsourcevendors` ORDER  BY `id` DESC  limit 1");
        foreach ($table as $i) {
            $last = $i->id;
        }
        $balance=0-intval($request->opening_balance);
        
        $result = DB::Insert("Insert Into `outsourcevendors`( `company_name`, `person_name`, `contact`, `email`, `cnic`, `address`, `opening_balance`, `wid`) VALUES (?,?,?,?,?,?,?,?)",
            [
                $request->company_name,
                $request->contact_person_name,
                $request->contact,
                $request->email,
                $request->cnic,
                $request->address,
                'opening Balance',
                session('login_workstation_id')
            ]);

        return redirect('outSourcevendor.create')->with('message', $request->contact_person_name . ' is added !');


    }

    function showAllPage()
    {

        $table = new Outsourcevendor();
        return view('Outsourcevendor_list')->with('table',$table->all());
    }


    function EditPage($id)
    {

        $table = Outsourcevendor::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'outsourcevendor_edit',array(
                    'id' => $i->id,
                    'company_name' => $i->company_name,
                    'person_name' => $i->person_name,
                    'contact' => $i->contact,
                    'email' => $i->email,
                    'cnic' => $i->cnic,
                    'address' => $i->address,
                    'opening_balance' => $i->opening_balance,
                 
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {



        $this->validate($request, [
            'company_name' => 'required',
            'contact_person_name' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'cnic' => 'required',
            'address' => 'required'
        ]);



        Outsourcevendor::where('id', $request->id)->update(array(
            'company_name'=> $request->company_name,
            'person_name' => $request->contact_person_name,
            'contact'     => $request->contact,
            'email'       => $request->email,
            'cnic'        => $request->cnic,
            'address'     => $request->address,
            'opening_balance' => $request->opening_balance,
            

    ));

        return redirect('outSourcevendor.list')->with('message', $request->contact_person_name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Outsourcevendor::where('id', $request->id)->delete();

        return redirect('outSourcevendor.list')->with('message', $request->contact_person_name . ' is deleted !');


    }
}
