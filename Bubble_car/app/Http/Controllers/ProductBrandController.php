<?php

namespace App\Http\Controllers;

use App\ProductBrand;
use App\ProductClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductBrandController extends Controller
{
    function showCreatePage()
    {

        $table = ProductClass::where('wid','=',session('app_kb_login_workstation_id'))->get();
        return view('product_brand_create')->with('table',$table);
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'class' => 'required'
        ]);


        $table = new ProductBrand();
        $table->name = $request->name;
        $table->class_id = $request->class;
        $table->wid =session('app_kb_login_workstation_id');
        $table->save();


        return redirect('product.brand.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = DB::table('product_brands')
            ->join('product_classes','product_brands.class_id','=','product_classes.id')
            ->where('product_brands.wid','=',session('app_kb_login_workstation_id'))
            ->get(['product_brands.*','product_classes.name As class_name']);


        return view('product_brand_list')->with('table', $table);
    }


    function EditPage($id)
    {

        $table = DB::table('product_brands')
            ->join('product_classes','product_brands.class_id','=','product_classes.id')
            ->where('product_brands.id','=',$id)
            ->first(['product_brands.*','product_classes.id As pid']);

        if ($table) {

                return view('product_brand_edit', array(
                    'id' => $table->id,
                    'name' => $table->name,
                    'class_id' => $table->pid,
                    'class' => ProductClass::all()
                ));
        } else {
            return $table;
       }
    }

    function submitUpdatePage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'class' => 'required'
            ]);



        ProductBrand::where('id', $request->id)->update(array(
            'name'=> $request->name,
            'class_id'=> $request->class
                ));

        return redirect('product.brand.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        ProductBrand::where('id', $request->id)->delete();

        return redirect('product.brand.list')->with('message', $request->name . 'is deleted !');


    }

}








