<?php

namespace App\Http\Controllers;

use App\AccountLedger;
use App\Banks;
use App\Customer;
use App\ExpenseHead;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{


    function newAccount($opening_balance)
    {
        // above bank detail //
        $last = Banks::orderby('id', 'desc')->first();
        $debit = $opening_balance;
        $balance = intval($opening_balance);

        //creating account in ledger //
        $table = new AccountLedger();
        $table->account_id = $last['id'];
        $table->credit = 0;
        $table->debit = $debit;
        $table->balance = $balance;
        $table->ref_transaction_id = 0;
        $table->ref_transaction_type = "152";
        $table->ref_transaction_desc = "opening_balance";
        $table->wid = session('app_kb_login_workstation_id');
        $table->save();

    }

    function newReceivable($account_id,$amount,$ref_id,$ref_type,$ref_desc)
    {
        // above bank detail //
        $last = AccountLedger::where('account_id','=',$account_id)->orderby('id', 'desc')->first();
        $debit = $amount;
        $balance = $amount + intval($last['balance']);
        //creating account in ledger //
        $table = new AccountLedger();
        $table->account_id = $account_id;
        $table->credit = 0;
        $table->debit = $debit;
        $table->balance = $balance;
        $table->ref_transaction_id = $ref_id;
        $table->ref_transaction_type = $ref_type;
        $table->ref_transaction_desc = $ref_desc;
        $table->wid = session('app_kb_login_workstation_id');
        $table->save();

    }


    function newPayable($account_id,$amount,$ref_id,$ref_type,$ref_desc)
    {
        // above bank detail //
        $last = AccountLedger::where('account_id','=',$account_id)->orderby('id', 'desc')->first();
        $debit = $amount;
        $balance = intval($last['balance'])-$amount ;
        //creating account in ledger //
        $table = new AccountLedger();
        $table->account_id = $account_id;
        $table->debit  = 0;
        $table->credit= $debit;
        $table->balance = $balance;
        $table->ref_transaction_id = $ref_id;
        $table->ref_transaction_type = $ref_type;
        $table->ref_transaction_desc = $ref_desc;
        $table->wid = session('app_kb_login_workstation_id');
        $table->save();

    }

    function accountLedger($id){

        $table = DB::table('account_ledgers')
            ->join('banks','account_ledgers.account_id','=','banks.id')
            ->where('account_ledgers.account_id','=',$id)
            ->get(['account_ledgers.*','banks.name As bank_name']);

        $array = array();
        $Aarray= array();

        foreach ($table as $e){

            if($e->ref_transaction_desc=="payable_expense" || $e->ref_transaction_desc=="receivable_expense"){
                $z = ExpenseHead::where('id','=',$e->ref_transaction_id)->first();

                $array[0] = $e->id;
                $array[1] = $e->bank_name;
                $array[2] = $e->credit;
                $array[3] = $e->debit;
                $array[4] = $e->balance;
                $array[5] = $z['name'];
                $array[6] = $e->ref_transaction_desc;
                $array[7] = $e->created_at;
                array_push($Aarray,$array);
            }

            if($e->ref_transaction_desc=="opening_balance" ){
                $array[0] = $e->id;
                $array[1] = $e->bank_name;
                $array[2] = $e->credit;
                $array[3] = $e->debit;
                $array[4] = $e->balance;
                $array[5] = "";
                $array[6] = $e->ref_transaction_desc;
                $array[7] = $e->created_at;
                array_push($Aarray,$array);
            }

            if($e->ref_transaction_desc=="customer_receivable" ){
                $x = Customer::where('id','=',$e->ref_transaction_id)->first();
                $array[0] = $e->id;
                $array[1] = $e->bank_name;
                $array[2] = $e->credit;
                $array[3] = $e->debit;
                $array[4] = $e->balance;
                $array[5] = $x['company_name'];
                $array[6] = $e->ref_transaction_desc;
                $array[7] = $e->created_at;
                array_push($Aarray,$array);
            }

            if($e->ref_transaction_desc=="vendor_payable" ){
                $x = Vendor::where('id','=',$e->ref_transaction_id)->first();
                $array[0] = $e->id;
                $array[1] = $e->bank_name;
                $array[2] = $e->credit;
                $array[3] = $e->debit;
                $array[4] = $e->balance;
                $array[5] = $x['company_name'];
                $array[6] = $e->ref_transaction_desc;
                $array[7] = $e->created_at;
                array_push($Aarray,$array);
            }





        }

        return view('bank_ledger')->with('table',$Aarray);



    }




}