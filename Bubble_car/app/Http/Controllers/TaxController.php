<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tax;

class TaxController extends Controller
{

    function showCreatePage()
    {

        return view('tax_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'amount' => 'required',
        ]);


        $table = new Tax();
        $table->name = $request->name;
        $table->amount = $request->amount;
        $table->save();
        return redirect('tax.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = new Tax();
        return view('tax_list')->with('table', $table->all());
    }


    function EditPage($id)
    {

        $table = Tax::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'tax_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                    'amount' => $i->amount,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {



        $this->validate($request, [
            'name' => 'required',
            'amount' => 'required',
        ]);



        Tax::where('id', $request->id)->update(array(
            'name' => $request->name,
            'amount' => $request->amount

        ));

        return redirect('tax.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Tax::where('id', $request->id)->delete();

        return redirect('tax.list')->with('message', $request->name . 'is deleted !');


    }

}