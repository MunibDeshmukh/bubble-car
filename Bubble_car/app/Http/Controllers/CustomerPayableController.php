<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Banks;
use App\CustomerPayable;
use App\GSTSale;
use App\Http\Controllers\AccountController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerPayableController extends Controller
{
    function showCreatePage()
    {

        return view('customer_payable_create',array(
            'customer' =>Customer::where('wid','=',session('app_kb_login_workstation_id'))->get(),
            'bank' =>Banks::where('wid','=',session('app_kb_login_workstation_id'))->get(),
        ));
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'customer_id' => 'required',
            'payment_mode' => 'required',
            'amount_paid' => 'required',

        ]);
        if($request->payment_mode =="cheque" ){
            $this->validate($request, [
                'bank_name' => 'required',
                'branch_name' => 'required',
                'account_title' => 'required',
                'cheque_no' => 'required',
            ]);

        }
        if($request->payment_mode =="online transfer" || $request->payment_mode =="fund transfer"){
            $this->validate($request, [
                'bank_name' => 'required',
                'branch_name' => 'required',
                'account_title' => 'required'
            ]);

        }
        if($request->payment_mode =="pay order" || $request->payment_mode =="direct deposit"){
            $this->validate($request, [
                'bank_name' => 'required',
            ]);

        }

        $last_payable = null;
        $table = DB::select("select * from `customer_payables` where `customer_id`=? ORDER  BY `id` DESC  limit 1",[$request->customer_id]);
        foreach ($table as $i) {
            $last_payable = $i->balance;
        }

        $a = intval($last_payable) - intval($request->amount_paid);

        $result = DB::Insert("Insert Into `customer_payables`(  `customer_id`, `credit`, `debit`, `balance` ,`transaction_date`,`transaction_type` ,`wid`,`account_id`) VALUES (?,?,?,?,?,?,?,?)",
            [
                $request->customer_id,
                0,
                $request->amount_paid,
                $a,
                date('Y-m-d'),
                '106',
                session('app_kb_login_workstation_id'),
                $request->account_id
            ]);


        $last = CustomerPayable::where('customer_id','=',$request->customer_id)->orderBy('id','desc')->first();
        $account = new AccountController();
        $account->newReceivable($request->account_id,$request->amount_paid,$request->customer_id,'316','customer_receivable');

        return redirect('customer.payable.create')->with('message', $request->name . 'is added !');


    }


    function showAllPage()
    {

        $table = DB::table('customer_payables')
            ->join('customers','customer_payables.customer_id','=','customers.id')
            ->where('customer_payables.wid','=',session('app_kb_login_workstation_id'))
            ->where('customer_payables.canceled','=',0)
            ->get([
                'customer_payables.*',
                'customers.contact As customer_contact',
                'customers.company_name As company_name',
                ]);


        return view('customer_payable_list')->with('table', $table);
    }



    function getCustomerInfo(Request $request)
    {
        //get last record from this customer //
        $last = CustomerPayable::where('customer_id','=',$request->customer_id)->orderBy('id','desc')->first();
        //end query//

        //get customer last detail of customer by $last var//
        $response = DB::table('customer_payables')->join('customers','customer_payables.customer_id','=','customers.id')->where('customer_payables.id','=',$last->id)
            ->get(['customer_payables.*','customers.contact As customer_contact']);
        //end query//

        return $response;
    }


    function newReceivableCredit($customer_id,$credit,$transaction_type,$transaction_date)
    {
        // get last balance //
        $last = CustomerPayable::where('customer_id','=',$customer_id)->orderBy('id','desc')->first();

        // calculate balance //
        $balance = $credit + intval($last['balance']);

        // Insert new credit //
        $result = DB::Insert("Insert Into `customer_payables`( `customer_id`, `credit`, `debit`, `balance`, `transaction_type`, `transaction_date`,`wid` ) VALUES (?,?,?,?,?,?,?)",
                  [$customer_id,$credit,0,$balance,$transaction_type,$transaction_date,session('app_kb_login_workstation_id'),]);

    }


}








