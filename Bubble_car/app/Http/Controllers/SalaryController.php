<?php

namespace App\Http\Controllers;

use App\Item;
use App\PurchaseLog;
use Illuminate\Http\Request;
use  App\Service;
use  App\Recipe;
use  App\Tax;
use  App\Vendor;
use Illuminate\Support\Facades\DB;

class SalaryController extends Controller
{

    function showCreatePage()
    {
        $table = DB::table('staff')
            ->leftjoin('loans','staff.id','=','loans.staff_id')
            ->leftjoin('advances','staff.id','=','advances.staff_id')
            ->get(['staff.*','loans.due As loan_due','loans.amount As loan_amount','loans.installment As loan_installment','advances.due As advance_amount']);

        return view('salary',array(
            'table'=>$table,
            'vendor'=>Vendor::all(),

        ));

    }

    function submitCreatePage(Request $request)
    {
        $result = DB::Insert("Insert Into `salaries`( `staff_id`, `salary`, `working_days`, `per_day`, `attended_day`, `net_salary`, `loan_amount`, `due_loan_amount`, `loan_installment`, `advance_amount`, `payable`,`loan_detect` ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
            [
                $request->id,
                $request->salary,
                $request->working_days,
                $request->per_day,
                $request->attended_day,
                $request->net_salary,
                $request->loan_amount,
                $request->due_loan_amount,
                $request->loan_installment,
                $request->advance_amount,
                $request->payable,
                $request->detect,
            ]);

        return 1;

    }

}