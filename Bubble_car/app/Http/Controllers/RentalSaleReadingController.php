<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Banks;
use App\CustomerPayable;
use App\GSTSale;
use App\RentalSale;
use App\RentalSaleReading;
use App\RentalSaleReadingProductLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RentalSaleReadingController extends Controller
{
    public $xyz;
    function showCreatePage($id)
    {
        //get last record from this customer //
        $x = RentalSaleReading::where('sale_id','=',$id)->orderBy('id','desc')->first();
        $arr  = explode('-',$x['voucher_no']);
        $num  = $arr[1]+1;
        $char = $arr[0];
        $voucher = $char."-".$num;


        //end query//
        return view('rental_sale_reading_create',array(
            'customer' =>Customer::all(),
            'sale_id' =>$id,
            'voucher' =>$voucher,
        ));
    }

    function submitCreatePage(Request $request)
    {
        $xyz = $request->sale_id;
        DB::Insert("Insert Into `rental_sale_readings`( `sale_id`, `total`, `service_tax`,`date`,`voucher_no`,`wid` ) VALUES (?,?,?,?,?,?)",
            [$request->sale_id,
                $request->final_amount,
                $request->service_tax,
                $request->date,
                $request->invoice_no,
                session('app_kb_login_workstation_id')

            ]);

        return 1;
    }





    function submitCreateProductLogPage(Request $request)
    {
        $reading = RentalSaleReading::where('wid','=',session('app_kb_login_workstation_id'))->orderBy('id','desc')->first();
            DB::Insert("Insert Into `rental_sale_reading_product_logs`( `reading_id`, `product_id`, `current_reading`,`previous_reading`, `quantity`, `rate`, `amount`, `others`,`net_amount`, `min_copy`, `amount_remaining_copy`, `total_amount` ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                [
                    $reading['id'],
                    $request->product_id,
                    $request->current_reading,
                    $request->previous_reading,
                    $request->quantity,
                    $request->rate,
                    $request->amount,
                    $request->other,
                    $request->net_amount,
                    $request->min_copy,
                    $request->amount_remaining_copy,
                    $request->total_amount,
                ]);

            return 1;
    }


    function submitCreateMiscLogPage(Request $request){

        $reading = RentalSaleReading::where('sale_id','=',$request->sale_id)->orderBy('id','desc')->first();
        $result = DB::Insert("Insert Into `rental_sale_reading_misc_logs`( `reading_id`, `field_name`, `field_amount` ) VALUES (?,?,?)",
                [
                    $reading['id'],
                    $request->field_name,
                    $request->field_amount,
                ]);

            return 1;




    }

    function showAllPage($id)
    {
        $table = DB::table('rental_sale_readings')
            ->join('rental_sales','rental_sale_readings.sale_id','=','rental_sales.id')
            ->join('customers','rental_sales.customer_id','=','customers.id')
            ->where('rental_sales.id', '=',$id)
            ->orderBy('rental_sale_readings.created_at', 'desc')
            ->get([
                'rental_sale_readings.*',
                'customers.contact As customer_contact',
                'customers.company_name As company_name',
                'customers.person_name As person_name',
                ]);

        return view('rental_sale_reading_list')->with('table', $table);
    }




    function getLastSale(Request $request)
    {
        $table = DB::table('rental_sales')
            ->join('customers','rental_sales.customer_id','=','customers.id')
            ->where('rental_sales.id','=',$request->id)
            ->where('rental_sales.wid','=',session('app_kb_login_workstation_id'))
            ->get(['rental_sales.*','customers.contact As customer_contact','customers.company_name As company_name','customers.person_name As person_name']);
        return $table;

    }



    function getSaleProducts(Request $request){

        $table = DB::table('rental_sale_logs')
            ->join('products','rental_sale_logs.product_id','=','products.id')
            ->where('rental_sale_logs.sale_id','=',$request->id)
            ->get(['products.id As product_id','products.name As product_name']);
        return $table;
    }

    function getSaleReading(Request $request){
        $reading = RentalSaleReading::where('sale_id','=',$request->sale_id)->orderBy('id','desc')->first();
        $table = DB::select("select * from `rental_sale_reading_product_logs` where `reading_id`= ? and `product_id` = ? order  by id desc  limit 1",[$reading['id'],$request->product_id]);
        return $table;

    }

    function  printView($id){
        $table = DB::table('rental_sale_readings')
            ->join('rental_sales','rental_sale_readings.sale_id','=','rental_sales.id')
            ->join('customers','rental_sales.customer_id','=','customers.id')
            ->where('rental_sale_readings.id',$id)
            ->first([
                'rental_sale_readings.*',
                'customers.contact As customer_contact',
                'customers.company_name As company_name',
                'customers.srb  As srb',
                'customers.person_name As person_name',
            ]);



        $product = DB::table('rental_sale_reading_product_logs')
            ->join('products','rental_sale_reading_product_logs.product_id','=','products.id')
            ->where('rental_sale_reading_product_logs.reading_id','=',$id)
            ->orderby('rental_sale_reading_product_logs.id','desc')
            ->distinct('rental_sale_reading_product_logs.product_id')
            ->get(['rental_sale_reading_product_logs.*','products.name As product_name']);

        $misc = DB::table('rental_sale_reading_misc_logs')
            ->where('reading_id','=',$id)
            ->get();


        return view( 'rental_sale_reading_print_view',array(
            'table'=>$table,
            'products'=>$product,
            'misc'=>$misc,
        ));

    }

    function createdMessage($id)
    {

        return redirect('rental.sale.reading.create.'.$id)->with('message', 'Rental sale is added!');
    }






}








