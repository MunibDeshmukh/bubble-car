<?php

namespace App\Http\Controllers;
use App\Sale;
use App\Product;
use App\Purchase;
use App\Inventory;
use App\purchase_log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class StockController extends Controller
{
    
   public function showStockAdjusment(){
    
       
       
         $table = DB::table('products')
            ->join('inventories','products.id','=','inventories.product_id')
            ->join('brands','products.brand','=','brands.id')
             ->join('categories','products.category','=','categories.id')
            ->get(['products.*','brands.name as brandName','categories.name as categoryName','inventories.*']);

        

        return view('StockAdjusment')->with('table', $table);
   
    
   }
    
    
    
    public function StockManage(Request $request){
        
     $i = 0;
       foreach($request->quantity as $quan){
           
           
           if($request->chk[$i]=="add"){
           
         $get_quantity = Inventory::where('product_id',$request->id[$i])->first();
        
        $updated_quantity = intval($get_quantity['product_quantity']) + intval($quan);     
             DB::table('inventories')
            ->where('product_id',$request->id[$i])
            ->update(['product_quantity' => $updated_quantity]);
          
       }else if($request->chk[$i]=="subtract"){
              $get_quantity = Inventory::where('product_id',$request->id[$i])->first();
        
        $updated_quantity = intval($get_quantity['product_quantity']) -  intval($quan);     
             DB::table('inventories')
            ->where('product_id',$request->id[$i])
            ->update(['product_quantity' => $updated_quantity]);
          
               
               
           }
          
          $i++;  
       }
       
            
                     
    
        
        
        
        
    }
    
    
    
    
       public function showDamage(){
    
       
         $table = DB::table('products')
            ->join('inventories','products.id','=','inventories.product_id')
            ->join('brands','products.brand','=','brands.id')
             ->join('categories','products.category','=','categories.id')
            ->get(['products.*','brands.name as brandName','categories.name as categoryName','inventories.*']);

        

        return view('Damage')->with('table', $table);
   
    
   }
    
    
    
    
    
}
