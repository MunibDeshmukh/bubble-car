<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Units;

class UnitController extends Controller
{

    function showCreatePage()
    {

        return view('unit_create');
    }

    function submitCreatePage(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);


        $table = new Units();
        $table->name = $request->name;
        $table->save();
        return redirect('unit.create')->with('message', $request->name . 'is added !');


    }

    function showAllPage()
    {

        $table = new Units();
        return view('unit_list')->with('table', $table->all());
    }


    function EditPage($id)
    {

        $table = Units::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'unit_edit',array(
                    'id' => $i->id,
                    'name' => $i->name,
                ));
            }
        } else {
            return $table;
        }
    }

    function submitUpdatePage(Request $request)
    {



        $this->validate($request, [
            'name' => 'required',
        ]);



        Units::where('id', $request->id)->update(array(
            'name' => $request->name,

        ));

        return redirect('unit.list')->with('message', $request->name . 'is updated !');


    }

    function destroy(Request $request)
    {


        Units::where('id', $request->id)->delete();

        return redirect('unit.list')->with('message', $request->name . 'is deleted !');


    }

}