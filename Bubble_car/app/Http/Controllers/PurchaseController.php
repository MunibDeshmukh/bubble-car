<?php

namespace App\Http\Controllers;

use App\GSTSale;
use App\Purchase;
use App\purchase_log;
use App\VendorPayable;
use App\Workstation;
use App\Grn;
use App\Inventory;
use Illuminate\Http\Request;
use  App\Service;
use  App\Recipe;
use  App\Tax;
use  App\Vendor;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{

    function showCreatePage()
    {
        $count = DB::table('purchases')->count();
        $voucher = null;
        if($count==0) {
            $x = Workstation::where('id', '=', session('login_workstation_id'))->first();
            // dd($x['stock_voucher']);
            $arr  = explode('-',$x['stock_voucher']);
            $num  = $arr[1];
            $char = $arr[0];
            $voucher = $char."-".$num;
        }
        else {
            $x = Purchase::orderby('id', 'desc')->first();
            $arr  = explode('-',$x['voucher_no']);
            $num  = intval($arr[1])+1;
            $char = $arr[0];
            $voucher = $char."-".$num;
        }

        return view('purchase_create',array(
            'vendors' =>Vendor::where('wid','=',session('login_workstation_id'))->get(),
            'tax'=>Tax::all(),
            'voucher'=>$voucher
        ));


    }

    function submitCreatePageForPurchase(Request $request)
    {

        $result = DB::Insert("Insert Into `purchases`(  `vendor_id`,`voucher_no`, `item_quantity`,  `total_amount`, `tax_type`, `tax_action`, `tax_detect_as`,`tax_value`, `payable`, `date`,`wid`) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
            [
                $request->vendor,
                $request->voucher,
                $request->qty,
                $request->amount,
                $request->tax_type,
                $request->tax_action,
                $request->tax_as,
                $request->tax,
                $request->total,
                
                $request->date,
                // $request->voucher,
                session('login_workstation_id')
               ]);

        $last = VendorPayable::where('wid','=',session('login_workstation_id'))
                ->where('vendor_id','=',$request->vendor)->orderBy('id','desc')->first();


        $a = intval($request->total)+intval($last['balance']);

        $result = DB::Insert("Insert Into `vendor_payables`(  `vendor_id`, `purchase`, `payment`, `balance`,`purchase_date`,`transaction_type` ,`wid`) VALUES (?,?,?,?,?,?,?)",
            [
                $request->vendor,
                $request->total,
                0,
                $a,
                date('Y-m-d'),
                'purchase',
                session('login_workstation_id')
            ]);

        return $request->all();

    }


    function submitCreatePageForLog(Request $request)
    {

//        if($request->i==0) {
            $purchase_id = null;
            $table = Purchase::orderby('id','desc')->first();

            if ($table->id) {


                $result = DB::Insert("Insert Into `purchase_logs`(  `product_id`, `purchase_id`, `item_quantity`, `price`, `amount`, `tax`, `payable` ) VALUES (?,?,?,?,?,?,?)",
                    [
                        $request->id,
                        $table->id,
                        $request->qty,
                        $request->price,
                        $request->amount,
                       
                        $request->tax,
                        $request->total
                    ]);

                return 1;

            }

            return 0;
//        }



    }


    function showAll()
    {

        $table = DB::table('purchases')
            ->join('vendors'   ,'purchases.vendor_id'     ,'=' ,'vendors.id')
            ->where('purchases.wid','=',session('login_workstation_id'))
//           ->where('g_s_t_sales.canceled','=',0)
            ->get([
                'purchases.*',
                'vendors.company_name as company_name',
                'vendors.person_name as person_name',
            ]);


        return view('purchase_list')->with('table', $table->all());
    }

    function grn(){
 $table = DB::table('purchases')
            ->join('vendors'   ,'purchases.vendor_id'     ,'=' ,'vendors.id')

            ->get(['vendors.company_name as company_name',

                'purchases.voucher_no as voucher_no',
                   'vendors.id as id',
                   
        ]);
        $table2=Grn::all();


        return view('purchase_grn_list',array('table'=>$table,'table2'=>$table2));

            // ->with('table', $table->all());


    }
    function grninsert(){
      $id = $_GET['id'];
      $voucherNo = $_GET['voucherNo'];
      $productId = $_GET['productId'];
      $purchase_id = $_GET['purchase_id'];
      $productName = $_GET['productName'];
      $amount = $_GET['amount'];
      $Quantity = $_GET['Quantity'];
    $result = DB::Insert("Insert Into `grns`(`grNo`,`productId`,`voucherNo`) 
                        VALUES ('".$id."','".$productId."','".$voucherNo."')");

 purchase_log::where('product_id', $productId)->where('purchase_id', $purchase_id)->update(array(
            'status' => 'Received',
            ));

  

   DB::Insert("Insert Into `stocks`(`gr_no`,`product_id`, `product_name`, `voucher_no`, `product_quantity`, `product_price`) 
    VALUES ('".$id."','".$productId."','".$productName."','".$voucherNo."',
            '".$Quantity."','".$amount."')");

 $chk = DB::table('inventories')->where('product_name', '=', $productName)->count();

     if($chk>0){
        $quan = Inventory::where('product_name', '=', $productName)->first();
     $inq = intval($quan['product_quantity']) + $Quantity;
     
Inventory::where('product_name', $productName)->update(array(
            'product_quantity' => $inq,
            ));

     }else{

DB::Insert("Insert Into `inventories`(`gr_no`,`product_id`, `product_name`, `voucher_no`, `product_quantity`, `product_price`) 
    VALUES ('".$id."','".$productId."','".$productName."','".$voucherNo."',
            '".$Quantity."','".$amount."')");
}



        $table = DB::table('purchases')
            ->join('vendors'   ,'purchases.vendor_id','=','vendors.id')
            ->join('purchase_logs','purchases.id','=','purchase_logs.purchase_id')
            ->join('products','purchase_logs.product_id','=','products.id')
            ->where('vendors.id','=',$id)
            ->where('purchases.voucher_no','=',$voucherNo)

            ->get([
                'purchases.*',
                'purchase_logs.*',
                'products.*',
            ]);


        return view('grn_Received_purchase_view')->with('table', $table->all());

 


    }

    function grnGetproduct(){

$id = $_GET['id'];
$vno = $_GET['vno'];
 $table = DB::table('purchases')
            ->join('vendors'   ,'purchases.vendor_id','=','vendors.id')
            ->join('purchase_logs','purchases.id','=','purchase_logs.purchase_id')
            ->join('products','purchase_logs.product_id','=','products.id')
             ->where('vendors.id','=',$id)
             ->where('purchases.voucher_no','=',$vno)
            
            ->get([
                   'purchases.*',
                   'purchase_logs.*',
                   'products.*',
        ]);


        return view('grn_purchase_view')->with('table', $table->all());

    }

    function grnView(){
  $table = DB::table('grns')
            ->join('products'   ,'grns.productId' ,'=' ,'products.id')

            ->get([
                'products.name As productName',
                'grns.*',
            ]);


        return view('grn_view_list')->with('table', $table->all());


    }

    function EditPage($id)
    {

       // $table = Purchase::where('id', '=', $id)->get();
 $table = DB::table('Purchases')
            ->join('vendors','Purchases.vendor_id','=','vendors.id')
            ->join('purchase_logs','Purchases.id','=','purchase_logs.purchase_id')
            ->join('products','purchase_logs.product_id','=','products.id')
            ->get(['Purchases.*','vendors.company_name As company_name','purchase_logs.*','products.name As productName','products.unit As productUnit']);
        if ($table) {

            foreach ($table as $i) {

                return view( 'purchase_edit',array(
                    'id' => $i->id,
                    'vendor_id' => $i->company_name,
                    'voucher_no' => $i->voucher_no,
                    'item_quantity' => $i->item_quantity,
                    'total_amount' => $i->total_amount,
                    'tax_type' => $i->tax_type,
                    'tax_action' => $i->tax_action,
                    'tax_detect_as' => $i->tax_detect_as,
                    'tax_value' => $i->tax_value,
                    'date' => $i->date,
                    'tex_detect_as' => $i->tax_detect_as,
                    'price' => $i->price,
                    'item_quantity' => $i->item_quantity,
                    'amount' => $i->amount,
                    'tax' => $i->tax,
                    'payable' => $i->payable,
                    'productUnit' => $i->productUnit,
                    'productName' => $i->productName,

                ));
            }
        } else {
            return $table;
        }
    }



    function fetchItem()
    {

        $table = DB::table('items')
            ->join('units','items.unit','=','units.id')
            ->get(['items.*','units.name As unit_name']);


       return $table;

    }



    function  showSingleRecipe($id)
    {

        $table = DB::table('purchase_logs')
            ->join('products','purchase_logs.product_id','=','products.id')
            ->join('purchases','purchase_logs.purchase_id','=','purchases.id')
            ->join('vendors','purchases.vendor_id','=','vendors.id')
            ->where('purchase_logs.purchase_id','=',$id)
            ->get(['purchase_logs.*','products.name As product_name','vendors.*','purchases.payable As total_amount','purchases.tax_value As tax_val','purchases.voucher_no As voucher_no']);



        return view( 'purchase_print_view',array(
            'table'=>$table,
            // 'sale'=>GSTSale::where('id','=',$id)->first(),
            'company_name'=>$table[0]->company_name,
            'person_name'=>$table[0]->person_name,
            'voucher_no'=>$table[0]->voucher_no,
            'total_amount'=>$table[0]->total_amount,
            // 'srb'=>$table[0]->srb,
            
            'tax'    =>$table[0]->tax_val

        ));

    }


        function createdMessage(){
            return redirect('purchase.create')->with('message',  'Purchase saved !');
        }




}