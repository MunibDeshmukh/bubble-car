<?php

namespace App\Http\Controllers;

use App\Outsourceservices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OutSourceServiceController extends Controller
{
     function showCreatePage()
    {

   

        return view('outsourceservice_create');
    }

    function submitCreatePage(Request $request)
    {


        $result = DB::Insert("Insert Into `outsourceservices`(`ServiceName`) VALUES (?)",
            [
                $request->serviceName
            ]);

     
return redirect('outsourceservices.list')->with('message',' Service Added Successfully!');


    }

    function showAll()
    {

        $table =  new Outsourceservices();


        return view('outsourceservice_list')->with('table', $table->all());
    }
    
    
        function EditPage($id)
    {

        $table = Outsourceservices::where('id', '=', $id)->get();

        if ($table) {

            foreach ($table as $i) {

                return view( 'outsourceservices_edit',array(
                    'id' => $i->id,
                    'serviceName' => $i->serviceName,
                   
                 
                ));
            }
        } else {
            return $table;
        }
    }
    
    
        function submitUpdatePage(Request $request)
    {



        $this->validate($request, [
            'serviceName' => 'required'
        ]);



        Outsourceservices::where('id', $request->id)->update(array(
            'serviceName'=> $request->serviceName,
            ));

       return redirect('outsourceservices.list')->with('message',' Service Updated Successfully!');


    }
    
        function destroy(Request $request)
    {


        Outsourceservices::where('id', $request->id)->delete();

        return redirect('outsourceservices.list')->with('message',' Service deleted Successfully!');


    }
    
    
    
    
    
    

}
